.PHONY: all, concat_md, render_html, render_pdf, gitlab_pages, setup_gitlab_pages

IN ?= presentation-fr.md
IN_ENGLISH ?= presentation-en.md
LIBRERY = the-librery-doc-fr.md
OUT ?= $(basename $(IN))

# $(info $$CI_PROJECT_NAME = $(CI_PROJECT_NAME))

# all: render_html

# # OR to update many presentations at once:

concat_md:
	$(info $... concat_md)
	@cat \
		texts/01_cover.md \
		texts/02_summary.md \
		texts/03_prologue.md \
		texts/commons/personal-presentations.md \
		texts/04_main_proposals.md \
		texts/15_arguments.md \
		texts/05_problematic.md \
		texts/06_commons_condition.md \
		texts/07_centric_notions.md \
		texts/08_why_2_percents.md \
		texts/09_author_rights.md \
		texts/10_the_librery.md \
		texts/11_collect_redistribution.md \
		texts/12_governance.md \
		texts/13_international.md \
		texts/14_scenarios.md \
		texts/15_arguments.md \
		texts/94_inspirations-A.md \
		texts/commons/inspirations.md \
		texts/94_inspirations-B-copie_publique.md \
		texts/94_inspirations-C-post_open_licence.md \
		texts/95_contribute.md \
		texts/96_references-A.md \
		texts/commons/references.md \
		texts/96_references-press-A.md \
		texts/commons/press.md \
		texts/96_references-B-credits.md \
		texts/97_thanks-A-start.md \
		texts/commons/thanks-reviews.md \
		texts/97_thanks-B-middle.md \
		texts/commons/thanks-informal.md \
		texts/97_thanks-C-end.md \
		texts/98_versions.md \
		texts/99_outro.md \
		> ${IN}
	@cat \
		texts/en/01_cover.md \
		texts/en/02_summary.md \
		texts/en/03_prologue.md \
		texts/commons/personal-presentations.md \
		texts/en/04_main_proposals.md \
		texts/en/15_arguments.md \
		texts/en/05_problematic.md \
		texts/en/06_commons_condition.md \
		texts/en/07_centric_notions.md \
		texts/en/08_why_2_percents.md \
		texts/en/09_author_rights.md \
		texts/en/10_the_librery.md \
		texts/en/11_collect_redistribution.md \
		texts/en/12_governance.md \
		texts/en/13_international.md \
		texts/en/14_scenarios.md \
		texts/en/15_arguments.md \
		texts/en/94_inspirations-A.md \
		texts/commons/inspirations.md \
		texts/en/94_inspirations-B-copie_publique.md \
		texts/en/94_inspirations-C-post_open_licence.md \
		texts/en/95_contribute.md \
		texts/en/96_references-A.md \
		texts/commons/references.md \
		texts/en/96_references-press-A.md \
		texts/commons/press.md \
		texts/en/96_references-B-credits.md \
		texts/en/97_thanks-A-start.md \
		texts/commons/thanks-reviews.md \
		texts/en/97_thanks-B-middle.md \
		texts/commons/thanks-informal.md \
		texts/en/97_thanks-C-end.md \
		texts/98_versions.md \
		texts/en/99_outro.md \
		> ${IN_ENGLISH}
	@cat \
		texts/01_cover_librery.md \
		texts/02_summary_librery.md \
		texts/05_problematic.md \
		texts/06_commons_condition.md \
		texts/10_the_librery.md \
		texts/11_collect_redistribution.md \
		texts/12_governance.md \
		texts/13_international.md \
		texts/14_scenarios.md \
		texts/94_inspirations-A.md \
		texts/commons/inspirations.md \
		texts/94_inspirations-B-copie_publique.md \
		texts/94_inspirations-C-post_open_licence.md \
		texts/95_contribute.md \
		texts/96_references-A.md \
		texts/commons/references.md \
		texts/96_references-press-A.md \
		texts/commons/press.md \
		texts/96_references-B-credits.md \
		texts/97_thanks-A-start.md \
		texts/commons/thanks-reviews.md \
		texts/97_thanks-B-middle.md \
		texts/commons/thanks-informal.md \
		texts/97_thanks-C-end.md \
		texts/98_versions.md \
		texts/99_outro_librery.md \
		> ${LIBRERY}

all:
	$(info $... all)
	@make concat_md
	@make render_html IN=presentation-fr.md
	@make render_html IN=presentation-en.md
	@make render_html IN=${LIBRERY}

all_pdf:
	$(info $... all_pdf)
	@make all
	@make render_pdf IN=presentation-fr.md
	@make render_pdf IN=presentation-en.md
	@make render_pdf IN=${LIBRERY}
	@make move_pdf

move_pdf:
	$(info $... move_pdf)
	@mv *.pdf ./pdf_files

render_html: ## Render presentation to revealjs slideshow with pandoc
	$(info $... render_html)
	$(info $$REPO = $(REPO))
	$(info $$IN = $(IN))
	$(info $$OUT = $(OUT))
	$(info $$basename = $(basename $(IN)))
	@pandoc \
		-t revealjs \
		-o $(OUT).html \
		-V colorlinks=true \
		-V theme=white \
		-V slideNumber=true \
		-V progress=true \
		--css static/style.css \
		--standalone \
		--slide-level=2 \
		--include-in-header=static/header_include.html \
		--include-after-body=static/add_custom_footer.html \
		$(IN)
# -F mermaid-filter \

$(OUT).html: render_html

render_pdf: $(OUT).html ## Render presentation to pdf with decktape (requires revealjs slideshow)
	@decktape $(OUT).html $(OUT).pdf

gitlab_pages: $(OUT).html public/_redirects # Used in CI to prepare ./public directory for gitlab pages. Does not repeat pages setup if already run.
	@cp $(OUT).html public

public/_redirects:
	@make setup_gitlab_pages

setup_gitlab_pages: # Setup static assets and redirection from the root. CI_PROJECT_NAME is populated by gitlab CI
	@mkdir public
	@echo '/$(CI_PROJECT_NAME)  /$(CI_PROJECT_NAME)/$(OUT).html' > public/_redirects
	@echo '/:subgroup/$(CI_PROJECT_NAME)/  /:subgroup/$(CI_PROJECT_NAME)/$(OUT).html' >> public/_redirects
	@cp -r static public/static
	@cp -r images public/images

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

