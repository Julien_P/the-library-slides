# Revealjs slideshow for Librery project

![&nbsp;](images/Librery-logo-baudot-black.png)

---

 <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/Julien_P/the-library-slides">The Librery - slides</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/Julien_P">Julien Paris</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:1em!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg"></a></p>

---

## Comment contribuer ?

Si vous souhaitez contribuer à cette réflexion veillez à **NE PAS mofifier le fichier `presentation-fr.md`** à la racine, mais bien le ou les fichiers situés dans le répertoire `/texts`.

Le répertoire `/texts` sert à ventiler tous les chapitres de la réflexion en autant de fichiers :

```md
└── texts
    ├── commons
    |  ├── inspirations.md
    |  ├── references.md
    |  ├── thanks-informal.md
    |  └── thanks-reviews.md
    ├── en
    |  ├── (translations to english)
    |  └── ...
    ├── 01_cover.md
    ├── 02_summary.md
    ├── 02_summary_librery.md
    ├── 03_prologue.md
    ├── 04_main_proposals.md
    ├── 05_problematic.md
    ├── 06_commons_condition.md
    ├── 07_centric_notions.md
    ├── 08_why_2_percents.md
    ├── 09_author_rights.md
    ├── 10_the_librery.md
    ├── 11_collect_redistribution.md
    ├── 12_governance.md
    ├── 94_inspirations-A.md
    ├── 94_inspirations-B-copie_publique.md
    ├── 95_contribute.md
    ├── 96_references-A.md
    ├── 96_references-B-credits.md
    ├── 97_thanks-A-start.md
    ├── 97_thanks-B-middle.md
    ├── 97_thanks-C-end.md
    ├── 98_versions.md
    └── 99_outro_librery.md
    └── 99_outro.md
```

Ces différents fichiers sont compilés grâce à la commande `concat_md` dans le `Makefile`.

La commande `make all_pdf` permet de :

1. Produire les fichiers `presentation-fr.md`, `presentation-en.md`, `the-librery-doc.md`  qui compilent tous les textes contenus dans le répertoire `/texts`
1. Générer les fichiers associés `presentation-fr.html`, `presentation-en.html`, `the-librery-doc.html` associés
1. Générer les pdf associés et les déplacer dans le répertoire `/pdf_files`

---

## Ressources

- Documentation / articles / thèses / rapports ... : 
  - [Articles / livres en pdf (nextcloud)](https://nuage.liiib.re/s/dx5eribMSRm6RTm)
  - [Bibliographie (pad)](https://hackmd.io/@Jpy/librery-bibliographie)
  - [Bibliographie (Zotero)](https://www.zotero.org/groups/5232357/the_librery/library)
- [Vidéos / conférences (playlist Youtube)](https://www.youtube.com/playlist?list=PLfXKG51wk2sTBeAJtuL8wsUNvD-6lVDPM)

---

## Deployed Gitlab pages

- [Presentation-fr.md](https://julien_p.gitlab.io/the-librery-slides)
- [the-librery-doc-fr.md](https://julien_p.gitlab.io/the-librery-slides/librery.html)

---

## Usage

* Install dependencies :
  * [pandoc](https://pandoc.org/) for html output
  * [decktape](https://github.com/astefanutti/decktape) for pdf output.
  * [mermaid-filter](https://github.com/raghur/mermaid-filter) (`mermaid-filter@1.4.6`) - not working perfectly yet

* Check that you have the proper fonts : [`Poppins semibold`](https://fonts.google.com/specimen/Poppins) & [`Carlito`](https://www.1001fonts.com/carlito-font.html) (or [`Lato`](https://fonts.google.com/specimen/Lato))

* Write your presentation in pandoc using [pandoc markdown syntax](https://pandoc.org/MANUAL.html#slide-shows) (default `presentation-fr.md` file shows some examples)

* Convert to a [revealjs](https://revealjs.com/) html presentation with `make render_html` (or just `make`) or to a pdf with `make render_pdf`.

## Gitlab pages

Build and deployed to Gitlab pages with Gitlab CI.

To render a collection of presentations on the same instance of pages, just repeat `make gitlab_pages` with the specific `IN` and optionnaly `OUT` variables in the `.gitlab-ci.yml` file. 

The first call will determine to which presentation the root will be redirected.

## Customization

* The name of the presentation and of the output can be changed with environment variables `IN` and `OUT`. `IN` is the markdown source file, and   `OUT` is the basename (no extension) of the output files. They can be updated in the header of the "Makefile" accordingly, or passed to the command line as in the following example : `make render_html OUT=$(date -I)_presentation-fr`.
* Adding and positionning the logo of the customer on the title page can be done using the yaml header of the markdown file.
* The default reciepe (`make` or `make all`) can be customized to your liking, see commented block in `Makefile` for an example that renders several presentations at once.

## Notes

- Logo / image as watermak (top-right) : in the `css` file

```css
body:after {
  content: url(https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg);
  position: fixed;
  transform: scale(.5);
  top: .5rem;
  right: .5rem;
}
```

- Hide a slide

Encapsulate the markdown block with a `data-visibility= "hidden"` block:

```md
::::::::: {data-visibility="hidden"}
## En quelques questions

Blablabla

:::::::::
```

- Use icons

We use [RemixIcons library](https://remixicon.com)