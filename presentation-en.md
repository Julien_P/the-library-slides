---
title: The Librery
subtitle: |
  <strong>2% for the commons &nbsp; <br>&nbsp;and their authors</strong>
  <br>
  <img src="images/qr-code-slides-en.png" alt="QR code" height="175px" style="margin: 5% 3%"/>
author: '
  <code>
    Version 1.7
  </code><br><br>
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _FLOSS fullstack developer_
  </span><br>
  <span class="text-micro text-author-details">
    _co-founder of the [tech cooperative multi](https://multi.coop)_
  </span>
'
date: '
  Illustration : [XKCD n°2347](https://xkcd.com/2347/)
  <br>
  <br><b>Navigation with the arrows</b> <i class="ri-drag-move-2-fill"></i> / Plan pressing <span class="text-nano">`echap`</span>
  <br>
  <br>
  <span style="font-size: .7rem;" xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/Julien_P/the-librery-slides">The Librery - slides</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/Julien_P">J. Paris</a> is under licence <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:.7rem!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:.7rem!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:.7rem!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></span> 
'
title-slide-attributes:
  data-background-image: "images/XKCD.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 45%"
  data-background-position: "right 20% top 40%"
---

# Summary

::: {.text-micro}

- [Prologue](#prologue)
- [Summary of key proposals](#summary-of-key-proposals)
- [Problematic](#problematic)
- [The commons, condition and driver of innovation](#the-commons-condition-and-driver-of-innovation)
- [Some central notions](#some-central-notions)
- [Why 2% of public procurement?](#why-2-of-public-procurement)
- [The interest of copyright in supporting the economy of the commons](#the-interest-of-copyright-in-supporting-the-economy-of-the-commons)
- [The Librery](#the-librery)
- [Collect and redistribution](#collect-and-redistribution)
- [Thoughts on governance](#thoughts-on-the-governance)
- [Internationalisation & decentralization](#internationalisation-decentralization)
- [Inspirations](#inspirations)
- [Scenarios](#scenarios)
- [How can we continue this reflection?](#how-can-we-continue-this-reflection)
- [References and documentation](#references-and-documentation)
- [Thanks](#thanks)
- [Document versions](#versions-du-document)
:::

# Prologue

::: {.text-center}
[https://julien_p.gitlab.io/the-librery-slides](https://julien_p.gitlab.io/the-librery-slides)
:::

The <b>comments</b> appearing in this presentation <b>are those of the author alone</b>.

These reflections and proposals are in <b>no way fixed</b> and represent more of a <b>step in a process of collective reflection</b>.

This communication exercise should be considered above all as a way of <b>opening these ideas to criticism </b>, with the aim of ultimately arriving at a base of <b>concrete proposals</b> that can be put forward by the broadest possible consortium of commons' stakeholders.

All elements presented here are open to discussion and criticism, including on a [notes pad](https://hackmd.io/@Jpy/the-librery) <b> open for writing and comments</b>.

## Related presentations

- [2024-12-05 / OSX 2024 : "Make your way to finance an open source project"](https://multi-coop.gitlab.io/slides/seminaires/2024-12-05-OSX-jungle-financements/presentation.html)

# Summary of key proposals

:::::: {.columns}
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-money-euro-circle-fill"></i><br>
"**2% for commons**" <br>on public digital projects
:::
:::
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-group-fill"></i><br>
A fund managed **independently**<br>by commons stakeholders
:::
:::
::::::

:::::: {.columns}
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-download-2-fill"></i><br>
A structure that can receive public or private **financial contributions**
:::
:::
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-scales-3-fill"></i><br>
A **transparent, equal and non-profit** redistribution organization
:::
:::
::::::

## General economy

:::{.opaque .img-no-caption}
![&nbsp;](images/diagrams/diagram-08.png)
:::

## Distribution of collected funds

:::{.opaque .img-no-caption}
![&nbsp;](images/librery-fund-sankey-en.png)
:::

## Internationalisation

:::{.opaque .img-no-caption}
![&nbsp;](images/diagrams/decentralized/diagram-99.png)
:::

## Funding digital commons as a common

::: {.opaque .img-xl .img-no-caption .img-no-margin}
![&nbsp;](images/diagrams/Handler.ashx_.jpeg)
:::

::: {.text-micro .text-center}
<i class="ri-book-2-line"></i>
See the book <b>"Governing the commons"</b> by [Elinor Orstom](https://en.wikipedia.org/wiki/Elinor_Ostrom)
:::

## Argument grid <br>for financial contributors

<table class="grid">
  <thead>
    <tr>
      <th></th>
      <th class="text-center"><b>PUBLIC</b></th>
      <th class="text-center"><b>PRIVATE</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>**CONTRIBUTION**</td>
      <td>
        2% of the amount <br>of digital projects
      </td>
      <td>
        Depending on the type and size of the business:<br>
        - x % of annual recurring revenue<br>
        - x % of management surplus / annual profits<br>
        - x € yearly by developer<br>
        - fixed sum annually
      </td>
    </tr>
    <tr>
      <td>**OPTIONS**</td>
      <td>
One-off agreement <br>
Multi-annual agreement <br>
Agreements per project<br>
(or) with an administration
      </td>
      <td>
One-off or multi-annual agreement <br>
Donations<br>
Pledge of donations
      </td>
    </tr>
    <tr>
      <td>**CONTRACTUALISATION**</td>
      <td class="text-center" colspan="2">
45% of the donation: direction by the structure towards one or more common areas<br>
45% of the donation: direction by OGC towards one or more communities<br>
10% of the donation: OGC management fees
      </td>
    </tr>
    <tr>
      <td>**COUNTERPARTS**</td>
      <td class="text-center" colspan="2">
Critical commons maintenance insurance<br>
Labeling <br>
Team empowerment<br>
Choice by structure to direct towards common areas<br>
Choice by structure to specify certain remuneration mechanisms (distribution keys)<br>
Tax exemption (if private structure)
      </td>
    </tr>
    <tr>
      <td>**FUNDS MANAGEMENT**</td>
      <td class="text-center" colspan="2">
Transparency of the OGC and participation in its governance<br>
Automatic reporting on donation usage<br>
Online management of contribution contracts<br>
Automation of administrative procedures<br>
Certificate for tax exemption (for private structures)
      </td>
    </tr>
  </tbody>
</table>

# Problematic

::: {.img-medium}
![&nbsp;](images/hamlet-meme.jpg)
:::

:::fragment
::: {.text-center}
**Economically, industrially and democratically &nbsp; <br> &nbsp; a question of life and death...**
:::
:::

## No internet without digital commons

:::{.columns .align-center}

::: {.column width="65%"}
![&nbsp;](images/logos-open-source.png)
:::

::: {.column width="35%" .text-micro}
Could <b>public services</b> today do without all or part of the few free tools shown here (absolutely non-exhaustive list)?

These same tools are also themselves based on more or less low layers of a [multitude of other open source tools]((https://medium.com/graph-commons/analyzing-the-npm-dependency-network-e2cf318c1d0d)) (languages, libraries, databases, standards, etc.).

Some companies whose services rely on these open source building blocks are fully aware of this <b>dependence on an extremely large and diverse eco-system</b>.

Among these companies, some [contribute and redistribute the value they generate](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/), others quietly try to r[e-internalize these building blocks and the people](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/) who master them best .
:::
:::

## The main issues

:::incremental

- **Sustain** and **improve** existing commons
- Helping the **emergence of new digital commons**
- Develop the contribution to the commons as **an economic activity** of tis own
- Strengthen **advocacy** for a society of the commons at a time when the risks of resource exhaustion are increasing
- Anticipate and prevent the risks of loss of digital **sovereignty**
- Specify the place of **public authorities policies** in supporting the commons

:::

## Main considerations

:::incremental

- The sustainability of digital commons poses **infrastructural and strategic, even democratic, challenges**
- Many digital commons are **invisible** because they are in more or less low layers of digital infrastructures
- The economic models of digital commons assume - often - a logic of **valorization of services around the commons** , therefore assuming to be developed ex-post after significant investments in R&D, in time…
- The economic balance of the commons can be based on very **diversified sources of income** : donations, subsidies, volunteering, patronage, etc.
- **Direct support from public actors** can today be available in different forms: calls for projects, subsidies, public procurement, etc.
- The general economy of digital commons and commoners is **unique in the economic landscape** and is also complex to evaluate
:::

<!-- ::::::::: {data-visibility="hidden"} -->
## Some questions

:::incremental

- How can we develop the **digital commons economy** as an **essential** or even **desirable** industrial sector ?
- What if we considered “commoners” (contributors to digital commons) as **authors** , in the sense of copyright?
- What are the **examples** in recent history that echo these issues?
- What **kind of structure** should we invent to respond to these challenges?
- If digital companies have understood the strategic and economic interest of the commons on which they have developed, what is the **level of awareness of public authorities** ?

:::

<!-- ::::::::: -->

## Attention points

:::::: {.columns}

::: {.column}
:::fragment
### Be wary of _business as usual..._
![&nbsp;](images/opensource-money-meme.jpg)

::: {.text-center}
**Consolidate the digital commons economy  &nbsp; <br> &nbsp; by directly remunerating the authors**
:::
:::
:::

::: {.column}
:::fragment
### _Or just shot yourself in the foot_
![&nbsp;](images/opensource-remove-meme.jpg)

::: {.text-center}
**Without digital commons &nbsp; <br> &nbsp; our digital infrastructures will collapse**
:::
:::
:::

::::::

# The commons, condition and driver of innovation

:::{.r-stack .no-margin}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-00.png)
:::
:::{.fragment .fade-in data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-00-bis.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-01.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-02.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-03.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-04.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05-a.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05-b.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-06.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-06-bis.png)
:::
:::

## The risk of ecosystem exhaustion

:::{.img-shadow .opaque .img-no-caption .img-large}
![&nbsp;](images/maintenance-paradox.png)
:::

<br>

:::{.text-micro .text-center}
See the presentation by [Tobie Langel](https://speaking.unlockopen.com/nBXJS5/1-billion-dollars-for-open-source-maintainers) given during [State Of Open Con](https://stateofopencon.com/) 2024.
:::

## Security risk

:::{.columns}

:::{.column width="50%"}
:::{.text-micro}
The security risk narrowly avoided in 2024 on the XZ Utils library ([source](https://www.itpro.com/software/open-source/we-got-lucky-what-the-xz-utils-backdoor -says-about-the-strength-and-insecurities-of-open-source))
:::
:::{.img-shadow .opaque}
![&nbsp;](images/xz-fail.png)
:::
:::

:::{.column width="50%"}
:::{.text-micro}
“A billion for open source”. See the presentation by [Tobie Langel](https://speaking.unlockopen.com/nBXJS5/1-billion-dollars-for-open-source-maintainers) 2024.
:::
:::{.img-shadow .opaque}
![&nbsp;](images/one-billion-maintain.png)
:::
:::

:::

:::{.text-micro .text-center}
The security risks (see the [FOSSEPS report](https://joinup.ec.europa.eu/collection/fosseps/news/fosseps-critical-open-source-software-study-report) from the European Commission in 2022) of our digital infrastructures once again raise the question of the <b>lack of resources allocated to the maintenance</b> of open source projects.
:::

## Systemic risk

:::{.columns}
:::{.column width="50%"}
:::{.fragment .img-shadow .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/curl.png)
:::
:::
:::{.column width="50%"}
:::{.fragment .img-shadow .fade-in data-fragment-index="1"}
![&nbsp;](images/open-ssl.png)
:::
:::
:::

:::{.text-micro .text-center}
:::fragment
Examples found on the [Github sponsors](https://github.com/sponsors) homepage
:::
:::

## The economics of public services and digital commons

:::{.fragment .text-center .semi-fade-out .opaque data-fragment-index="1"}
::: {.img-small}
![&nbsp;](images/2-percents.png)
:::
:::

:::{.fragment .text-center data-fragment-index="1"}
<br>
<b>An engine running on reserve ?</b>
:::

# Some central notions

<br>

:::::: {.columns}
:::{.column .text-center .card width="25%"}
<i class="ri-shield-check-fill"></i><br>
Common **guarantors of infrastructure** and digital **public services**
:::
:::{.column .text-center .card width="25%"}
<i class="ri-building-3-fill"></i><br>
The commons as an **industrial sector**
:::
::::::

:::::: {.columns}
:::{.column .text-center .card width="25%"}
<i class="ri-quill-pen-fill"></i><br>
The commoner as an **author**
:::
:::{.column .text-center .card width="25%"}
<i class="ri-money-euro-circle-fill"></i><br>
**Pooling** of funds and management
:::
::::::

# Why 2% of public procurement?

:::::: {.columns}
:::{.column .text-center .card width="33%"}
:::fragment
A **simple** slogan
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Existing **regulatory and legal frameworks**
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
**Acceptable amounts** on public development budgets
:::
:::
::::::

:::::: {.columns}
:::{.column .text-center .card width="33%"}
:::fragment
A **swarmable** mechanism for scaling up
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Raise awareness of **sovereignty issues**
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Develop the **role of the public actor** in supporting the commons
:::
:::
::::::

## Simple slogan and mecanism

The ambition is to reach certain <b>orders of magnitude in terms of amounts collected</b>, so that these are sufficient to irrigate and consolidate an industrial economy of the commons.

This ambition therefore requires addressing numerous interlocutors in the administration and in the private sector, each with **different concerns, professions, priorities or action frameworks**.

In order to be able to <b>generalize the principle of collection and pooling</b> for support of the commons, it is therefore necessary to offer all sponsors a mechanism that is **simple to understand** from the outset and **simple to implement** , whatever their administrative affiliation or their sector of activity.

## Framing figures and orders of magnitude

:::::: {.columns}

::: {.column .text-micro width="55%"}

<b>Overview of major French State IT projects</b>
<br>September 2023, source [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/panorama-des-grands-projets-numeriques-de-letat/)
<br>For a total of approximately **3,625 million euros**

::: {.table-micro}

| Ministère nom complet                                                                 | Nom du projet              | Début        | Durée prévisionnelle en année | Phase du projet en cours          | Coût estimé |
|---------------------------------------------------------------------------------------|----------------------------|--------------|-------------------------------|-----------------------------------|-------------|
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | PAYSAGE                    | octobre 14   | 9,5                           | Déploiement / Bilan intermédiaire | 53,1        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | CFVR                       | juillet 13   | 9,5                           | Terminé                           | 34,5        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | PILAT                      | juin 18      | 7,6                           | Conception / Réalisation          | 123,5       |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | GMBI                       | septembre 18 | 5,6                           | Conception / Réalisation          | 35,7        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | NRP (NOUVEAU RÉSEAU DGFIP) | janvier 18   | 5,8                           | Conception / Réalisation          | 38,5        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | ROCSP                      | février 19   | 8,9                           | Expérimentation                   | 96,4        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | 3D                         | février 20   | 3,9                           | Expérimentation                   | 31,9        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | TNCP                       | janvier 20   | 4,4                           | Expérimentation                   | 21,1        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | PCR                        | octobre 19   | 3,2                           | Terminé                           | 52,8        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FRANCE SESAME              | janvier 20   | 4                             | Conception / Réalisation          | 10,9        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FONCIER INNOVANT           | novembre 20  | 3,2                           | Déploiement / Bilan intermédiaire | 33,1        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FICOBA 3                   | novembre 20  | 4                             | Conception / Réalisation          | 21          |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | Chorus – Projet S_4HANA    | septembre 22 | 2,1                           | Conception / Réalisation          | 87          |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FACTURATION ELECTRONIQUE   | mars 21      | 7,8                           | Conception / Réalisation          | 231         |
| Ministère de l'Europe et des Affaires étrangères                                      | SAPHIR                     | décembre 15  | 8                             | Déploiement / Bilan intermédiaire | 10,4        |
| Ministère de la Santé et de la Prévention                                             | SI SAMU                    | septembre 14 | 10,3                          | Conception / Réalisation          | 218,4       |
| Ministère de la Santé et de la Prévention                                             | Mon Espace Santé           | janvier 20   | 4                             | Conception / Réalisation          | 227,4       |
| Ministère de la Santé et de la Prévention                                             | ROR                        | janvier 21   | 5,5                           | Conception / Réalisation          | 24,9        |
| Ministère de la Santé et de la Prévention                                             | SI APA                     | juin 22      | 3,6                           | Conception / Réalisation          | 63,4        |
| Ministère de l'Agriculture et de la Souveraineté alimentaire                          | EXPADON 2                  | janvier 13   | 11                            | Déploiement / Bilan intermédiaire | 30,9        |
| Ministère de la Culture                                                               | MISAOA                     | juin 20      | 4                             | Conception / Réalisation          | 11,2        |
| Ministère des Armées                                                                  | ARCHIPEL                   | avril 15     | 8,8                           | Déploiement / Bilan intermédiaire | 14,3        |
| Ministère des Armées                                                                  | SOURCE WEB                 | janvier 14   | 9,8                           | Conception / Réalisation          | 15,3        |
| Ministère des Armées                                                                  | ROC                        | mars 16      | 8,1                           | Conception / Réalisation          | 15,6        |
| Ministère des Armées                                                                  | EUREKA                     | octobre 17   | 6,4                           | Conception / Réalisation          | 21,9        |
| Ministère des Armées                                                                  | SSLD-II                    | novembre 20  | 2,8                           | Conception / Réalisation          | 28,8        |
| Ministère des Armées                                                                  | SPARTA                     | février 18   | 6,3                           | Conception / Réalisation          | 15,8        |
| Services du Premier Ministre                                                          | NOPN                       | janvier 21   | 5,5                           | Conception / Réalisation          | 26,9        |
| Ministère de l'Éducation nationale et de la Jeunesse                                  | OP@LE                      | septembre 14 | 11                            | Déploiement / Bilan intermédiaire | 91,3        |
| Ministère de l'Éducation nationale et de la Jeunesse                                  | MIGRATION SIRH             | janvier 20   | 5,3                           | Conception / Réalisation          | 57,9        |
| Ministère de l'Éducation nationale et de la Jeunesse                                  | REURBANISATION SIRH        | janvier 20   | 5,3                           | Conception / Réalisation          | 68,7        |
| Ministère de la Transition écologique et de la Cohésion des territoires               | GEOPLATEFORME              | janvier 19   | 5,4                           | Déploiement / Bilan intermédiaire | 23,2        |
| Ministère de la Transition écologique et de la Cohésion des territoires               | OCSGE                      | septembre 19 | 5,8                           | Déploiement / Bilan intermédiaire | 30,4        |
| Ministère de la Transition écologique et de la Cohésion des territoires               | GUNENV Phase 2             | mars 23      | 4                             | Conception / Réalisation          | 37          |
| Ministère de l'Intérieur et des Outre-mer                                             | MCIC-2                     | février 15   | 9,9                           | Expérimentation                   | 24,2        |
| Ministère de l'Intérieur et des Outre-mer                                             | RRF                        | octobre 22   | 8                             | Conception / Réalisation          | 900,3       |
| Ministère de l'Intérieur et des Outre-mer                                             | NexSIS Version 1           | avril 17     | 8,2                           | Déploiement / Bilan intermédiaire | 72          |
| Ministère de l'Intérieur et des Outre-mer                                             | ANEF                       | janvier 18   | 5,9                           | Déploiement / Bilan intermédiaire | 54,5        |
| Ministère de l'Intérieur et des Outre-mer                                             | LOG-MI                     | septembre 17 | 7,8                           | Déploiement / Bilan intermédiaire | 28,1        |
| Ministère de l'Intérieur et des Outre-mer                                             | ERPC                       | mai 19       | 4,8                           | Déploiement / Bilan intermédiaire | 82,8        |
| Ministère de l'Intérieur et des Outre-mer                                             | KIOSQUES - PFSF            | avril 20     | 4                             | Déploiement / Bilan intermédiaire | 26,3        |
| Ministère de l'Intérieur et des Outre-mer                                             | FIN                        | février 20   | 5,3                           | Déploiement / Bilan intermédiaire | 68,3        |
| Ministère de l'Intérieur et des Outre-mer                                             | SIV                        | février 20   | En cadrage                    | Cadrage                           | En cadrage  |
| Ministère de l'Intérieur et des Outre-mer                                             | M@GRH                      | avril 21     | 2,8                           | Déploiement / Bilan intermédiaire | 14          |
| Ministère de la Justice                                                               | ASTREA                     | janvier 12   | 14                            | Conception / Réalisation          | 77,3        |
| Ministère de la Justice                                                               | PORTALIS                   | mars 14      | 12,8                          | Conception / Réalisation          | 98,9        |
| Ministère de la Justice                                                               | PROJAE – AXONE             | juin 17      | 7,1                           | Conception / Réalisation          | 14,5        |
| Ministère de la Justice                                                               | NED                        | janvier 18   | 4,8                           | Terminé                           | 8,7         |
| Ministère de la Justice                                                               | PPN                        | janvier 20   | 6                             | Conception / Réalisation          | 145,1       |
| Ministère de la Justice                                                               | ATIGIP                     | janvier 20   | 4,4                           | Déploiement / Bilan intermédiaire | 43,2        |
| Ministère du Travail, du Plein emploi et de l'Insertion                               | SI EMPLOI                  | novembre 21  | 4                             | Conception / Réalisation          | 33          |
| Ministère du Travail, du Plein emploi et de l'Insertion                               | SI FSE                     | janvier 20   | 6                             | Déploiement / Bilan intermédiaire | 30,1        |

:::
:::

::: {.column .text-micro width="45%"}

<b>Management of major French State digital projects</b>
<br>2020, source [Cour des Comptes](https://www.ccomptes.fr/fr/publications/la-conduite-des-grands-projets-numeriques-de-letat)
<br>an average of **343.631 million euros/year** between 2016 and 2018

![&nbsp;](images/cdc/cdc-07.png)

:::
::::::

## Become aware of sovereignty issues

- Free tools located in the lower and less visible layers of the various digital services set up by public authorities raise **questions of criticality and risks** (see the note from the [Quai d'Orsay](https://www.diplomatie.gouv.fr/IMG/pdf/20200731-note-complete-communs_cle021839.pdf) ): security, technical complexity, updates, maintainability…

- The proper functioning of “ low level ” tools can be an undervalued dimension when **calculating the risks of digital projects**, projects which sometimes have an **infrastructural dimension** ( sovereign cloud , health data, army payroll software, etc.).

- Sometimes critical “ low level ” tools can be **maintained by individuals** outside national borders, **outside the sectoral authority** of the sponsoring administrations, and yet prove essential to the proper functioning of the services implemented ( [examples of cURLor openSSL](https://github.com/sponsors) , or that of [core.js](https://github.com/zloirock/core-js/blob/master/docs/2023-02-14-so-whats-next.md) library).

## Role of the State

### The State as **facilitator for commons**

:::{.text-micro}
Before playing the role of actor or producer of commons, the State can take on as a priority the role of <b>guarantor of the framework</b> for the production of commons of general interest by civil society. This framework is above all legal and legislative (GDPR, RGAA, DMA, etc.) but the role of guarantor of this framework is also played through support and animation actions, as already proposed by public actors such as the ANCT , IGN or DINUM.
:::

### The State as **industrial policies** carrier

:::{.text-micro}
The State, but also all ancillary administrations (agencies) or decentralized (communities), can <b>support the industrial economy and the jobs represented by private actors producing the commons</b>, by perpetuating various direct financing methods including the “2% common”, in addition to other historical modes of support.
:::

## A sustainable financing mechanism

- An **alternative to one-off funding** (grants, calls for projects, ad hoc funds , etc.)

- Taking into account the **orders of magnitude** necessary to support a real economy of the commons

- An ability to “ **scale up** ”: either by playing on the percentage, or by expanding to sectors other than digital

## Implementation to be clarified

- Is the **existing regulatory framework sufficient** ? Is there a legal risk of being accused of distortion of competition?

- Which projects, in which partner administrations, with which allies, to **experiment with now** ?

- What strategy is there **to bring this principle before decision-makers** in the administration?

## Some additional ideas

- Start with the public, expand to the private (or share the effort): would it be interesting (for political or economic reasons) to break down the “2%” into “ **2% = 1% public + 1% private** ”? That is to say 1% of the public order upstream, and 1% of the turnover or invoices of private companies in project management?

- **Putting numbers against ideas** : assessing the amounts of public money currently spent on digital projects to establish a sort of forecast budget

# The interest of copyright in supporting the economy of the commons

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
Digital commons are **collective or collaborative works**
:::
:::{.column .card width="25%"}
Commoners are **authors​**
:::
:::{.column .card width="25%"}
Contributing can be recognized as **work**
:::
:::{.column .card width="25%"}
Managing free copyright requires a **shared** structure
:::
::::::

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
The **digital commons economy** is unique and very diversified
:::
:::{.column .card width="25%"}
Use **existing and community law** as leverage
:::
:::{.column .card width="25%"}
Learn from **past economic and legal experiences**
:::
:::{.column .card .card-transparent width="25%"}
&nbsp;
:::
::::::

## Digital commons are <br> collective or collaborative works

- Digital is a **cultural industry**, just like publishing, cinema or music

- Digital commons (software, databases) can be considered **works of the mind**


## Commoners are *authors*

- Contributors and/or initiators of digital commons can be considered as **authors or co-authors** (at least in France)

- **The notion of author is legally regulated in France**: an author of free software retains economic rights over his work.

- A free/digital common work is potentially a **collective or collaborative work** (hence the notion of co-author)

## Contributing is a job

- Contributing to software or a database is a **high value-added activity**

- You can **combine the fact of being an author and having a salaried activity**

- A free work can be developed by an employee **within a company**
  - The author remains the employee (a legal entity cannot be an author)
  - The company is the owner of the rights, provided that this is specified in a copyright assignment contract.
  - Special case of civil servants developing commons within the framework of their functions.

## Managing free copyrights <br>requires a shared structure

- Just as has been the case in the field of music or publishing, it is in practice **impossible for an individual author to do himself** :

  - **monitoring** all modes of exploitation of its works

  - **defending** its rights with broadcasters

- New free works are initiated by individuals , sometimes as employees in companies but not always.

## The digital commons economy is unique


::: {.text-center}
`"A software is free once it is paid"`
:::

- A free work is - as a first approximation - not intended to directly receive operating income, one of the specificities of digital commons is their **free use and distribution**.

- Financial flows linked to the production of a digital commons can only be found **upstream of their publication**

## Use the law as leverage

A <b>free license</b> does not mean that the author no longer has rights, but:

- what entity exists to **defend these right**s ?

- Even if there were an entity to defend these rights, there are no clearly defined **financial flows** for their support, but on the contrary a jungle of different economic models

## Learn from past experiences

- It is necessary to **defend and promote people/authors**, offer them an income model, to encourage people to contribute to digital commons and above all to earn income from it (partially or totally)

- There already exist and there have been **experiments** for changing the law (global license, universal income, etc.), but these proposals have not always been successful and/or are still not very operational.

- Rather than seeking to add to the law and regulations, it is possible to **rely on existing law**, to draw inspiration from various historical examples such as the [private copy royalties](https://www.culture.gouv.fr/Espace-documentation/Rapports/Rapport-du-Gouvernement-au-Parlement-sur-la-remuneration-pour-copie-privee-octobre-2022), the creation of[collective rights management organisations](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006069414/LEGISCTA000006146355/2222-02-22) such as the SACEM or the SACD, or even the concept of [French "cultural exception"](https://fr.wikipedia.org/wiki/Exception_culturelle)

# <span style="display:none">The Librery</span>

:::{.img-librery .img-no-margin .img-no-caption}
![&nbsp;](images/Librery-logo-baudot-black.png)
:::

:::{.text-center}
<b>A sustainable financing mechanism
  <br>and a collective management organization (CMO)
  <br>by and for the authors of commons
</b>
:::

::::::{.columns .text-center .text-micro}
:::{.column .card .card-secondary width="25%"}
<i class="ri-download-2-fill"></i><br>
**Collect,**<br>**pooling,**<br>and **redistribution** <br>of funds
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-scales-3-fill"></i><br>
A strong requirement for **transparency, non-profit making and parity**
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-share-fill"></i><br>
Diversified forms of **support for the commons and commoners**
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-shield-check-line"></i><br>
The possibility of a **label**
:::
::::::

## General economy

:::{.r-stack}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-03-bis.png)
:::
:::{.fragment .fade-in data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-07.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-08.png)
:::
:::

## OGC missions

- **collect** financial contributions from broadcasters
- **identify** common areas and their authors
- **calculate** what must be distributed (distribution keys)
- **redistribute** in rights / declare to URSSAF (to make life easier for authors)
- **defend** the cause and rights of authors of digital commons
- **promote** the status of authorship of digital commons
- **introduce into the law** clauses equivalent to those relating to the CNC / SACD / etc… (compulsory contribution regime?)

## Financial contributions to support the economy of the commons

For the sake of pooling resources, the collective management organization must be able to <b>receive different types of potentially “incoming” financial flows</b>, public or private money:

::: {.text-micro}
:::{.columns}
:::{.column width="50%"}

**Potential financial flows of public origin**

- Plea for the principle of <b>2% common in public procurement</b> on digital projects (inspired by “ [2% for 2 degrees](https://institut-rousseau.fr/2-pour-2c-resume-executif/) ”, from the Rousseau Institute; or “ [1% for art in France](https://www.culture.gouv.fr/Thematiques/Arts-plastiques/Commande-artistique/Le-1-artistique)”)
- <b>Subsidy-type</b> financial contributions from public institutions (IGN, ADEME, DINUM, etc.)
- Intervention at <b>European level</b> to support digital commons, such as [NGI](https://www.ngi.eu/), [NLnet](https://nlnet.nl/), or projects such as [EDIC](https://digital-strategy.ec.europa.eu/en/policies/edic)
:::

:::{.column width="50%"}
**Potential financial flows of private origin**

- Financial contributions (donation type) from certain <b>private companies</b> benefiting from/disseminating digital commons
- <b>Services</b> such as code audits, sourcing , etc.
- Advocacy for the principle of a mandatory minimum contribution obligation - or <b>royalty</b> - for industries whose activity is based on the extensive exploitation of strategic digital commons

<!-- - Possibilité de proposer à des acteurs privés la **garantie de bon fonctionnement de certaines librairies libres critiques**, en échange d'apports financiers redistribués directement vers les mainteneur.e.s de ces librairies. -->
:::
:::
:::

## Governance conditions

- The commons falling both in the public sphere and in the private sphere, it is desirable that **all parties can be represented** within the OGC, however ensuring good representation of authors/individuals, given that the mission of the OGC is good at defending them

- As a structure defending a sector (the commons) the OGC has more **legitimacy** as a private law structure, provided that the different stakeholders can be fairly represented

- **Include authors** as well as **publishers**, **funders** and **broadcasters** of digital commons into the governance

## Structural conditions

On the basis of the criticisms made of cultural OGCs (see Benhamou) we can list some <b>necessary (but not sufficient) conditions to avoid certain shortcomings</b> of historical OGCs identified by observers and the academic field:

- **Management** fees not exceeding 5% to 10%
- **Monitoring** and automation of author registration and remuneration
- **Capping** of salaries of OGC managers
- **Representation** of all authors (1 person == 1 vote)
- **Total transparency** of governance and operation, particularly regarding the choice and implementation of distribution keys

## Operating equilibrium<br>(assumptions)

::::::{.columns}
:::{.column width="50%"}
**Operating costs**

:::{.text-micro}
With a full team (5 full-time people, see further in the “Reflections on governance” section) with salaries of €4,000 net/month, offices, and some additional services (accounting, subscriptions, services online…) :

as a first approximation the total <b>operating and management cost would be around €550k/year</b>.
:::
:::

:::{.column width="50%"}
**Collect**

:::{.text-micro}
If we maintain operating and management costs around 10%, this means that the <b>OGC must be able to collect €5.5 million/year.</b>

Starting from a base of “common 2%” on digital projects, this collection amount corresponds to a set of <b>digital projects whose cumulative budgets would represent €275 million / year</b> before contribution to the commons.

:::
:::
::::::

**Redistribution**

:::{.text-micro}
In summary, by applying the principle of “common 2%” on a set of digital projects to the tune of €275 million / year, the OGC would be able to redistribute: €5.5 million (collected) - 0.55 million € (management fees)

soit <b>i.e. a redistribution of €4.95 million / year to digital commons.</b>
:::

## Possible legal vehicles

Different types of legal structures can be imagined to fulfill the missions of The Librery, with the common point of being <b>non-profit</b> or at least limited profit.

:::{.text-micro}
- The **Collective Management Organization**
- The **Collective Copyright Management Society**
- The **Foundation**
- The **"Fiducie"** (or _trust_)
- The **Cooperative Society of Collective Interest** (SCIC in France)
- THE **Public Interest Group** (GIP in France)
- The **"Mutuelle"**
- The **Investment fund**
- The **Endowment fund**
- The **Association / NGO** (French 1901 law)
:::

## The label as an incentive

- The existence of a legitimate organization for all stakeholders in the digital commons would make it possible to imagine the creation of a “ **common label** ”, which the organization could issue to projects and structures.

- The notion of “common labels” can be an interesting area to develop to **encourage administrations and businesses to contribute** up to 2% of their digital projects budgets.

- The notion of label raises the questions of **objective criteria** allowing a digital common to be qualified as such.

- The other counterpart of the notion of label would be to be able to **identify and monitor economic valuation indices** (financial, time allocated, savings, etc.) making it possible to better situate the contributions of digital commons within the industrial sector.

## Logo ideas

<br>

:::{.columns .text-micro}
:::{.column width="50%"}
### Inspired from [XKCD n°2347](https://xkcd.com/2347/)

![&nbsp;](images/Librery-logo-long-white.png)

![&nbsp;](images/Librery-logo-long-dots-01.png)
:::
:::{.column width="50%"}
### Inspired from [Baudot codex](https://fr.wikipedia.org/wiki/Code_Baudot)

![&nbsp;](images/code-baudot.png)

![&nbsp;](images/Librery-logo-baudot-black.png)
:::
:::

# Collect and redistribution

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
Distinguish<br>**work,** **author,**<br>**broadcaster,** and **editor**
:::
:::{.column .card width="33%"}
A "**two-sided market**" type approach
:::
:::{.column .card width="33%"}
The notion of **apportionment contracts** to integrate the particularities of the stakeholders
:::
::::::

## Main concepts

The parallels with other cultural industries are as follows:

- **Work** (individual or collaborative): 1 free software (=== 1 song)
- **Author** : 1 developer / designer (=== 1 artist)
- **Broadcaster** : 1 entity/administration deploying free software (=== 1 radio station)
- **Editor** : 1 private company developing a free product (=== 1 production house)

## General mecanism

- **Authors** : authors must register, and declare which free works they contribute to
- **Works** : you need a catalog of works (free software, free databases) on which you can track contributions
- **Broadcasters** : they must declare which works they broadcast, and they must contribute financially to a common fund (via agreements). This contribution can be imagined in different ways, cumulatively:
  - contributions
  - and/or flat-rate contribution
  - and/or flat-rate contribution per community
  - and/or indexation on a budget allocated to the development of common areas

## Distribution of collected funds

:::{.opaque .img-no-caption}
![&nbsp;](images/librery-fund-sankey-en.png)
:::

## A/ Process from the commons broadcasters side

:::::::::::::: {.columns}
::: {.column width="50%"}
![&nbsp;](images/diffusors.png)
:::
::: {.column width="50%"}
::: {.text-micro}

- **A broadcaster** is considered to be a legal or physical entity ready to voluntarily pay a sum in order to remunerate the authors and co-authors of one or more publications.

- A broadcaster signs a **commons apportionment contract** with the OGC, the OGC undertakes to redistribute the amounts voluntarily paid by the broadcaster.

- The contract indicates an **amount** of voluntary contribution on one or more common areas, or even without a particular common area, all over a given period.

- This contract may include **redistribution** clauses and rules

- The contract is then broken down by **amounts allocated per year and per common**.

- All contracts and their associated amounts are **aggregated by common**, this constitutes one fund per common (and per year)

:::
:::
::::::::::::::

## A/ The concept of apportionment contracts

Upstream - that is to say with respect to the financial contributors (process A/) - it would be possible to associate with each financial contribution **apportionment contracts** specifying the different <b>uses of the sums</b>, the <b>conditions and rules</b> of redistribution, as well as the <b>shares allocated</b> to specific common areas or on the contrary without precise marking.

:::{.text-micro}
We could imagine that **each financial contribution** could be associated with a contract specifying:

- **one or more shares marked** on one or more common areas chosen by the contributor/broadcaster, due to their direct interest in delegating maintenance for the proper functioning of their own services.
- **a floor share without signage**, with a view to redistribution chosen by the OGC to other “ low level ” or low visibility commons (share which could for example be donated to entities such as Thanks.dev or equivalent).

:::

## B/ Process from the authors’ side

:::::::::::::: {.columns}
::: {.column width="50%"}
![&nbsp;](images/authors.png)
:::
::: {.column width="50%"}
::: {.text-micro}
- An **author registers** with the OGC, specifying which works/communities he contributes to.

- A **contract for the transfer/management of copyright** is established between the OGC and the author.

- Its **contributions** to commons are calculated and recorded during the year, common by common

- At the end of the year, his contributions make it possible to **calculate the amount of remuneration** allocated to him, common by common, according to the associated fund and the redistribution rules.

- The OGC establishes its **copyright declaration** with the authorities and sends the proof of declaration to the author

- _Note_ : the process on the author's side is the one that needs to be as **fast and automated** as possible in order to avoid any administrative burden and exponential management costs.
:::
:::
::::::::::::::

## C/ Special case of digital commons editors

::: {.text-micro}
- A commons editor employs **one or more employees, considered to be the initial authors** of the published commons

- The editor is considered the **manager of the copyright of its employees**.

- The editor allows the **management of rights in his name by the OGC** with the URSSAF

- The editor **registers its employees as authors** on the OGC

- The OGC calculates and declares the remunerations to the authors as in B/, with the particularity of **declaring these sums to the authorities in the name of the editor** also but sends the documents to the editor

:::

## Simplified full data model

See the markdown [document](https://gitlab.com/Julien_P/the-librery-slides/-/blob/main/Model.md) on the repository.

:::::::::::::: {.columns}

::: {.column}
::: {.text-micro}
Part 1/2
:::
::: {.img-no-margin}
![&nbsp;](images/model-simplified-01.png)
:::
:::

::: {.column}
::: {.text-micro}
Part 2/2
:::
::: {.img-no-margin}
![&nbsp;](images/model-simplified-02.png)
:::
:::

::::::::::::::

# Thoughts on the governance

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
A representation of **all commons stakeholders**
:::
:::{.column .card width="25%"}
A structural majority for the **authors**
:::
:::{.column .card width="25%"}
**Human and technical resources** for the implementation of missions
:::
:::{.column .card width="25%"}
Strong **transparency** and **non-profit** requirements
:::
::::::

## Funding digital commons as a common

::: {.opaque .img-xl .img-no-caption .img-no-margin}
![&nbsp;](images/diagrams/Handler.ashx_.jpeg)
:::

::: {.text-micro .text-center}
<i class="ri-book-2-line"></i>
See the book <b>"Governing the commons"</b> by [Elinor Orstom](https://en.wikipedia.org/wiki/Elinor_Ostrom)
:::

## General diagram

:::::: {.columns}

::: {.column width="60%"}
![&nbsp;](images/global.png)
:::

::: {.column width="40%"}
::: {.text-micro}
General principle diagram of The Librery (made with[Whimsical](https://whimsical.com/the-librery-organigramme-RimouvNB2jVq7pcu59zP4H))

From top to bottom :

- **The stakeholders** :
  - Authors
  - Works (common)
  - Broadcasters
  - Editors
  - Sympathizers
- **The OGC team**
- **The pool fund** and its different uses
  - Rights management
  - Targeted aid
  - Programs / actions
  - Promotion of the commons
- **The available tools**
  - Web portal (+ backoffice)
  - Third-party services

:::
:::

::::::

## The stakeholders

![&nbsp;](images/stakeholders.png)

## The financial means: the fund

![&nbsp;](images/fund.png)

## The human resources: the team

![&nbsp;](images/team.png)

## The technical means: tools

![&nbsp;](images/tools.png)

# Internationalisation & decentralization

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
Being able to adapt to **local legal specificities**
:::
:::{.column .card width="33%"}
Being able to **share datasets** (registers of authors or commons)
:::
:::{.column .card width="33%"}
Have a **decentralized information architecture**
:::
::::::

## Why anticipating an international approach?

#### The constraints of the production of digital commons

::: {.text-micro}
- Digital commons can be developed <b>anywhere in the world</b>.
- The <b>commoners</b> contributing to the same common can themselves be <b>based in different countries</b>.
- The <b>financial contributors to the commons</b> can also be based in different countries.
- An OGC is necessarily legally based in a country, <b>the legal status of an OGC can therefore be different from one country to another</b> (foundation, trust, association, trust, etc.).
- An OGC does not necessarily have the vocation - or the capacity - to deal with all of the commons at the global level, <b>it is more realistic to envisage that an OGC has a precise territorial and/or sectoral anchoring</b>.

It is therefore necessary that all CMOs throughout the world can **share common registers to avoid duplication and legal inconsistencies**: register of commons, register of commoners, amounts of financial contributions, allocation of sums towards the commons and the commoners...
:::

## Decentralized architecture

::: {.text-micro}
- It would be possible - or even more realistic - to have **several OGCs distributed throughout the world**, and/or different OGCs "specialized" in certain verticals (industrial sector, programming language, etc.)

- For such a system to operate on an international scale, the **information infrastructure must be shared**, while allowing each OGC to manage its own corpus.

- Technically it would be possible to offer all OGCs a free digital management tool based on [ActivityPods](https://activitypods.org) in order to be able to both **decentralize data storage**, while allowing **each instance to manage its own datasets**.

:::

## Principle diagram

:::{.r-stack .no-margin}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/decentralized/diagram-01.png)
:::
:::fragment
![&nbsp;](images/diagrams/decentralized/diagram-02.png)
:::
:::fragment
![&nbsp;](images/diagrams/decentralized/diagram-03.png)
:::
:::fragment
![&nbsp;](images/diagrams/decentralized/diagram-99.png)
:::
:::

# Scenarios

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
A **simple mechanism**
:::
:::{.column .card width="33%"}
**Contractual** approach
:::
:::{.column .card width="33%"}
Different approaches depending on **public or private actors**
:::
::::::

## Argument grid <br>for financial contributors

<table class="grid">
  <thead>
    <tr>
      <th></th>
      <th class="text-center"><b>PUBLIC</b></th>
      <th class="text-center"><b>PRIVATE</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>**CONTRIBUTION**</td>
      <td>
        2% of the amount <br>of digital projects
      </td>
      <td>
        Depending on the type and size of the business:<br>
        - x % of annual recurring revenue<br>
        - x % of management surplus / annual profits<br>
        - x € yearly by developer<br>
        - fixed sum annually
      </td>
    </tr>
    <tr>
      <td>**OPTIONS**</td>
      <td>
One-off agreement <br>
Multi-annual agreement <br>
Agreements per project<br>
(or) with an administration
      </td>
      <td>
One-off or multi-annual agreement <br>
Donations<br>
Pledge of donations
      </td>
    </tr>
    <tr>
      <td>**CONTRACTUALISATION**</td>
      <td class="text-center" colspan="2">
45% of the donation: direction by the structure towards one or more common areas<br>
45% of the donation: direction by OGC towards one or more communities<br>
10% of the donation: OGC management fees
      </td>
    </tr>
    <tr>
      <td>**COUNTERPARTS**</td>
      <td class="text-center" colspan="2">
Critical commons maintenance insurance<br>
Labeling <br>
Team empowerment<br>
Choice by structure to direct towards common areas<br>
Choice by structure to specify certain remuneration mechanisms (distribution keys)<br>
Tax exemption (if private structure)
      </td>
    </tr>
    <tr>
      <td>**FUNDS MANAGEMENT**</td>
      <td class="text-center" colspan="2">
Transparency of the OGC and participation in its governance<br>
Automatic reporting on donation usage<br>
Online management of contribution contracts<br>
Automation of administrative procedures<br>
Certificate for tax exemption (for private structures)
      </td>
    </tr>
  </tbody>
</table>

# Inspirations

:::{.text-micro}
- The existing mechanism of  "**artistic 1%**" in  [public construction (in France)](https://www.culture.gouv.fr/Thematiques/Arts-plastiques/Commande-artistique/Le-1-artistique)

- Proposal "**2% for 2 degrees**" from the [Institut Rousseau](https://institut-rousseau.fr/2-pour-2c-resume-executif/)

- Different initiatives coming from the world of free software or institutions:
:::

:::{.columns .text-nano}
:::{.column width="60%"}

- [AFNIC](https://www.afnic.fr/en/)
- [Compte commun des Communs](https://wiki.resilience-territoire.ademe.fr/wiki/LE_COMPTE_COMMUN_DES_COMMUNS)
- [Contributive.org](https://contributive.org/fr/)
- [Copie Publique](https://copiepublique.fr/) by Code Lutin
- [Drips](https://www.drips.network/)
- [Document Foundation](https://www.documentfoundation.org/) by and for Libre Office
- [Fiducies d'intérêt social](https://tiess.ca/nouveaux-outils-fus/) by [TIESS](https://tiess.ca/)
- [Fondation Blue Hat](https://bzg.fr/le-logiciel-libre-a-besoin-d-une-vraie-strategie-de-mutualisation-au-sein-de-letat/)
- [Fondation Wikimedia](https://wikimediafoundation.org/fr/) by and for Wikimedia
- [Fonds de Dotation du Libre](https://www.fdl-lef.org/)
- [FOSS funders](https://fossfunders.com/)
- [Fund for Defense of Net Neutrality](https://fdn2.org/en/presentation.html)
- [Github sponsors](https://github.com/sponsors)
- [KDE e.V](https://ev.kde.org/)
- [Libera Pay](https://liberapay.com/)
- [NLnet](https://nlnet.nl/) - [NGI e-Commons Fund](https://nlnet.nl/news/2023/20230719-eCommonsFund.html)
- [Oceco](https://movilab.org/wiki/Oceco) by Communecter
:::
:::{.column width="40%"}
- [Open Collective](https://opencollective.com/opensource)
- [Open Patent](https://www.boldandopen.com/open-patent)
- [Open Source Collective](https://oscollective.org/)
- [Open Source Initiative](https://members.opensource.org/donate/)
- [Open Source Pledge](https://osspledge.com/)
- [Open Source Security Foundation](https://openssf.org/)
- [Open Technology Fund](https://www.opentech.fund/)
- [OWASP Foundation](https://owasp.org/donate/)
- [Post-Open licence](https://perens.com/?s=post-open) by [Bruce Perens](https://perens.com)
- [Proton Lifetime Fundraiser](https://proton.me/blog/lifetime-fundraiser-survey-2023)
- [Prototype Fund](https://prototypefund.de/en/) (Allemagne)
- [SARD - Société de Répartition des Dons](https://framablog.org/2009/09/05/sard-hadopi/)
- [Software Freedom Conservancy](https://sfconservancy.org/)
- [Sovereign Tech Fund](https://sovereigntechfund.de/en/) (Allemagne)
- [Stakes.social](https://stakes.social/)
- [Thanks.dev](https://thanks.dev/home)
- [TOSIT - The Open Source I Trust](https://tosit.fr/)

:::
:::

## Focus on the Public Copy initiative

:::{.columns}
:::{.column width="15%"}
![&nbsp;](images/copie-publique-logo.svg)
:::

:::{.column .text-micro .text-no-margin-top width="85%"}
The [Public Copy](https://copiepublique.fr/) initiative consists of the description of a **mechanism for allocating and then redistributing funds** to open source projects, which a company can freely choose to implement provided that it respects its form and spirit. .

The system proposed by **Public Copy is not a fund shared by several companies** : each company choosing to adopt the Copie Publique mechanism contributes alone to its own fund, on the basis of an annual base that it chooses and makes public. A company can thus devote, for example, 1% of its turnover, or 3% of its net management surplus, or even a fixed amount per year. The only condition is that the rule is clear, transparent, and allows contributions to the fund on a recurring basis.

Different aspects of the Public Copy mechanism are interesting to highlight, in particular those relating to the **process of selecting supported projects** chosen by the company Code Lutin and the dynamics that this process generates:
:::
:::

:::{.text-nano-grey}
:::{.columns}
:::{.column width="50%"}
The choice of supported projects is broken down into 2 phases in parallel, dividing the sum to be initially redistributed into two separate envelopes:

- <b>a collective phase</b>, aimed at identifying and choosing a reduced number of projects through a “call for applications”: candidates send a description of the project for which they are requesting support, and also specify their needs. The collective chooses between 1 and 3, following the establishment of a jury.
- <b>a more individual phase</b>, where each employee has the possibility of redistributing an envelope to one or more projects of their choice (from the call for applications or not). This phase makes it possible to support more projects of smaller sizes.
:::
:::{.column width="50%"}
<b>All the company's employees are involved</b> during the process of identification, evaluation, selection, and remuneration of the projects to be supported. Each person - individually or collectively - can thus actively contribute to supporting free third-party projects, a freedom that does not exist in any company.

Following the call for applications, one of the moments described as the most exciting is that of the <b>meeting (face-to-face or video) with the candidates</b>. This stage allows members of the company to discover individuals and original technologies, in a direct manner while retaining the human and social dimension of the projects.
:::
:::
:::

:::{.text-nano-grey}
_A warm thank you to the members of [Code Lutin](https://www.codelutin.com/) with whom the author was able to exchange in Nantes, and who were able to describe their Public Copy initiative with precision and passion_
:::

## Focus on the "post-open" licence

:::{.columns}
:::{.column width="40%"}

:::{.r-stack .no-margin}
:::{.fragment .semi-fade-out .img-no-caption .opaque data-fragment-index="1"}
![&nbsp;](images/post-open/postopen-01.png)
:::
:::{.fragment .img-no-caption .fade-in data-fragment-index="1"}
![&nbsp;](images/post-open/postopen-02.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-03.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-04.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-05.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-06.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-07.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-08.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-09.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-10.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-11.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-12.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-13.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-14.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-15.png)
:::

:::

:::

:::{.column .text-micro .text-no-margin-top width="60%"}
The [Post-Open license](https://perens.com/2024/03/08/post-open-license-first-draft/) proposed by [Bruce Perens](https://wikipedia.org/wiki/Bruce_Perens) consists of making it contractually obligatory for companies generating more than 5 million USD per year using and/or modifying the source code to: either revert the code under the same license ("POST-OPEN OPERATING AGREEMENT"), or establish a remuneration contract for people or entities responsible for maintaining or improving the source code (“POST-OPEN PAID CONTRACT”).

The screenshots in this slide are extracted from this [Bruce Perens' video](https://www.youtube.com/watch?v=vTsc1m78BUk&list=PLfXKG51wk2sTBeAJtuL8wsUNvD-6lVDPM&index=3&t=1704s). Another [video from 2024](https://youtu.be/suUfS0-p5Yg?si=YRjwoLmnUXaToVqr) gives additionnal updates.

_A warm thank you to Bruce Perens with whom the author was able to exchange extensively on the topic_

:::
:::

**Extract of the article 3.2 (PAID CONDITIONS) of the post-open licence draft**

:::{.text-nano-grey}
:::{.columns}
:::{.column width="50%"}
a)  The end-user revenue through all legal entities in which YOU have
    ownership exceeding 5% or equivalent control without ownership exceeds
    USD$5 Million anually. End-user revenue is all money or other value
    collected from customers, including the financial value equivalent of
    any non-monetary remuneration such as barter or the grant of rights or
    privileges.

b)  You provide, for remuneration, any work in the POST-OPEN COLLECTION to
    others, other than PERSONAL USE, or perform that provision at the order
    of another legal entity that receives remuneration for it. This includes
    (but is not limited to) provision of the work as a service; or inclusion
    of the work in a product that is sold, for example software that is sold
    or sale of a device containing the WORK.
:::

:::{.column width="50%"}
c)  You make MODIFICATIONS to any WORK in the POST-OPEN COLLECTION without
    performing one of these actions (you may perform both):

    I)  You enter into the POST-OPEN OPERATING AGREEMENT and make a PUBLIC
        RELEASE of the MODIFICATION.

    II) You enter into the POST-OPEN PAID CONTRACT.
:::
:::
:::

# How can we continue <br>this reflection?

:::{.text-micro}

<i class="ri-share-line mr-1" style="color: black;"></i>
**Share and debate** these ideas with commons stakeholders

<i class="ri-edit-2-line mr-1" style="color: black;"></i>
**Contribute** on the [pad](https://hackmd.io/@Jpy/the-librery) open in writing and comments

<i class="ri-list-unordered mr-1" style="color: black;"></i>
**Identify** experiments in pooling financial resources to support the commons

<i class="ri-map-pin-line mr-1" style="color: black;"></i>
**Map** the actors / broadcasters / producers / products

<i class="ri-focus-3-line mr-1" style="color: black;"></i>
 Identify, classify and prioritize a shortlist of **strategic commons** for the State or communities

<i class="ri-message-2-line mr-1" style="color: black;"></i>
Collect **testimonials and new ideas** from digital commons producers

<i class="ri-pages-line mr-1" style="color: black;"></i>
Bring together these ideas, documentation, testimonials on an online **website**

:::

# References and documentation

Some bibliographical elements, classified by media and major themes, in addition to general notes relating to reflections around [The Librery](https://hackmd.io/@Jpy/the-librery).

<i class="ri-message-2-line"></i> [Channel on Matrix](https://matrix.to/#/#the-librery:multi.coop) for chatting

<i class="ri-file-pdf-2-line"></i> [Articles / books / pdf (nextcloud)](https://nuage.liiib.re/s/dx5eribMSRm6RTm) in open access

<i class="ri-book-2-line"></i> [Bibliographies (Zotero)](https://www.zotero.org/groups/5255274/librery/library)

<i class="ri-file-text-line"></i> [Note pad](https://hackmd.io/@Jpy/the-librery) open for writing and comments

<i class="ri-movie-line"></i> [Videos / conferences (playlist Youtube)](https://www.youtube.com/playlist?list=PLfXKG51wk2sTBeAJtuL8wsUNvD-6lVDPM)

<i class="ri-gitlab-line"></i> [Source files](https://gitlab.com/Julien_P/the-librery-slides/-/tree/main/texts) et [entire presentation](https://gitlab.com/Julien_P/the-librery-slides) on Gitlab

## Other notable online resources

:::{.text-nano}
:::{.columns}

:::{.column width="50%"}

- ADEME (wiki) : [Résilience des territoires](https://wiki.resilience-territoire.ademe.fr/wiki/Accueil)
- ADEME (forum) : [Sobriété & Résilience des territoires](https://forum.resilience-territoire.ademe.fr/)
- ADULLACT (site) : [L'association](https://adullact.org/)
- AFULL - Association Francophone des Utilisateurs de Logiciels Libres (site) : [L'association](https://aful.org/)
- AIC - Accélérateur d'Inititaves Citoyennes : [Site internet](https://communs.beta.gouv.fr/)
- ANCT (site) : [Labo Société Numérique](https://labo.societenumerique.gouv.fr/fr/recherche?thematics=communs)
- Annuaire des Communs (site) : [Annuaire](https://annuaire.lescommuns.org/category/collectifs/)
- L'Assemblée des communs (site) : [Site internet](https://assemblee.lescommuns.org/)
- Chaire Socio Economie des Communs (univ. Lille) : [Laboratoire CLERSE](https://clerse.univ-lille.fr/)
- La Chambre des Communs (site) : [Site internet](https://chambre.lescommuns.org/gouvernance/)
- Les communs d'abord ! (média) : [Site internet](https://www.les-communs-dabord.org) / [Chat](https://chat.lescommuns.org/home)
- La Comptoir du Libre (site) : [Catalogue](https://comptoir-du-libre.org/fr/)
- Contributive Commons (licence) : [Portail](https://contributivecommons.org/)
- La Coop des Communs (site) : [L'association](https://coopdescommuns.org/fr/association/)
- Collectif de Recherche sur les Initiatives, Transformations et Institutions des Communs (site) : [Site internet](https://critic-communs.ca/)
- Digital Transparency Lab (non-profit organisation) : [Site internet](https://www.transparencylab.ca/)
- En communs (revue) : [Site internet](https://www.encommuns.net/)
- La Fabmob (pdf) : [Socle de compréhension des communs numériques de la mobilité](https://cloud.fabmob.io/s/L3gJsa3EnYQNSTk)
- La Fabrique à communs des Tiers-lieux (wiki) : [Movilab](https://movilab.org/wiki/Fabrique_%C3%A0_communs_des_tiers-lieux)
- Liraries.io (site) : [Site internet](https://libraries.io/)
- FOSS Sustainability : [Site internet](https://fosssustainability.com/)

:::

:::{.column width="50%"}

- Funding the commons (rencontres) : [Site internet](https://fundingthecommons.io/)
- Green Software Foundation : [Site internet](https://greensoftware.foundation/)
- IGN (wiki) : [Le Commun des communs](https://interlab.cc/wikign/?LeCommundescommuns)
- IGN (pdf) : [Guide des communs](https://www.ign.fr/files/default/2023-10/guide_communs_ouishare.pdf)
- Imaginaire Communs (revue) : [Site internet](https://anis-catalyst.org/communs/imaginaire-communs/) 
- Inno<sup>3</sup> (outil) : [Canevas pour la gouvernance de communs numériques](https://inno3.fr/realisation/canevas-pour-la-gouvernance-de-commun-numerique/)
- Matti Schneider (site) : [Construire des communs numériques](https://communs.mattischneider.fr/)
- NEC - Numérique en commun\[s\] (site) : [Ressources](https://numerique-en-communs.fr/les-ressources-nec/)
- Numérique d'Intérêt Général - Cadre de référence : [Site internet](https://www.numeriqueinteretgeneral.org/)
- Open Future : [Publication](https://openfuture.eu/publication/european-public-digital-infrastructure-fund/)
- Open Infra : [Site internet](https://openinfra.dev/)
- Open North : [Site internet](https://opennorth.ca/)
- Open Source Initiative : [Site internet](https://opensource.org/)
- Organisations funding the commons (curated list by Jaime Arredondo) : [Page Notion](https://boldandopen.notion.site/Organisations-Funding-the-Commons-Open-source-projects-93c482ba51d7498eac4ecc50275b9d7f)
- P2P Foundation (site) : [Site internet](https://p2pfoundation.net/)
- Le portail des communs (site) : [Portail](https://lescommuns.org/)
- "Qu'est-ce qui vient après l'open source ?" article de Mathis Lucas sur une présentation de [Bruce Perens](https://en.wikipedia.org/wiki/Bruce_Perens) : [article et vidéo](https://programmation.developpez.com/actu/352421/Qu-est-ce-qui-vient-apres-l-open-source-Un-pionnier-du-mouvement-de-l-open-source-affirme-qu-il-faut-changer-de-paradigme-et-trouver-un-moyen-equitable-de-remunerer-les-developpeurs/)
- S.I.Lex (blog) : [Site internet](https://scinfolex.com/)
- SILL - Socle Interministériel des Logiciels Libres (site) : [Catalogue](https://code.gouv.fr/sill/)
- Sustain OSS (podcast) : [Site internet](https://podcast.sustainoss.org/)
- Transcendance (article) : [Site internet](https://democratie-action.notion.site/Transcendance-outil-opensource-et-collaboratif-pour-l-mergence-de-mod-les-conomiques-d-int-r-t-g--0ce0f7039dc04661921c9416a35b9796)
- Le wiki des communs (wiki) : [lescommuns.org](https://wiki.lescommuns.org/)
- La 27<sup>ème</sup> Région (site) : [Enacting the commons](https://enactingthecommons.la27eregion.fr/)

:::

:::
:::

## Press review

:::{.text-nano}
<i class="ri-message-2-line"></i> [Matrix channel for the press review & survey](https://matrix.to/#/%23librery-press:multi.coop)
:::

#### 2024

:::{.text-nano}

- 2024-11-19 / Github : ["Announcing GitHub Secure Open Source Fund: Help secure the open source ecosystem for everyone"](https://github.blog/news-insights/company-news/announcing-github-secure-open-source-fund/)
- 2024-11-14 / IEE Spectrum : ["Open-Source Software Is in Crisis"](https://spectrum.ieee.org/open-source-crisis)
- 2024-11-10 / TechCrunch: ["Open source projects draw equity-free funding from corporates, startups, and even VCs"](https://techcrunch.com/2024/11/10/open-source-projects-draw-equity-free-funding-from-corporates-startups-and-even-vcs/)
- 2024-10-15 / FLOSS fund: ["Announcing FLOSS Fund: $1M per year for free and open source projects"](https://floss.fund/blog/announcing-floss-fund/)
- 2024-09-30 / People Vs BigTech : ["Beyond Big Tech: A manifesto for a new digital economy"](https://peoplevsbig.tech/beyond-big-tech-a-manifesto-for-a-new-digital-economy/)
- 2024-09-19 / The Register : ["Kelsey Hightower: If governments rely on FOSS, they should fund it"](https://www.theregister.com/2024/09/19/kelsey_hightower_civo)
- 2024-05-24 / Public Interest Digital Innovation : ["Beyond big tech regulation : funding & scaling public good"](https://player.clevercast.com/?account_id=AkN3yR&item_id=2O3mE6)
- 2024-05-04 / Sequoia : ["New Fellowship: How Sequoia is Supporting Open Source"](https://www.sequoiacap.com/article/sequoia-open-source-fellowship/)
- 2024-04-17 / UK Day One : ["A UK Open-Source Fund to Support Software Innovation and Maintenance"](https://ukdayone.org/briefings/a-uk-open-source-fund)
- 2024-04-04 / Matrix.org : ["Open Source Infrastructure must be a publicly funded service."](https://matrix.org/blog/2024/04/open-source-publicly-funded-service/)
- 2024-04-03 / ITPro : [""We got lucky": What the XZ Utils backdoor says about the strength and insecurities of open source"](https://www.itpro.com/software/open-source/we-got-lucky-what-the-xz-utils-backdoor-says-about-the-strength-and-insecurities-of-open-source)
- 2024-02-16 / Jacobian.org : ["Paying people to work on open source is good actually"](https://jacobian.org/2024/feb/16/paying-maintainers-is-good/)
- 2024-01-19 / openpath.chadwhitacre.com : ["The Open Source Sustainability Crisis"](https://openpath.chadwhitacre.com/2024/the-open-source-sustainability-crisis/)

:::

---

#### 2023

:::{.text-nano}

- 2023-12-28 / Mathis Lucas : ["Qu'est-ce qui vient après l'open source ?"](https://programmation.developpez.com/actu/352421/Qu-est-ce-qui-vient-apres-l-open-source-Un-pionnier-du-mouvement-de-l-open-source-affirme-qu-il-faut-changer-de-paradigme-et-trouver-un-moyen-equitable-de-remunerer-les-developpeurs/)
- 2023-12-27 / linuxfr.org : ["La mission logiciels libres (DINUM) propose 4 prix BlueHats de 10000€ pour soutenir des mainteneurs"](https://linuxfr.org/news/la-mission-logiciels-libres-dinum-propose-4-prix-bluehats-de-10000-pour-soutenir-des-mainteneurs)
- 2023-11-09 / lemmy.ndlug.org : ["GNOME Recognized as Public Interest Infrastructure"](https://lemmy.ndlug.org/post/367541)
- 2023-10-24 / blog.sentry.io : ["We Just Gave $500,000 to Open Source Maintainers"](https://blog.sentry.io/we-just-gave-500-000-dollars-to-open-source-maintainers/)
- 2023-09-17 / nikclayton.writeas.com : ["Stepping back from the Tusky project"](https://nikclayton.writeas.com/update-5-on-stepping-back) serie
- 2023-07-27 / Serkan Holat : ["Open source public fund experiment - One and a half years update"](https://dev.to/coni2k/open-source-public-fund-experiment-one-and-a-half-years-update-367d)
- 2023-02-14 / Denis Pushkarev (core-js) : ["So, what's next ?"](https://github.com/zloirock/core-js/blob/master/docs/2023-02-14-so-whats-next.md)

:::

#### 2022

:::{.text-nano}

- 2022-12-14 / Startin' Blox : ["Co-funding in open source #1"](https://startinblox.com/en/2022/12/14/co-funding-in-open-source-1/)
- 2022-06-21 / Open Futuress : ["Digital commons are a pillar of european digital sovereignty"](https://openfuture.eu/blog/digital-commons-are-a-pillar-of-european-digital-sovereignty/)
- 2022-06 / diplomatie.gouv.fr (blog) : ["Les communs au service d’un modèle européen de souveraineté numérique non hégémonique"](https://www.diplomatie.gouv.fr/fr/politique-etrangere-de-la-france/diplomatie-numerique/blog-de-l-equipe/article/les-communs-au-service-d-un-modele-europeen-de-souverainete-numerique-non)
- 2022-05-16 / Tech Crunch : ["Tech giants pledge $30M to boost open source software security"](https://techcrunch.com/2022/05/16/white-house-open-source-security/)
- 2019-07-17 / unrealengine.com : ["Epic Games supports Blender with $1.2 million Epic MegaGrant"](https://www.unrealengine.com/en-US/blog/epic-games-supports-blender-with-1-2-million-epic-megagrant)
- 2018-06-24 / scinfolex.com : ["Les Communs numériques sont-il condamnés à devenir des « Communs du capital » ?"](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/)
- 2016-04-05 / Burak Arikan : ["Analyzing the NPM dependency network"](https://medium.com/graph-commons/analyzing-the-npm-dependency-network-e2cf318c1d0d)

:::

## Credits

Presentation realized with :

- [Pandoc](https://pandoc.org/)
- [Reveal.js](https://revealjs.com/)
- [Remix Icons](https://remixicon.com/)
- [Slider-template](https://gitlab.com/multi-coop/slider-template)

# Thanks

**The comments published in this presentation are those of the author alone.**

However, the author would like to send a <b>big thanks</b> to all the people who contribute to enriching this reflection and <b>supporting the movement for a society of the commons</b>.

::::::{.columns}

:::{.column}
::: {.text-nano-grey .text-center .text-compact}

**Proofreadings or comments**

<br>

:::{.columns}

:::{.column width="50%"}

[Luis Roman Arciniega Gil](https://www.linkedin.com/in/luis-roman-arciniega-gil/)

[Vincent Bachelet](https://www.linkedin.com/in/vincent-bachelet-831663aa/)

[Alex Bourreau](https://www.linkedin.com/in/alexbourreau/)

[Jeanne Bretécher](https://www.linkedin.com/in/jeanne-bret%C3%A9cher-267b0110/)

[Brendan Brieg Le Ny](https://www.codelutin.com/lutins)

[Sébastien Broca](https://www.linkedin.com/in/sebastien-broca-96b39a264/)

[Olivier Charbonneau](https://www.linkedin.com/in/olivier-charbonneau-b210491/)

[Benoît De Haas](https://www.linkedin.com/in/beno%C3%AEt-de-haas-78141653/)

[Maïa Dereva](https://blog.p2pfoundation.net/author/maia-dereva)

[Ludovic Dubost](https://www.linkedin.com/in/ldubost)

[Margot Godefroi](https://www.linkedin.com/in/margot-godefroi-40689b154/)

[Marguerite Grandjean](https://www.linkedin.com/in/margueritegrandjean/)

[Bastien Guerry](https://bzg.fr/)
:::

:::{.column width="50%"}
[Serkan Holat](https://www.linkedin.com/in/serkanholat/)

[Alain Imbaud](https://www.linkedin.com/in/alain-imbaud-7632b22a/)

[Nicolas Jullien](https://www.linkedin.com/in/nicolasjullien/)

[Laure Kassem](https://www.linkedin.com/in/laure-kassem/)

[Arnaud Levy](https://www.linkedin.com/in/arnaudlevy/)

[Tibor Katelbach](https://www.linkedin.com/in/tiborkatelbach/)

[Nicolas Loubet](https://www.linkedin.com/in/nicolasloubet/)

[Thomas Menant](https://www.linkedin.com/in/thomas-menant-95171937/)

[Alex Morel](https://twitter.com/alex_morel_)

[Bruce Perens](https://perens.com/)

[Benjamin Poussin](https://www.linkedin.com/in/benjamin-poussin-15b9a/)

[Pierre-Louis Rolle](https://www.linkedin.com/in/pierrelouisrolle/)

[Caroline Span](https://www.linkedin.com/in/caroline-span)

:::

:::

:::
:::

:::{.column}
::: {.text-nano-grey .text-center .text-compact}

**Informal discussions**

<br>

:::{.columns}

:::{.column width="50%"}

[Jaime Arredondo](https://www.linkedin.com/in/jaimearredondo/)

[Vincent Bergeot](https://www.linkedin.com/in/vincent-bergeot-aa95b82b/)

[Alexandre Bigot-Verdier](https://www.linkedin.com/in/alexandre-bigot-verdier-27348920/)

[Mathilde Bras](https://www.linkedin.com/in/mathildebras/)

[Dorie Bruyas](https://www.linkedin.com/in/dorie-bruyas/)

[Héloïse Calvier](https://www.linkedin.com/in/h%C3%A9lo%C3%AFse-calvier-040a85154/)

[Valentin Chaput](https://www.linkedin.com/in/valentinchaput/)

[Bertrand Denoncin](https://www.linkedin.com/in/bertranddenoncin/)

[Virgile Deville](https://www.linkedin.com/in/virgiledeville/)

[Angie Gaudion](https://www.linkedin.com/in/aggaudion/)

[Rémy Gerbet](https://www.linkedin.com/in/r%C3%A9my-gerbet-511249a1/)

[Maxime Guedj](https://www.linkedin.com/in/maximeguedj/)

:::

:::{.column width="50%"}
[Jan Krewer](https://www.linkedin.com/in/jan-krewer/)

[Benjamin Jean](https://www.linkedin.com/in/benjaminjean/)

[Tobie Langel](https://www.linkedin.com/in/tobielangel/)

[Claire-Marie Meriaux](https://www.linkedin.com/in/clairemarie/)

[Aude Nanquette](https://www.linkedin.com/in/aude-nanquette-53218947/)

[Samuel Paccoud](https://www.linkedin.com/in/spaccoud/)

[Gabriel Plassat](https://www.linkedin.com/in/plassat/)

[Benoît Ribon](https://www.linkedin.com/in/benoit-ribon/)

[Johan Richer](https://www.linkedin.com/in/johanricher/)

[Simon Sarazin](https://www.linkedin.com/in/simonsarazin/)

[Sébastien Shulz](https://www.linkedin.com/in/sebastien-shulz/)

[Xavier von Aarburg](https://www.linkedin.com/in/xaviervonaarburg/)

:::

:::

:::

:::

::::::

## A last word...

<br>

:::{.columns}

:::{.column width="30%" .img-shadow}
![&nbsp;](images/an-01.jpg)
:::

:::{.column width="70%" .text-center}

:::{.text-large}
<br>
**Utopia reduces when cooking**<br>
**which is why**<br>
**you need a lot from the start**
:::

<br>

:::{.text-micro}
Quote from Gébé, ["L'an 01"](https://www.lassociation.fr/catalogue/l-an-01-livre-et-dvd/)
:::
:::

:::

# Versions du document

:::{.text-nano-grey}

- **Version 1.x** - février 2024 / décembre 2024 :
  - Version 1.7 :
    - Ajout de la liste "Organisations funding the commons" dans la page [Ressources](##autres-ressources-en-ligne-remarquables)
    - Ajout de la slide [Présentations liées](#presentations-liées)
  - Version 1.6 :
    - Ajouts dans la page [Revue de presse](#revue-de-presse)
    - Ajout du lien vers le nouvel [espace Matrix](https://matrix.to/#/#the-librery:multi.coop) dédié
    - Ajout de [Open Source Pledge](https://osspledge.com/) dans la page [Inspirations](#inspirations) et ajouts dans la page [Remerciements](#remerciements)
    - Ajout du diagramme de Sankey sur la [répartition des fonds](#répartition-des-fonds-collectés)
    - Ajout de la slide [Revue de presse](#revue-de-presse)
    - Ajout de la slide sur les risques d'[épuisement de l'écosystème](le-risque-dépuisement-de-lécosystème) et de [sécurité](#le-risque-de-faille-de-sécurité)
    - Ajouts dans la page [Remerciements](#remerciements)
    - Ajout de la verticale de slides sur les[scénarios](#scenarios)
    - Ajout de la verticale de slides sur l'[internationalisation](#internationalisation)
  - Version 1.5 :
    - Updates dans la page [Remerciements](#remerciements) - ajout d'[Olivier Charbonneau](https://www.linkedin.com/in/olivier-charbonneau-b210491/)
    - Ajout du lien vers [Open Patent](https://www.boldandopen.com/open-patent) dans la page [Inspirations](#inspirations)
    - Ajout de la slide focus sur la licence "[Post-Open](#focus-sur-la-licence-post-open)"
    - Updates dans la page [Remerciements](#remerciements) + ajouts de liens + ajout de [Bruce Perens](https://perens.com/)
    - Ajout du lien vers des informations sur les [Fiducies d'intérêt social]() dans la page [Inspirations](#inspirations)
    - Ajout de la [version en anglais de la présentation](https://julien_p.gitlab.io/the-librery-slides/presentation-en.html)
    - Ajouts dans la page [Remerciements](#remerciements)
  - Version 1.4 :
    - Ajout du lien vers la publication du think tank [Open Future](https://openfuture.eu/publication/european-public-digital-infrastructure-fund/)
    - Ajout du lien vers l'article sur [Bruce Perens](https://programmation.developpez.com/actu/352421/Qu-est-ce-qui-vient-apres-l-open-source-Un-pionnier-du-mouvement-de-l-open-source-affirme-qu-il-faut-changer-de-paradigme-et-trouver-un-moyen-equitable-de-remunerer-les-developpeurs/), de [KDE e.V.](https://ev.kde.org/), et de [Open Source Initiative](https://opensource.org/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
:::

## Archives (v.1.0 - v.1.3)

:::{.text-nano-grey}

- **Version 1.x** - novembre 2023 / février 2024 :
  - Version 1.3 :
    - Ajouts dans la page [Remerciements](#remerciements)
    - Ajout des liens vers [Open Infra](https://openinfra.dev/), [Open North](https://opennorth.ca/), et [Green Software Foundation](https://greensoftware.foundation/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Ajout du lien vers [Funding the commons](https://fundingthecommons.io/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Ajout du lien vers le [Digital Transparency Lab](https://www.transparencylab.ca/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Ajout de la revue [Imaginaire Communs](https://anis-catalyst.org/communs/imaginaire-communs/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Chapitre à part pour la partie [Inspirations](#inspirations)
    - Version parallèle simplifiée, réduite uniquement sur Librery + [url dédiée](https://julien_p.gitlab.io/the-librery-slides/librery.html)
    - Ventilation des chapitres dans le répertoire `/texts` ([voir sur le repo](https://gitlab.com/Julien_P/the-librery-slides/-/tree/main/texts))
  - Version 1.2 : Mises à jour
    - Ajout du lien [Compte commun des communs](https://wiki.resilience-territoire.ademe.fr/wiki/LE_COMPTE_COMMUN_DES_COMMUNS) dans la page [Inspirations](#inspirations)
    - Ajouts dans la page [Ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Ajouts dans la page [Remerciements](#remerciements)
    - Ajout de la page [Focus sur Copie Publique](#focus-sur-linitiative-copie-publique)
    - Ajout de la _Software Freedom Conservancy_ dans la page [Inspirations](#inspirations)
  - Version 1.1 : Ajout de la slide [Place de l'Etat](#la-place-de-letat) et modifications mineures
  - Version 1.0 : Première version du document
  - Version 1.0 : Présentation lors de l'évènement NEC local à Lille "[Les communs pour un numérique au service de tous](https://openagenda.com/assembleurs/events/les-communs-pour-un-numerique-au-service-de-tous)"
:::

#

::: {.text-center}
**Thanks for your attention !**

[https://julien_p.gitlab.io/the-librery-slides/presentation-en.html](https://julien_p.gitlab.io/the-librery-slides/presentation-en.html)

:::

::: {.img-medium .img-no-margin .img-no-caption}
![&nbsp;](images/XKCD.png)
:::
::: {.text-nano .text-center style="margin: -10px 0 40px 0"}
[XKCD n°2347](https://xkcd.com/2347/)
:::

::: {.text-center}
<i class="ri-mail-line"></i>&nbsp; julien.paris<i class="ri-at-line"></i>multi.coop
<br><i class="ri-message-2-line"></i> [channel Matrix for chatting](https://matrix.to/#/#the-librery:multi.coop)

:::
