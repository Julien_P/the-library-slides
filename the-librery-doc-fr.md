---
title: Financer <br>les communs
subtitle: |
  <strong>Par et pour les commoners</strong>
  <br>
  <img src="images/qr-code-slides-librery.png" alt="QR code" height="150px" style="margin: 5% 3%"/>
author: '
  <code>
    Version 1.7
  </code><br><br>
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _développeur FLOSS fullstack_
  </span><br>
  <span class="text-micro text-author-details">
    _co-fondateur de la [coopérative multi](https://multi.coop)_
  </span>
'
date: '
  Illustration : [XKCD n°2347](https://xkcd.com/2347/)
  <br>
  <br><b>Navigation avec les flèches</b> <i class="ri-drag-move-2-fill"></i> / Plan en pressant <span class="text-nano">`echap`</span>
  <br>
  <br>
  <span style="font-size: .7rem;" xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/Julien_P/the-librery-slides">The Librery - slides</a> par <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/Julien_P">J. Paris</a> est sous licence <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:.7rem!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:.7rem!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:.7rem!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></span> 
'
title-slide-attributes:
  data-background-image: "images/Librery-logo-baudot-black.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 15%"
  data-background-position: "right 5% top 45%"
---

# Sommaire

::: {.text-micro}

- [Prologue](#prologue)
- [Problématique](#problématique)
- [Les communs, condition et moteur de l’innovation](#les-communs-condition-et-moteur-de-linnovation)
- [The Librery](#the-librery)
- [Collecte et redistribution](#collecte-et-redistribution)
- [Réflexions sur la gouvernance](#réflexions-sur-la-gouvernance)
- [Internationalisation & décentralisation](#internationalisation-décentralisation)
- [Scénarios](#scénarios)
- [Inspirations](#inspirations)
- [Comment poursuivre cette réflexion ?](#comment-poursuivre-cette-réflexion)
- [Références et documentation](#références-et-documentation)
- [Remerciements](#remerciements)
- [Versions du document](#versions-du-document)
:::

# Problématique

::: {.img-medium}
![&nbsp;](images/hamlet-meme.jpg)
:::

:::fragment
::: {.text-center}
**Economiquement, industriellement et démocratiquement &nbsp; <br> &nbsp;une question de vie ou de mort...**
:::
:::

## Pas d'internet sans communs numériques

:::{.columns .align-center}

::: {.column width="65%"}
![&nbsp;](images/logos-open-source.png)
:::

::: {.column width="35%" .text-micro}
Les <b>services publics</b> pourraient-ils aujourd'hui se passer de tout ou partie des quelques outils libres montrés ici (liste absolument non exhaustive) ?

Ces mêmes outils sont par ailleurs [eux-mêmes basés sur des couches plus ou moins basses d'une multitude d'autres outils _open source_](https://medium.com/graph-commons/analyzing-the-npm-dependency-network-e2cf318c1d0d) (langages, librairies, bases de données, standards...).

Certaines entreprises dont les services reposent sur ces briques _open source_ ont pleinement conscience de cette <b>dépendance à un éco-système</b> extrêmement vaste et divers.

Parmi ces entreprises d'aucunes [contribuent et redistribuent la valeur](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/) qu'elles génèrent, d'autres [tentent à bas bruit de ré-internaliser ces briques et les personnes qui les maîtrisent le mieux](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/).
:::
:::

## Les enjeux principaux

:::incremental

- **Pérenniser** et **améliorer** des communs existants
- Aider à l'**émergence de nouveaux communs** numériques
- Développer la contribution aux communs comme **activité économique** à part entière
- Renforcer le **plaidoyer** pour une société des communs à l'heure où s'accentuent les risques d'épuisement des ressources
- Anticiper et prévenir les risques de perte de **souveraineté** numérique
- Préciser la **place des pouvoirs publics** dans le soutien aux communs

:::

## Les principaux constats

:::incremental

- La pérennité des communs numériques pose des **enjeux infrastructurels et stratégiques, voire démocratiques**
- De nombreux communs numériques sont **invisibles** car dans des couches plus ou moins basses des infrastructures numériques
- Les modèles économiques des communs numériques supposent - souvent - une logique de **valorisation de services autour des communs**, donc supposant d'être développés _ex-post_ après des investissements importants en R&D, en temps...
- L'équilibre économique des communs peut reposer sur des **sources de revenus très diversifiées** : dons, subventions, bénévolat, mécénat, ...
- Le **soutien direct des acteurs publics** peut aujourd'hui se décliner sous différentes formes : appels à projet, subventions, commande publique...
- L'économie générale des communs numériques et des _commoners_ est **singulière dans le paysage économique** et est de surcroît complexe à évaluer

:::

<!-- ::::::::: {data-visibility="hidden"} -->
## En quelques questions

:::incremental

- Comment développer l'**économie des communs numériques** comme une **filière industrielle essentielle** voire **désirable** ?
- Et si on considérait les “commoners” (contributeurs à des communs numériques) comme des **auteurs**, au sens du droit d’auteur ?
- Quelles sont les **exemples** qui dans l’histoire font écho à ces problématiques ?
- **Quelle structure** inventer pour répondre à ces enjeux ?
- Si les entreprises du numérique ont compris l'intérêt stratégique et économique des communs sur lesquels elles se sont développées, quel est le **niveau de prise de conscience des pouvoirs publics** ?

:::

<!-- ::::::::: -->

## Points d'attention

:::::: {.columns}

::: {.column}
:::fragment
### Se méfier du _business as usual..._
![&nbsp;](images/opensource-money-meme.jpg)

::: {.text-center}
**Consolider l'économie des communs numériques &nbsp; <br> &nbsp;en rémunérant directement le travail de leurs auteurs**
:::
:::
:::

::: {.column}
:::fragment
### _Or just shot yourself in the foot_
![&nbsp;](images/opensource-remove-meme.jpg)

::: {.text-center}
**Sans communs numériques &nbsp; <br> &nbsp;nos infrastructures numériques s'effondrent**
:::
:::
:::

::::::

# Les communs, condition et moteur de l'innovation

:::{.r-stack .no-margin}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-00.png)
:::
:::{.fragment .fade-in data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-00-bis.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-01.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-02.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-03.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-04.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05-a.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05-b.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-06.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-06-bis.png)
:::
:::

## Le risque d'épuisement de l'écosystème

:::{.img-shadow .opaque .img-no-caption .img-large}
![&nbsp;](images/maintenance-paradox.png)
:::

<br>

:::{.text-micro .text-center}
Voir la présentation de [Tobie Langel](https://speaking.unlockopen.com/nBXJS5/1-billion-dollars-for-open-source-maintainers) donné lors de [State Of Open Con](https://stateofopencon.com/) 2024.
:::

## Le risque de faille de sécurité

:::{.columns}

:::{.column width="50%"}
:::{.text-micro}
Le risque de sécurité évité de justesse en 2024 sur la libraire XZ Utils ([source](https://www.itpro.com/software/open-source/we-got-lucky-what-the-xz-utils-backdoor-says-about-the-strength-and-insecurities-of-open-source))
:::
:::{.img-shadow .opaque}
![&nbsp;](images/xz-fail.png)
:::
:::

:::{.column width="50%"}
:::{.text-micro}
"Un milliard pour l'open source". Voir la présentation de [Tobie Langel](https://speaking.unlockopen.com/nBXJS5/1-billion-dollars-for-open-source-maintainers) donné lors de [State Of Open Con](https://stateofopencon.com/) 2024.
:::
:::{.img-shadow .opaque}
![&nbsp;](images/one-billion-maintain.png)
:::
:::

:::

:::{.text-micro .text-center}
Les risques de sécurité (voir le [rapport FOSSEPS](https://joinup.ec.europa.eu/collection/fosseps/news/fosseps-critical-open-source-software-study-report) de la Commission Européenne en 2022) de nos infrastructures numériques posent de nouveau la question du <b>manque de moyens alloués à la maintenance</b> de projets open source.
:::

## Le risque systémique

:::{.columns}
:::{.column width="50%"}
:::{.fragment .img-shadow .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/curl.png)
:::
:::
:::{.column width="50%"}
:::{.fragment .img-shadow .fade-in data-fragment-index="1"}
![&nbsp;](images/open-ssl.png)
:::
:::
:::

:::{.text-micro .text-center}
:::fragment
Exemples trouvés sur la page d'accueil de [Github sponsors](https://github.com/sponsors)
:::
:::

## L'économie des services publics <br> et des communs numériques

:::{.fragment .text-center .semi-fade-out .opaque data-fragment-index="1"}
::: {.img-small}
![&nbsp;](images/2-percents.png)
:::
:::

:::{.fragment .text-center data-fragment-index="1"}
<br>
<b>Un moteur qui tourne sur la réserve ?</b>
:::

# <span style="display:none">The Librery</span>

:::{.img-librery .img-no-margin .img-no-caption}
![&nbsp;](images/Librery-logo-baudot-black.png)
:::

:::{.text-center}
<b>Penser un mécanisme de financement pérenne 
  <br> et un organisme de gestion collective (OGC)
  <br> par et pour les auteur.e.s de communs
</b>
:::

::::::{.columns .text-center .text-micro}
:::{.column .card .card-secondary width="25%"}
<i class="ri-download-2-fill"></i><br>
La **collecte,**<br>**la mutualisation,**<br>et la **redistribution** <br>des fonds
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-scales-3-fill"></i><br>
Une exigence forte de **transparence, de non-lucrativité et de parité**
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-share-fill"></i><br>
Des formes diversifiées de **soutien aux communs et aux _commoners_**
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-shield-check-line"></i><br>
La possibilité <br>d'un **label**
:::
::::::

## Economie générale

:::{.r-stack}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-03-bis.png)
:::
:::{.fragment .fade-in data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-07.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-08.png)
:::
:::

## Missions de l'OGC

- **collecter** les contributions financières des diffuseurs
- **recenser** des communs et leurs auteurs
- **calculer** ce qui doit être réparti (clés de répartition)
- **redistribuer** en droits / déclarer à l'URSSAF (pour simplifier la vie des auteurs)
- **défendre** la cause et les droits des auteurs de communs numériques
- **promouvoir** le statut d'auteur de commun numériques
- **faire entrer dans la loi** des clauses équivalentes à celles relatives au CNC / SACD / etc... (régime de contribution obligatoire ?)

## Contributions financières <br>en soutien à l'économie des communs

Dans un souci de mutualisation l'organisme de gestion collective doit être en capacité de pouvoir <b>recevoir différents types de flux financiers</b> potentiellement "entrants", argent public ou argent privé :

::: {.text-micro}
:::{.columns}
:::{.column width="50%"}

**Flux financiers potentiels d'origine publique**

- Plaidoyer pour le principe de <b>2% communs dans la commande publique</b> sur des chantiers numériques (inspiré du "[2% pour 2 degrés](https://institut-rousseau.fr/2-pour-2c-resume-executif/)", institut Rousseau ; ou du "[1% artistique](https://www.culture.gouv.fr/Thematiques/Arts-plastiques/Commande-artistique/Le-1-artistique)")
- Contributions financières de type <b>subvention</b> de certaines institutions publiques (IGN, ADEME, DINUM...)
- Intervention <b>au niveau européen</b> de soutien aux communs numériques, à l'image de [NGI](https://www.ngi.eu/), de [NLnet](https://nlnet.nl/), ou du projet [EDIC](https://digital-strategy.ec.europa.eu/en/policies/edic)
:::

:::{.column width="50%"}
**Flux financiers potentiels d'origine privée**

- Contributions financières (type don) de certaines <b>entreprises privées</b> bénéficiant/diffusant des communs numériques
- <b>Prestations de services</b> type audit de code, _sourcing_...
- Plaidoyer pour le principe d'une obligation de contribution minimale obligatoire - ou <b>redevance</b> - de certaines industries dont l'activité repose sur l'exploitation extensive de communs numériques stratégiques
<!-- - Possibilité de proposer à des acteurs privés la **garantie de bon fonctionnement de certaines librairies libres critiques**, en échange d'apports financiers redistribués directement vers les mainteneur.e.s de ces librairies. -->
:::
:::
:::

## Conditions de gouvernance

- Les communs relevant tout à la fois de la sphère publique que de la sphère privée il est souhaitable que **toutes les parties puissent être représentées** au sein de l'OGC, toutefois en assurant une bonne réprésentation des auteurs/individus, étant donné que la mission de l'OGC est bien de les défendre

- En tant que structure défendant une filière (les communs) l'OGC a davantage de **légitimité** en tant que structure de droit privé, sous réserve que les différentes parties prenantes puissent être justement représentées

- Intégrer à la gouvernance autant les **auteurs** que les **éditeurs** ou les **financeurs** et **diffuseurs** de communs numériques

## Conditions structurelles

Sur la base des critiques faites aux OGC culturelles (cf. Benhamou) on peut lister quelques <b>conditions nécessaires (mais pas suffisantes) pour éviter certains travers</b> des OGC historiques identifiés par les observateurs et le champ académique :

- **Frais de gestion** ne dépassant pas 5% à 10%
- **Suivi et automatisation de l'enregistrement** des auteur.e.s et de leur rétribution
- **Plafonnement** des salaires des dirigeant.e.s de l'OGC
- **Représentation** de tous les auteur.e.s (1 personne == 1 voix)
- **Transparence** totale de la gouvernance et du fonctionnement, notamment concernant le choix et la mise en oeuvre des clés de répartitions

## Equilibre de fonctionnement<br>(hypothèses)

::::::{.columns}
:::{.column width="50%"}
**Coûts de fonctionnement**

:::{.text-micro}
En comptant une équipe au complet (5 personnes à temps plein, voir plus loin dans la section "Réflexions sur la gouvernance") avec des salaires de 4&nbsp;000 € nets/mois, des bureaux, et quelques services complémentaires (comptable, abonnements, services en ligne...) :

en première approximation le <b>coût total de fonctionnement et de gestion tournerait autour de 550 k€ / an.</b>
:::
:::

:::{.column width="50%"}
**Collecte**

:::{.text-micro}
Si on maintient des coûts de fonctionnement et de gestion autour de 10%, cela signifie que l'<b>OGC doit être en mesure de collecter 5,5 millions € / an.</b>

En partant d'une base de "2% communs" sur les chantiers numériques, ce montant de collecte correspond à un ensemble de <b>chantiers numériques dont les budgets cumulés représenteraient 275 millions € / an</b> avant contribution aux communs.

:::
:::
::::::

**Redistribution**

:::{.text-micro}
En résumé, en appliquant le principe de "2% communs" sur un ensemble de chantiers numériques à hauteur de 275 M€ / an,
l'OGC serait en capacité de redistribuer : 5,5 M€ (collectés) - 0,55 M€ (frais de gestion)

soit <b>une redistribution de 4,95 M€ / an à des communs numériques.</b>
:::

## Véhicules juridiques possibles

Différents types de structures juridiques peuvent être imaginées pour remplir les missions de The Librery, avec pour point commun d'être à <b>but non lucratif</b> ou du moins à lucrativité limitée.

:::{.text-micro}
- L'**OGP** (Organisme de Gestion Collective)
- La **SGCDA** (Société de Gestion Collective des Droits d'Auteur )
- La **Fondation**
- La **Fiducie** (ou _trust_)
- La **SCIC** (Société Coopérative d'Intérêt Collectif)
- Le **GIP** (Groupement d'Intérêt Public)
- La **Mutuelle**
- Le **Fonds d'investissement**
- Le **Fonds de dotation**
- L'**Association loi de 1901**
:::

## Le label comme incitation

- L'existence d'un organisme légitime auprès de toutes les parties prenantes des communs numériques permettrait d'imaginer la création d'un "**label communs**", que l'organisme pourrait délivrer aux projets comme aux structures.

- La notion de "label communs" peut être un axe intéressant à développer pour **inciter les administrations et les entreprises à contribuer** à hauter de 2% de leurs chantiers numériques.

- La notion de label pose les questions des **critères objectifs** permettant de qualifier un commun numérique comme tel.

- L'autre pendant de la notion de label serait d'arriver à **identifier et suivre des indices de valorisation économique** (financiers, temps alloué, économies...) permettant de mieux situer les apports des communs numériques au sein de la filière industrielle.

## Idées de logo

<br>

:::{.columns .text-micro}
:::{.column width="50%"}
### Inspiré du [XKCD n°2347](https://xkcd.com/2347/)

![&nbsp;](images/Librery-logo-long-white.png)

![&nbsp;](images/Librery-logo-long-dots-01.png)
:::
:::{.column width="50%"}
### Inspiré du [code Baudot](https://fr.wikipedia.org/wiki/Code_Baudot)

![&nbsp;](images/code-baudot.png)

![&nbsp;](images/Librery-logo-baudot-black.png)
:::
:::

# Collecte et redistribution

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
Distinguer<br>**oeuvre,** **auteur,**<br>**diffuseur,** et **éditeur**
:::
:::{.column .card width="33%"}
Une approche de type <br>"**marché biface**"
:::
:::{.column .card width="33%"}
La notion de **contrats de rétribution** pour intégrer les particularités des parties prenantes
:::
::::::

## Notions principales

Les parallèles avec les autres industries culturelles sont les suivants :

- **Oeuvre** (individuelle ou de collaboration) : 1 logiciel libre (=== 1 chanson)
- **Auteur** : 1 développeur / designer (=== 1 artiste)
- **Diffuseur** : 1 entité/administration déployant un logiciel libre( === 1 radio)
- **Editeur** : 1 entreprise privée développant un produit libre (=== 1 maison de production)

## Mécanisme général

- **Auteurs** : il faut que les auteurs s'inscrivent, et qu'il déclarent à quelles oeuvres libres ils contribuent
- **Oeuvres** : il faut un catalogue d'oeuvres (lociels libres, BDD libres) sur lesquelles on peut avoir un suivi des contributions
- **Diffuseurs** : il faut qu'ils déclarent quelles oeuvres ils diffusent, et qu'ils contribuent financièrement à une caisse commune (via des conventions). Cette contribution peut être imaginée de différentes manières, cumulativement :
  - cotisations
  - et/ou contribution forfaitaire
  - et/ou contribution forfaitaire par commun
  - et/ou indexation sur un budget alloué à du développement de communs

## Répartition des fonds collectés

:::{.opaque .img-no-caption}
![&nbsp;](images/librery-fund-sankey-fr.png)
:::

## A/ Processus côté diffuseurs de communs

:::::::::::::: {.columns}
::: {.column width="50%"}
![&nbsp;](images/diffusors.png)
:::
::: {.column width="50%"}
::: {.text-micro}

- Est considéré comme **diffuseur** une entité morale ou physique prête à verser volontairement une somme afin de rémunerer les auteurs et co-auteurs d'un ou plusieurs communs

- Un diffuseur signe un **contrat de rétribution aux communs** avec l'OGC, l'OGC s'engage à redistribuer les montants volontairement versés par le diffuseur.

- Le contrat porte sur un **montant** de contribution volontaire sur un ou plusieurs communs, voire sans commun particulier, le tout sur une période donnée

- Ce contrat peut porter des clauses et **règles de redistribution**

- Le contrat est alors ventilé par **montants alloués par année et par commun**

- Tous les contrats et leurs montants associés sont **agregés par commun**, cela constitue un fonds par commun (et par année)
:::
:::
::::::::::::::

## A/ La notion de contrats de rétribution

En amont - c'est-à-dire vis-à-vis des contributeurs financiers (processus A/) - il serait possible d'associer à chaque contribution financière des **contrats de rétribution** précisant les différents <b>emplois des sommes</b>, les <b>conditions et règles</b> de redistribution, ainsi que les <b>parts allouées</b> à des communs spécifiques ou au contraire sans fléchage précis.

:::{.text-micro}
On pourrait imaginer qu'à **chaque contribution financière** puisse être associé un contrat précisant :

- **une ou plusieurs parts fléchées** sur un/des communs choisis par le contributeur / diffuseur, en raison de son intérêt direct à en déléguer la maintenance pour le bon fonctionnement de ses propres services.
- **une part plancher sans fléchage**, en vue d'une redistribution choisie par l'OGC sur d'autres communs "_low level_" ou à faible visibilité (part qui pourrait par exemple être reversée à des entités telles que Thanks.dev ou équivalent).
:::

## B/ Processus côté auteurs de communs

:::::::::::::: {.columns}
::: {.column width="50%"}
![&nbsp;](images/authors.png)
:::
::: {.column width="50%"}
::: {.text-micro}
- **Un.e auteur.e s'enregistre** à l'OGC en précisant à quelles oeuvres/communs il contribue

- Un **contrat de cession/gestion** de ses droits d'auteur est etabli entre l'OGC et l'auteur

- Ses **contributions** à des communs sont calculées et enregistrées au cours de l'année, commun par commun

- En fin d'année ses contributions permettent de **calculer le montant de la rétribution** qui lui est allouée, commun par commun, en fonction du fonds associé et des règles de redistribution

- L'OGC établit sa **déclaration de droits d'auteurs** auprès de l'URSSAF et transmet à l'auteur la preuve de déclaration

- _Nota_ : le processus côté auteur.e.s est celui qui demande à être le plus **rapide et automatisé** possible afin d'éviter toute lourdeur administrative et coûts exponentiels de gestion.
:::
:::
::::::::::::::

## C/ Cas particulier <br>des éditeurs de communs numériques

::: {.text-micro}

- Un éditeur de communs emploie **un.e ou des salarié.es, considéré.es comme les auteurs initiaux** des communs édités

- L'**éditeur est considéré comme titulaire des droits** d'auteurs de ses employé.e.s

- L'éditeur permet la **gestion des droits en son nom par l'OGC** auprès de l'URSSAF

- L'éditeur **enregistre ses employé.e.s** comme auteurs sur l'OGC

- L'OGC calcule et déclare les rétributions aux auteurs comme en B/, avec la particularité d'**enregistrer ces sommes à l'URSSAF au nom de l'éditeur** aussi mais envoie les documents à l'éditeur

:::

## Modèle de données complet simplifié

Voir le [document](https://gitlab.com/Julien_P/the-librery-slides/-/blob/main/Model.md) au format _markdown_ sur le _repo_.

:::::::::::::: {.columns}

::: {.column}
::: {.text-micro}
Partie 1/2
:::
::: {.img-no-margin}
![&nbsp;](images/model-simplified-01.png)
:::
:::

::: {.column}
::: {.text-micro}
Partie 2/2
:::
::: {.img-no-margin}
![&nbsp;](images/model-simplified-02.png)
:::
:::

::::::::::::::

# Réflexions sur la gouvernance

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
Un représentation de **toutes les parties prenantes** des communs
:::
:::{.column .card width="25%"}
Une majorité structurelle pour les **auteur.e.s**
:::
:::{.column .card width="25%"}
Des **ressources humaines et techniques** pour la mise en oeuvre des missions
:::
:::{.column .card width="25%"}
Des exigences de **transparence** et de **non-lucrativité** fortes
:::
::::::

## Financer les communs numériques comme un commun

::: {.opaque .img-xl .img-no-caption .img-no-margin}
![&nbsp;](images/diagrams/Handler.ashx_.jpeg)
:::

::: {.text-micro .text-center}
<i class="ri-book-2-line"></i>
Voir le livre <b>"Governing the commons"</b> par [Elinor Orstom](https://en.wikipedia.org/wiki/Elinor_Ostrom)
:::

## Diagramme général

:::::: {.columns}

::: {.column width="60%"}
![&nbsp;](images/global.png)
:::

::: {.column width="40%"}
::: {.text-micro}
Diagramme général de principe de The Librery (réalisé avec [Whimsical](https://whimsical.com/the-librery-organigramme-RimouvNB2jVq7pcu59zP4H))

De haut en bas :

- **Les parties prenantes** :
  - Auteurs
  - Oeuvres (communs)
  - Diffuseurs
  - Editeurs
  - Sympathisants
- **L'équipe de l'OGC**
- **Le fonds mutualisé** et ses différents usages
  - Gestion des droits
  - Aides ciblées
  - Programmes / actions
  - Promotion des communs
- **Les outils** mis à disposition
  - Portail web (+ backoffice)
  - Services tiers

:::
:::

::::::

## Les parties prenantes

![&nbsp;](images/stakeholders.png)

## Les moyens financiers : le fonds

![&nbsp;](images/fund.png)

## Les moyens humains : l'équipe

![&nbsp;](images/team.png)

## Les moyens techniques : les outils

![&nbsp;](images/tools.png)

# Internationalisation & décentralisation

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
Pouvoir s'adapter aux **spécificités juridiques locales**
:::
:::{.column .card width="33%"}
Pouvoir **mutualiser des jeux de données** (registres des auteurs ou des communs)
:::
:::{.column .card width="33%"}
Avoir une **architecture d'information décentralisée**
:::
::::::

## Pourquoi anticiper une approche internationale ?

#### Les contraintes de la production de communs numériques

::: {.text-micro}
- Les communs numériques peuvent être développés <b>partout dans le monde</b>.
- Les <b>commoners</b> contribuant à un même commun peuvent eux-mêmes être <b>basés dans des pays différents</b>.
- Les <b>contributeurs financiers aux communs</b> peuvent également être basés dans des pays différents.
- Un OGC est nécessairement basé juridiquement dans un pays, <b>le statut juridique d'un OGC peut donc être différent d'un pays à l'autre</b> (fondation, trust, association, fiducie...).
- Un OGC n'a pas forcément vocation - ou la capacité - à traiter l'ensemble des communs au niveau mondial, <b>il est plus réaliste d'envisager qu'un OGC ait un ancrage territorial et/ou sectoriel précis</b>.

Il est donc nécessaire que tous les OGC partout dans le monde puissent **partager des registres communs pour éviter les doublons et les incohérences juridiques** : registre des communs, registre des commoners, montants des contributions financières, allocations des sommes vers les communs et les commoners...
:::

## Architecture décentralisée

::: {.text-micro}
- Il serait envisageable - voire plus réaliste - qu'il existe **plusieurs OGC répartis partout dans le monde**, et/ou différents OGC "spécialisés" sur certaines verticales (secteur industriel, language de programmation, ...)

- Pour qu'un tel système fonctionne à l'échelle internationale l'**infrastructure d'information doit être partagée**, tout en permettant à chaque OGC de gérer son propre corpus de données.

- Techniquement il serait possible de proposer à tous les OGC un outil numérique libre de gestion basé sur [ActivityPods](https://activitypods.org) afin de pouvoir à la fois **décentraliser le stockage des données**, tout en permettant à **chaque instance de gérer ses propres jeux de données**.

:::

## Diagramme de principe

:::{.r-stack .no-margin}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/decentralized/diagram-01.png)
:::
:::fragment
![&nbsp;](images/diagrams/decentralized/diagram-02.png)
:::
:::fragment
![&nbsp;](images/diagrams/decentralized/diagram-03.png)
:::
:::fragment
![&nbsp;](images/diagrams/decentralized/diagram-99.png)
:::
:::

# Scénarios

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
Un **mécanisme simple**
:::
:::{.column .card width="33%"}
Approche **contractuelle**
:::
:::{.column .card width="33%"}
Différentes approches selon des **acteurs publics ou privés**
:::
::::::

# Inspirations

:::{.text-micro}
- Le mécanisme existant du "**1% artistique**" dans les [constructions publiques](https://www.culture.gouv.fr/Thematiques/Arts-plastiques/Commande-artistique/Le-1-artistique)

- La proposition de "**2% pour 2 degrés**" de l'[Institut Rousseau](https://institut-rousseau.fr/2-pour-2c-resume-executif/)

- Différentes initiatives venant du monde du logiciel libre ou d'institutions :
:::

:::{.columns .text-nano}
:::{.column width="60%"}

- [AFNIC](https://www.afnic.fr/en/)
- [Compte commun des Communs](https://wiki.resilience-territoire.ademe.fr/wiki/LE_COMPTE_COMMUN_DES_COMMUNS)
- [Contributive.org](https://contributive.org/fr/)
- [Copie Publique](https://copiepublique.fr/) by Code Lutin
- [Drips](https://www.drips.network/)
- [Document Foundation](https://www.documentfoundation.org/) by and for Libre Office
- [Fiducies d'intérêt social](https://tiess.ca/nouveaux-outils-fus/) by [TIESS](https://tiess.ca/)
- [Fondation Blue Hat](https://bzg.fr/le-logiciel-libre-a-besoin-d-une-vraie-strategie-de-mutualisation-au-sein-de-letat/)
- [Fondation Wikimedia](https://wikimediafoundation.org/fr/) by and for Wikimedia
- [Fonds de Dotation du Libre](https://www.fdl-lef.org/)
- [FOSS funders](https://fossfunders.com/)
- [Fund for Defense of Net Neutrality](https://fdn2.org/en/presentation.html)
- [Github sponsors](https://github.com/sponsors)
- [KDE e.V](https://ev.kde.org/)
- [Libera Pay](https://liberapay.com/)
- [NLnet](https://nlnet.nl/) - [NGI e-Commons Fund](https://nlnet.nl/news/2023/20230719-eCommonsFund.html)
- [Oceco](https://movilab.org/wiki/Oceco) by Communecter
:::
:::{.column width="40%"}
- [Open Collective](https://opencollective.com/opensource)
- [Open Patent](https://www.boldandopen.com/open-patent)
- [Open Source Collective](https://oscollective.org/)
- [Open Source Initiative](https://members.opensource.org/donate/)
- [Open Source Pledge](https://osspledge.com/)
- [Open Source Security Foundation](https://openssf.org/)
- [Open Technology Fund](https://www.opentech.fund/)
- [OWASP Foundation](https://owasp.org/donate/)
- [Post-Open licence](https://perens.com/?s=post-open) by [Bruce Perens](https://perens.com)
- [Proton Lifetime Fundraiser](https://proton.me/blog/lifetime-fundraiser-survey-2023)
- [Prototype Fund](https://prototypefund.de/en/) (Allemagne)
- [SARD - Société de Répartition des Dons](https://framablog.org/2009/09/05/sard-hadopi/)
- [Software Freedom Conservancy](https://sfconservancy.org/)
- [Sovereign Tech Fund](https://sovereigntechfund.de/en/) (Allemagne)
- [Stakes.social](https://stakes.social/)
- [Thanks.dev](https://thanks.dev/home)
- [TOSIT - The Open Source I Trust](https://tosit.fr/)

:::
:::

## Focus sur l'initiative Copie Publique

:::{.columns}
:::{.column width="15%"}
![&nbsp;](images/copie-publique-logo.svg)
:::

:::{.column .text-micro .text-no-margin-top width="85%"}
L'initiative [Copie Publique](https://copiepublique.fr/) consiste en la description d'un **mécanisme d'allocation puis de redistribution** de fonds à des projets _open source_, qu'une entreprise peut librement choisir de mettre en oeuvre à condition d'en respecter la forme et l'esprit.

Le système proposé par **Copie Publique n'est pas une caisse partagée par plusieurs entreprise** : chaque entreprise  choisissant d'adopter le mécanisme Copie Publique abonde seule à son propre fonds, sur la base d'une assiette annuelle qu'elle choisit et rend publique. Une entreprise peut ainsi consacrer par exemple 1% de son CA, ou 3% de ses excédents nets de gestion, ou encore un montant fixe par an. La seule condition est que la règle soit claire, transparente, et permette d'abonder au fonds de façon récurrente.

Différents aspects du mécanisme Copie Publique sont intéressants à souligner, notamment ceux relatifs au **processus de sélection des projets soutenus** qu'a choisi l'entreprise Code Lutin et aux dynamiques que ce processus engendre :
:::
:::

:::{.text-nano-grey}
:::{.columns}
:::{.column width="50%"}
Le choix des projets soutenus se décompose en 2 phases en parallèle, partageant en deux enveloppes disctinctes la somme à redistribuer initialement :

- <b>une phase collective</b>, visant à identifier et choisir un nombre réduit de projets grâce à un "appel à candidatures" : les candidats envoient un descriptif du projet pour lequel ils demandent un soutien, et précisent également leurs besoins. Le collectif en choisit entre 1 et 3, suite à la mise en place d'un jury.
- <b>une phase plus individuelle</b>, où chaque salarié.e a la possibilité de redistribuer une enveloppe à un ou plusieurs projets de son choix (issus de l'appel à candidature ou non). Cette phase permet de soutenir davantage de projets de tailles plus modestes.
:::
:::{.column width="50%"}
<b>Tou.te.s les salarié.e.s de l'entreprise se mobilisent</b> durant le processus d'identification, d'évaluation, de sélection, et de rétribution des projets à soutenir. Chaque personne - indivuellement ou collectivement - peut ainsi contribuer activement au soutien de projets libres tiers, liberté qui n'existe pas dans n'importe quelle entreprise.

Suite à l'appel à candidature, un des moments décrit comme des plus enthousiasmants est celui de <b>la rencontre (présentielle ou en visio) avec les candidats</b>. Cette étape permet aux membres de l'entreprise de découvrir des individus et des technologies originales, de manière directe en conservant la dimension humaine et sociale des projets.
:::
:::
:::

:::{.text-nano-grey}
_Un chaleureux merci aux membres de [Code Lutin](https://www.codelutin.com/) avec qui l'auteur a pu échanger à Nantes, et qui ont pu décrire avec précision et passion leur intiative Copie Publique_
:::

## Focus sur la licence "post-open"

:::{.columns}
:::{.column width="40%"}

:::{.r-stack .no-margin}
:::{.fragment .semi-fade-out .img-no-caption .opaque data-fragment-index="1"}
![&nbsp;](images/post-open/postopen-01.png)
:::
:::{.fragment .img-no-caption .fade-in data-fragment-index="1"}
![&nbsp;](images/post-open/postopen-02.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-03.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-04.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-05.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-06.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-07.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-08.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-09.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-10.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-11.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-12.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-13.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-14.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-15.png)
:::

:::

:::

:::{.column .text-micro .text-no-margin-top width="60%"}
La [licence Post-Open](https://perens.com/2024/03/08/post-open-license-first-draft/) proposée par [Bruce Perens](https://wikipedia.org/wiki/Bruce_Perens) consiste à rendre contractuellement obligatoire pour les companies générant plus de 5 millions de USD$ par an utilisant et/ou modifiant le code source de : soit reverser le code sous la même licence ("POST-OPEN OPERATING AGREEMENT"), soit établir un contrat de rémunération pour des personnes ou entités chargées de maintenir ou améliorer le code source ("POST-OPEN PAID CONTRACT").

Les captures d'écran utilisées dans cette page sont extraites de cette [vidéo de Bruce Perens](https://www.youtube.com/watch?v=vTsc1m78BUk&list=PLfXKG51wk2sTBeAJtuL8wsUNvD-6lVDPM&index=3&t=1704s). Un autre [video de 2024](https://youtu.be/suUfS0-p5Yg?si=YRjwoLmnUXaToVqr) donne quelques informations supplémentaires.

_Un chaleureux merci à Bruce Perens avec qui l'auteur a pu échanger longuement sur ces sujets._

:::
:::

**Extrait de l'article 3.2 (PAID CONDITIONS) du _draft_ de la licence _post-open_**

:::{.text-nano-grey}
:::{.columns}
:::{.column width="50%"}
a)  The end-user revenue through all legal entities in which YOU have
    ownership exceeding 5% or equivalent control without ownership exceeds
    USD$5 Million anually. End-user revenue is all money or other value
    collected from customers, including the financial value equivalent of
    any non-monetary remuneration such as barter or the grant of rights or
    privileges.

b)  You provide, for remuneration, any work in the POST-OPEN COLLECTION to
    others, other than PERSONAL USE, or perform that provision at the order
    of another legal entity that receives remuneration for it. This includes
    (but is not limited to) provision of the work as a service; or inclusion
    of the work in a product that is sold, for example software that is sold
    or sale of a device containing the WORK.
:::

:::{.column width="50%"}
c)  You make MODIFICATIONS to any WORK in the POST-OPEN COLLECTION without
    performing one of these actions (you may perform both):

    I)  You enter into the POST-OPEN OPERATING AGREEMENT and make a PUBLIC
        RELEASE of the MODIFICATION.

    II) You enter into the POST-OPEN PAID CONTRACT.
:::
:::
:::

# Comment poursuivre <br>cette réflexion ?

:::{.text-micro}

<i class="ri-share-line mr-1" style="color: black;"></i>
**Partager et débattre** de ces idées auprès des acteurs des communs

<i class="ri-edit-2-line mr-1" style="color: black;"></i>
**Contribuer** sur le [pad de notes](https://hackmd.io/@Jpy/the-librery) ouvert en écriture et en commentaires

<i class="ri-list-unordered mr-1" style="color: black;"></i>
**Recenser** les expérimentations de mutualisation de moyens financiers en soutien aux communs

<i class="ri-map-pin-line mr-1" style="color: black;"></i>
**Cartographier** les acteurs / diffuseurs / producteurs / produits

<i class="ri-focus-3-line mr-1" style="color: black;"></i>
Identifier, classer et hiérarchiser une _shortlist_ de **communs “stratégiques”** pour l’Etat ou les collectivités

<i class="ri-message-2-line mr-1" style="color: black;"></i>
Recueillir **des témoignages et de nouvelles idées** auprès des producteurs de communs numériques

<i class="ri-pages-line mr-1" style="color: black;"></i>
Regrouper ces idées, la documentation, les témoignages sur un **site** en ligne

:::

# Références et documentation

Quelques éléments bibliographiques, classés par supports et grandes thématiques, en complément des notes générales relatives aux réflexions autour de [The Librery](https://hackmd.io/@Jpy/the-librery).

<i class="ri-message-2-line"></i> [Salon Matrix](https://matrix.to/#/#the-librery:multi.coop) pour continuer la discussion

<i class="ri-file-pdf-2-line"></i> [Articles / livres en pdf (nextcloud)](https://nuage.liiib.re/s/dx5eribMSRm6RTm) en accès libre

<i class="ri-book-2-line"></i> [Bibliographies (Zotero)](https://www.zotero.org/groups/5255274/librery/library)

<i class="ri-file-text-line"></i> [Pad de notes](https://hackmd.io/@Jpy/the-librery) ouvert en écriture et en commentaires

<i class="ri-movie-line"></i> [Vidéos / conférences (playlist Youtube)](https://www.youtube.com/playlist?list=PLfXKG51wk2sTBeAJtuL8wsUNvD-6lVDPM)

<i class="ri-gitlab-line"></i> [Fichiers sources](https://gitlab.com/Julien_P/the-librery-slides/-/tree/main/texts) et [ensemble de la présentation](https://gitlab.com/Julien_P/the-librery-slides) sur Gitlab

## Autres ressources en ligne remarquables

:::{.text-nano}
:::{.columns}

:::{.column width="50%"}

- ADEME (wiki) : [Résilience des territoires](https://wiki.resilience-territoire.ademe.fr/wiki/Accueil)
- ADEME (forum) : [Sobriété & Résilience des territoires](https://forum.resilience-territoire.ademe.fr/)
- ADULLACT (site) : [L'association](https://adullact.org/)
- AFULL - Association Francophone des Utilisateurs de Logiciels Libres (site) : [L'association](https://aful.org/)
- AIC - Accélérateur d'Inititaves Citoyennes : [Site internet](https://communs.beta.gouv.fr/)
- ANCT (site) : [Labo Société Numérique](https://labo.societenumerique.gouv.fr/fr/recherche?thematics=communs)
- Annuaire des Communs (site) : [Annuaire](https://annuaire.lescommuns.org/category/collectifs/)
- L'Assemblée des communs (site) : [Site internet](https://assemblee.lescommuns.org/)
- Chaire Socio Economie des Communs (univ. Lille) : [Laboratoire CLERSE](https://clerse.univ-lille.fr/)
- La Chambre des Communs (site) : [Site internet](https://chambre.lescommuns.org/gouvernance/)
- Les communs d'abord ! (média) : [Site internet](https://www.les-communs-dabord.org) / [Chat](https://chat.lescommuns.org/home)
- La Comptoir du Libre (site) : [Catalogue](https://comptoir-du-libre.org/fr/)
- Contributive Commons (licence) : [Portail](https://contributivecommons.org/)
- La Coop des Communs (site) : [L'association](https://coopdescommuns.org/fr/association/)
- Collectif de Recherche sur les Initiatives, Transformations et Institutions des Communs (site) : [Site internet](https://critic-communs.ca/)
- Digital Transparency Lab (non-profit organisation) : [Site internet](https://www.transparencylab.ca/)
- En communs (revue) : [Site internet](https://www.encommuns.net/)
- La Fabmob (pdf) : [Socle de compréhension des communs numériques de la mobilité](https://cloud.fabmob.io/s/L3gJsa3EnYQNSTk)
- La Fabrique à communs des Tiers-lieux (wiki) : [Movilab](https://movilab.org/wiki/Fabrique_%C3%A0_communs_des_tiers-lieux)
- Liraries.io (site) : [Site internet](https://libraries.io/)
- FOSS Sustainability : [Site internet](https://fosssustainability.com/)

:::

:::{.column width="50%"}

- Funding the commons (rencontres) : [Site internet](https://fundingthecommons.io/)
- Green Software Foundation : [Site internet](https://greensoftware.foundation/)
- IGN (wiki) : [Le Commun des communs](https://interlab.cc/wikign/?LeCommundescommuns)
- IGN (pdf) : [Guide des communs](https://www.ign.fr/files/default/2023-10/guide_communs_ouishare.pdf)
- Imaginaire Communs (revue) : [Site internet](https://anis-catalyst.org/communs/imaginaire-communs/) 
- Inno<sup>3</sup> (outil) : [Canevas pour la gouvernance de communs numériques](https://inno3.fr/realisation/canevas-pour-la-gouvernance-de-commun-numerique/)
- Matti Schneider (site) : [Construire des communs numériques](https://communs.mattischneider.fr/)
- NEC - Numérique en commun\[s\] (site) : [Ressources](https://numerique-en-communs.fr/les-ressources-nec/)
- Numérique d'Intérêt Général - Cadre de référence : [Site internet](https://www.numeriqueinteretgeneral.org/)
- Open Future : [Publication](https://openfuture.eu/publication/european-public-digital-infrastructure-fund/)
- Open Infra : [Site internet](https://openinfra.dev/)
- Open North : [Site internet](https://opennorth.ca/)
- Open Source Initiative : [Site internet](https://opensource.org/)
- Organisations funding the commons (curated list by Jaime Arredondo) : [Page Notion](https://boldandopen.notion.site/Organisations-Funding-the-Commons-Open-source-projects-93c482ba51d7498eac4ecc50275b9d7f)
- P2P Foundation (site) : [Site internet](https://p2pfoundation.net/)
- Le portail des communs (site) : [Portail](https://lescommuns.org/)
- "Qu'est-ce qui vient après l'open source ?" article de Mathis Lucas sur une présentation de [Bruce Perens](https://en.wikipedia.org/wiki/Bruce_Perens) : [article et vidéo](https://programmation.developpez.com/actu/352421/Qu-est-ce-qui-vient-apres-l-open-source-Un-pionnier-du-mouvement-de-l-open-source-affirme-qu-il-faut-changer-de-paradigme-et-trouver-un-moyen-equitable-de-remunerer-les-developpeurs/)
- S.I.Lex (blog) : [Site internet](https://scinfolex.com/)
- SILL - Socle Interministériel des Logiciels Libres (site) : [Catalogue](https://code.gouv.fr/sill/)
- Sustain OSS (podcast) : [Site internet](https://podcast.sustainoss.org/)
- Transcendance (article) : [Site internet](https://democratie-action.notion.site/Transcendance-outil-opensource-et-collaboratif-pour-l-mergence-de-mod-les-conomiques-d-int-r-t-g--0ce0f7039dc04661921c9416a35b9796)
- Le wiki des communs (wiki) : [lescommuns.org](https://wiki.lescommuns.org/)
- La 27<sup>ème</sup> Région (site) : [Enacting the commons](https://enactingthecommons.la27eregion.fr/)

:::

:::
:::

## Revue de presse

:::{.text-nano}
<i class="ri-message-2-line"></i> [Salon Matrix pour veille et revue de presse](https://matrix.to/#/%23librery-press:multi.coop)
:::

#### 2024

:::{.text-nano}

- 2024-11-19 / Github : ["Announcing GitHub Secure Open Source Fund: Help secure the open source ecosystem for everyone"](https://github.blog/news-insights/company-news/announcing-github-secure-open-source-fund/)
- 2024-11-14 / IEE Spectrum : ["Open-Source Software Is in Crisis"](https://spectrum.ieee.org/open-source-crisis)
- 2024-11-10 / TechCrunch: ["Open source projects draw equity-free funding from corporates, startups, and even VCs"](https://techcrunch.com/2024/11/10/open-source-projects-draw-equity-free-funding-from-corporates-startups-and-even-vcs/)
- 2024-10-15 / FLOSS fund: ["Announcing FLOSS Fund: $1M per year for free and open source projects"](https://floss.fund/blog/announcing-floss-fund/)
- 2024-09-30 / People Vs BigTech : ["Beyond Big Tech: A manifesto for a new digital economy"](https://peoplevsbig.tech/beyond-big-tech-a-manifesto-for-a-new-digital-economy/)
- 2024-09-19 / The Register : ["Kelsey Hightower: If governments rely on FOSS, they should fund it"](https://www.theregister.com/2024/09/19/kelsey_hightower_civo)
- 2024-05-24 / Public Interest Digital Innovation : ["Beyond big tech regulation : funding & scaling public good"](https://player.clevercast.com/?account_id=AkN3yR&item_id=2O3mE6)
- 2024-05-04 / Sequoia : ["New Fellowship: How Sequoia is Supporting Open Source"](https://www.sequoiacap.com/article/sequoia-open-source-fellowship/)
- 2024-04-17 / UK Day One : ["A UK Open-Source Fund to Support Software Innovation and Maintenance"](https://ukdayone.org/briefings/a-uk-open-source-fund)
- 2024-04-04 / Matrix.org : ["Open Source Infrastructure must be a publicly funded service."](https://matrix.org/blog/2024/04/open-source-publicly-funded-service/)
- 2024-04-03 / ITPro : [""We got lucky": What the XZ Utils backdoor says about the strength and insecurities of open source"](https://www.itpro.com/software/open-source/we-got-lucky-what-the-xz-utils-backdoor-says-about-the-strength-and-insecurities-of-open-source)
- 2024-02-16 / Jacobian.org : ["Paying people to work on open source is good actually"](https://jacobian.org/2024/feb/16/paying-maintainers-is-good/)
- 2024-01-19 / openpath.chadwhitacre.com : ["The Open Source Sustainability Crisis"](https://openpath.chadwhitacre.com/2024/the-open-source-sustainability-crisis/)

:::

---

#### 2023

:::{.text-nano}

- 2023-12-28 / Mathis Lucas : ["Qu'est-ce qui vient après l'open source ?"](https://programmation.developpez.com/actu/352421/Qu-est-ce-qui-vient-apres-l-open-source-Un-pionnier-du-mouvement-de-l-open-source-affirme-qu-il-faut-changer-de-paradigme-et-trouver-un-moyen-equitable-de-remunerer-les-developpeurs/)
- 2023-12-27 / linuxfr.org : ["La mission logiciels libres (DINUM) propose 4 prix BlueHats de 10000€ pour soutenir des mainteneurs"](https://linuxfr.org/news/la-mission-logiciels-libres-dinum-propose-4-prix-bluehats-de-10000-pour-soutenir-des-mainteneurs)
- 2023-11-09 / lemmy.ndlug.org : ["GNOME Recognized as Public Interest Infrastructure"](https://lemmy.ndlug.org/post/367541)
- 2023-10-24 / blog.sentry.io : ["We Just Gave $500,000 to Open Source Maintainers"](https://blog.sentry.io/we-just-gave-500-000-dollars-to-open-source-maintainers/)
- 2023-09-17 / nikclayton.writeas.com : ["Stepping back from the Tusky project"](https://nikclayton.writeas.com/update-5-on-stepping-back) serie
- 2023-07-27 / Serkan Holat : ["Open source public fund experiment - One and a half years update"](https://dev.to/coni2k/open-source-public-fund-experiment-one-and-a-half-years-update-367d)
- 2023-02-14 / Denis Pushkarev (core-js) : ["So, what's next ?"](https://github.com/zloirock/core-js/blob/master/docs/2023-02-14-so-whats-next.md)

:::

#### 2022

:::{.text-nano}

- 2022-12-14 / Startin' Blox : ["Co-funding in open source #1"](https://startinblox.com/en/2022/12/14/co-funding-in-open-source-1/)
- 2022-06-21 / Open Futuress : ["Digital commons are a pillar of european digital sovereignty"](https://openfuture.eu/blog/digital-commons-are-a-pillar-of-european-digital-sovereignty/)
- 2022-06 / diplomatie.gouv.fr (blog) : ["Les communs au service d’un modèle européen de souveraineté numérique non hégémonique"](https://www.diplomatie.gouv.fr/fr/politique-etrangere-de-la-france/diplomatie-numerique/blog-de-l-equipe/article/les-communs-au-service-d-un-modele-europeen-de-souverainete-numerique-non)
- 2022-05-16 / Tech Crunch : ["Tech giants pledge $30M to boost open source software security"](https://techcrunch.com/2022/05/16/white-house-open-source-security/)
- 2019-07-17 / unrealengine.com : ["Epic Games supports Blender with $1.2 million Epic MegaGrant"](https://www.unrealengine.com/en-US/blog/epic-games-supports-blender-with-1-2-million-epic-megagrant)
- 2018-06-24 / scinfolex.com : ["Les Communs numériques sont-il condamnés à devenir des « Communs du capital » ?"](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/)
- 2016-04-05 / Burak Arikan : ["Analyzing the NPM dependency network"](https://medium.com/graph-commons/analyzing-the-npm-dependency-network-e2cf318c1d0d)

:::

## Crédits

Présentation réalisée avec :

- [Pandoc](https://pandoc.org/)
- [Reveal.js](https://revealjs.com/)
- [Remix Icons](https://remixicon.com/)
- [Slider-template](https://gitlab.com/multi-coop/slider-template)

# Remerciements

**Les propos publiés dans cette présentation n'engagent que leur auteur.**

Toutefois l'auteur souhaite adresser <b>un grand merci</b> à toutes les personnes qui contribuent à enrichir cette réflexion et à <b>soutenir le mouvement pour une société des communs</b>.

::::::{.columns}

:::{.column}
::: {.text-nano-grey .text-center .text-compact}

**Relectures ou commentaires**

<br>

:::{.columns}

:::{.column width="50%"}

[Luis Roman Arciniega Gil](https://www.linkedin.com/in/luis-roman-arciniega-gil/)

[Vincent Bachelet](https://www.linkedin.com/in/vincent-bachelet-831663aa/)

[Alex Bourreau](https://www.linkedin.com/in/alexbourreau/)

[Jeanne Bretécher](https://www.linkedin.com/in/jeanne-bret%C3%A9cher-267b0110/)

[Brendan Brieg Le Ny](https://www.codelutin.com/lutins)

[Sébastien Broca](https://www.linkedin.com/in/sebastien-broca-96b39a264/)

[Olivier Charbonneau](https://www.linkedin.com/in/olivier-charbonneau-b210491/)

[Benoît De Haas](https://www.linkedin.com/in/beno%C3%AEt-de-haas-78141653/)

[Maïa Dereva](https://blog.p2pfoundation.net/author/maia-dereva)

[Ludovic Dubost](https://www.linkedin.com/in/ldubost)

[Margot Godefroi](https://www.linkedin.com/in/margot-godefroi-40689b154/)

[Marguerite Grandjean](https://www.linkedin.com/in/margueritegrandjean/)

[Bastien Guerry](https://bzg.fr/)
:::

:::{.column width="50%"}
[Serkan Holat](https://www.linkedin.com/in/serkanholat/)

[Alain Imbaud](https://www.linkedin.com/in/alain-imbaud-7632b22a/)

[Nicolas Jullien](https://www.linkedin.com/in/nicolasjullien/)

[Laure Kassem](https://www.linkedin.com/in/laure-kassem/)

[Arnaud Levy](https://www.linkedin.com/in/arnaudlevy/)

[Tibor Katelbach](https://www.linkedin.com/in/tiborkatelbach/)

[Nicolas Loubet](https://www.linkedin.com/in/nicolasloubet/)

[Thomas Menant](https://www.linkedin.com/in/thomas-menant-95171937/)

[Alex Morel](https://twitter.com/alex_morel_)

[Bruce Perens](https://perens.com/)

[Benjamin Poussin](https://www.linkedin.com/in/benjamin-poussin-15b9a/)

[Pierre-Louis Rolle](https://www.linkedin.com/in/pierrelouisrolle/)

[Caroline Span](https://www.linkedin.com/in/caroline-span)

:::

:::

:::
:::

:::{.column}
::: {.text-nano-grey .text-center .text-compact}

**Discussions informelles**

<br>

:::{.columns}

:::{.column width="50%"}

[Jaime Arredondo](https://www.linkedin.com/in/jaimearredondo/)

[Vincent Bergeot](https://www.linkedin.com/in/vincent-bergeot-aa95b82b/)

[Alexandre Bigot-Verdier](https://www.linkedin.com/in/alexandre-bigot-verdier-27348920/)

[Mathilde Bras](https://www.linkedin.com/in/mathildebras/)

[Dorie Bruyas](https://www.linkedin.com/in/dorie-bruyas/)

[Héloïse Calvier](https://www.linkedin.com/in/h%C3%A9lo%C3%AFse-calvier-040a85154/)

[Valentin Chaput](https://www.linkedin.com/in/valentinchaput/)

[Bertrand Denoncin](https://www.linkedin.com/in/bertranddenoncin/)

[Virgile Deville](https://www.linkedin.com/in/virgiledeville/)

[Angie Gaudion](https://www.linkedin.com/in/aggaudion/)

[Rémy Gerbet](https://www.linkedin.com/in/r%C3%A9my-gerbet-511249a1/)

[Maxime Guedj](https://www.linkedin.com/in/maximeguedj/)

:::

:::{.column width="50%"}
[Jan Krewer](https://www.linkedin.com/in/jan-krewer/)

[Benjamin Jean](https://www.linkedin.com/in/benjaminjean/)

[Tobie Langel](https://www.linkedin.com/in/tobielangel/)

[Claire-Marie Meriaux](https://www.linkedin.com/in/clairemarie/)

[Aude Nanquette](https://www.linkedin.com/in/aude-nanquette-53218947/)

[Samuel Paccoud](https://www.linkedin.com/in/spaccoud/)

[Gabriel Plassat](https://www.linkedin.com/in/plassat/)

[Benoît Ribon](https://www.linkedin.com/in/benoit-ribon/)

[Johan Richer](https://www.linkedin.com/in/johanricher/)

[Simon Sarazin](https://www.linkedin.com/in/simonsarazin/)

[Sébastien Shulz](https://www.linkedin.com/in/sebastien-shulz/)

[Xavier von Aarburg](https://www.linkedin.com/in/xaviervonaarburg/)

:::

:::

:::

:::

::::::

## Un dernier mot...

<br>

:::{.columns}

:::{.column width="30%" .img-shadow}
![&nbsp;](images/an-01.jpg)
:::

:::{.column width="70%" .text-center}

:::{.text-large}
<br>
**L’utopie, ça réduit à la cuisson,**<br>
**c’est pourquoi**<br>
**il en faut énormément au départ**
:::

<br>

:::{.text-micro}
Citation de Gébé, ["L'an 01"](https://www.lassociation.fr/catalogue/l-an-01-livre-et-dvd/)
:::
:::

:::

# Versions du document

:::{.text-nano-grey}

- **Version 1.x** - février 2024 / décembre 2024 :
  - Version 1.7 :
    - Ajout de la liste "Organisations funding the commons" dans la page [Ressources](##autres-ressources-en-ligne-remarquables)
    - Ajout de la slide [Présentations liées](#presentations-liées)
  - Version 1.6 :
    - Ajouts dans la page [Revue de presse](#revue-de-presse)
    - Ajout du lien vers le nouvel [espace Matrix](https://matrix.to/#/#the-librery:multi.coop) dédié
    - Ajout de [Open Source Pledge](https://osspledge.com/) dans la page [Inspirations](#inspirations) et ajouts dans la page [Remerciements](#remerciements)
    - Ajout du diagramme de Sankey sur la [répartition des fonds](#répartition-des-fonds-collectés)
    - Ajout de la slide [Revue de presse](#revue-de-presse)
    - Ajout de la slide sur les risques d'[épuisement de l'écosystème](le-risque-dépuisement-de-lécosystème) et de [sécurité](#le-risque-de-faille-de-sécurité)
    - Ajouts dans la page [Remerciements](#remerciements)
    - Ajout de la verticale de slides sur les[scénarios](#scenarios)
    - Ajout de la verticale de slides sur l'[internationalisation](#internationalisation)
  - Version 1.5 :
    - Updates dans la page [Remerciements](#remerciements) - ajout d'[Olivier Charbonneau](https://www.linkedin.com/in/olivier-charbonneau-b210491/)
    - Ajout du lien vers [Open Patent](https://www.boldandopen.com/open-patent) dans la page [Inspirations](#inspirations)
    - Ajout de la slide focus sur la licence "[Post-Open](#focus-sur-la-licence-post-open)"
    - Updates dans la page [Remerciements](#remerciements) + ajouts de liens + ajout de [Bruce Perens](https://perens.com/)
    - Ajout du lien vers des informations sur les [Fiducies d'intérêt social]() dans la page [Inspirations](#inspirations)
    - Ajout de la [version en anglais de la présentation](https://julien_p.gitlab.io/the-librery-slides/presentation-en.html)
    - Ajouts dans la page [Remerciements](#remerciements)
  - Version 1.4 :
    - Ajout du lien vers la publication du think tank [Open Future](https://openfuture.eu/publication/european-public-digital-infrastructure-fund/)
    - Ajout du lien vers l'article sur [Bruce Perens](https://programmation.developpez.com/actu/352421/Qu-est-ce-qui-vient-apres-l-open-source-Un-pionnier-du-mouvement-de-l-open-source-affirme-qu-il-faut-changer-de-paradigme-et-trouver-un-moyen-equitable-de-remunerer-les-developpeurs/), de [KDE e.V.](https://ev.kde.org/), et de [Open Source Initiative](https://opensource.org/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
:::

## Archives (v.1.0 - v.1.3)

:::{.text-nano-grey}

- **Version 1.x** - novembre 2023 / février 2024 :
  - Version 1.3 :
    - Ajouts dans la page [Remerciements](#remerciements)
    - Ajout des liens vers [Open Infra](https://openinfra.dev/), [Open North](https://opennorth.ca/), et [Green Software Foundation](https://greensoftware.foundation/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Ajout du lien vers [Funding the commons](https://fundingthecommons.io/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Ajout du lien vers le [Digital Transparency Lab](https://www.transparencylab.ca/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Ajout de la revue [Imaginaire Communs](https://anis-catalyst.org/communs/imaginaire-communs/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Chapitre à part pour la partie [Inspirations](#inspirations)
    - Version parallèle simplifiée, réduite uniquement sur Librery + [url dédiée](https://julien_p.gitlab.io/the-librery-slides/librery.html)
    - Ventilation des chapitres dans le répertoire `/texts` ([voir sur le repo](https://gitlab.com/Julien_P/the-librery-slides/-/tree/main/texts))
  - Version 1.2 : Mises à jour
    - Ajout du lien [Compte commun des communs](https://wiki.resilience-territoire.ademe.fr/wiki/LE_COMPTE_COMMUN_DES_COMMUNS) dans la page [Inspirations](#inspirations)
    - Ajouts dans la page [Ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Ajouts dans la page [Remerciements](#remerciements)
    - Ajout de la page [Focus sur Copie Publique](#focus-sur-linitiative-copie-publique)
    - Ajout de la _Software Freedom Conservancy_ dans la page [Inspirations](#inspirations)
  - Version 1.1 : Ajout de la slide [Place de l'Etat](#la-place-de-letat) et modifications mineures
  - Version 1.0 : Première version du document
  - Version 1.0 : Présentation lors de l'évènement NEC local à Lille "[Les communs pour un numérique au service de tous](https://openagenda.com/assembleurs/events/les-communs-pour-un-numerique-au-service-de-tous)"
:::

#

::: {.text-center}
**Merci pour votre attention !**

::: {.img-nano .img-no-caption}
![&nbsp;](images/Librery-logo-baudot-black.png)
:::

[https://julien_p.gitlab.io/the-librery-slides/librery.html](https://julien_p.gitlab.io/the-librery-slides/librery.html)

<i class="ri-mail-line"></i>&nbsp; julien.paris<i class="ri-at-line"></i>multi.coop
<br><i class="ri-message-2-line"></i> [salon Matrix](https://matrix.to/#/#the-librery:multi.coop)

:::
