
# Comment poursuivre <br>cette réflexion ?

:::{.text-micro}

<i class="ri-share-line mr-1" style="color: black;"></i>
**Partager et débattre** de ces idées auprès des acteurs des communs

<i class="ri-edit-2-line mr-1" style="color: black;"></i>
**Contribuer** sur le [pad de notes](https://hackmd.io/@Jpy/the-librery) ouvert en écriture et en commentaires

<i class="ri-list-unordered mr-1" style="color: black;"></i>
**Recenser** les expérimentations de mutualisation de moyens financiers en soutien aux communs

<i class="ri-map-pin-line mr-1" style="color: black;"></i>
**Cartographier** les acteurs / diffuseurs / producteurs / produits

<i class="ri-focus-3-line mr-1" style="color: black;"></i>
Identifier, classer et hiérarchiser une _shortlist_ de **communs “stratégiques”** pour l’Etat ou les collectivités

<i class="ri-message-2-line mr-1" style="color: black;"></i>
Recueillir **des témoignages et de nouvelles idées** auprès des producteurs de communs numériques

<i class="ri-pages-line mr-1" style="color: black;"></i>
Regrouper ces idées, la documentation, les témoignages sur un **site** en ligne

:::
