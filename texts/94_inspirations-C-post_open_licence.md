
## Focus sur la licence "post-open"

:::{.columns}
:::{.column width="40%"}

:::{.r-stack .no-margin}
:::{.fragment .semi-fade-out .img-no-caption .opaque data-fragment-index="1"}
![&nbsp;](images/post-open/postopen-01.png)
:::
:::{.fragment .img-no-caption .fade-in data-fragment-index="1"}
![&nbsp;](images/post-open/postopen-02.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-03.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-04.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-05.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-06.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-07.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-08.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-09.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-10.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-11.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-12.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-13.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-14.png)
:::
:::{.fragment .img-no-caption}
![&nbsp;](images/post-open/postopen-15.png)
:::

:::

:::

:::{.column .text-micro .text-no-margin-top width="60%"}
La [licence Post-Open](https://perens.com/2024/03/08/post-open-license-first-draft/) proposée par [Bruce Perens](https://wikipedia.org/wiki/Bruce_Perens) consiste à rendre contractuellement obligatoire pour les companies générant plus de 5 millions de USD$ par an utilisant et/ou modifiant le code source de : soit reverser le code sous la même licence ("POST-OPEN OPERATING AGREEMENT"), soit établir un contrat de rémunération pour des personnes ou entités chargées de maintenir ou améliorer le code source ("POST-OPEN PAID CONTRACT").

Les captures d'écran utilisées dans cette page sont extraites de cette [vidéo de Bruce Perens](https://www.youtube.com/watch?v=vTsc1m78BUk&list=PLfXKG51wk2sTBeAJtuL8wsUNvD-6lVDPM&index=3&t=1704s). Un autre [video de 2024](https://youtu.be/suUfS0-p5Yg?si=YRjwoLmnUXaToVqr) donne quelques informations supplémentaires.

_Un chaleureux merci à Bruce Perens avec qui l'auteur a pu échanger longuement sur ces sujets._

:::
:::

**Extrait de l'article 3.2 (PAID CONDITIONS) du _draft_ de la licence _post-open_**

:::{.text-nano-grey}
:::{.columns}
:::{.column width="50%"}
a)  The end-user revenue through all legal entities in which YOU have
    ownership exceeding 5% or equivalent control without ownership exceeds
    USD$5 Million anually. End-user revenue is all money or other value
    collected from customers, including the financial value equivalent of
    any non-monetary remuneration such as barter or the grant of rights or
    privileges.

b)  You provide, for remuneration, any work in the POST-OPEN COLLECTION to
    others, other than PERSONAL USE, or perform that provision at the order
    of another legal entity that receives remuneration for it. This includes
    (but is not limited to) provision of the work as a service; or inclusion
    of the work in a product that is sold, for example software that is sold
    or sale of a device containing the WORK.
:::

:::{.column width="50%"}
c)  You make MODIFICATIONS to any WORK in the POST-OPEN COLLECTION without
    performing one of these actions (you may perform both):

    I)  You enter into the POST-OPEN OPERATING AGREEMENT and make a PUBLIC
        RELEASE of the MODIFICATION.

    II) You enter into the POST-OPEN PAID CONTRACT.
:::
:::
:::
