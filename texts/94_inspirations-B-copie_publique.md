
## Focus sur l'initiative Copie Publique

:::{.columns}
:::{.column width="15%"}
![&nbsp;](images/copie-publique-logo.svg)
:::

:::{.column .text-micro .text-no-margin-top width="85%"}
L'initiative [Copie Publique](https://copiepublique.fr/) consiste en la description d'un **mécanisme d'allocation puis de redistribution** de fonds à des projets _open source_, qu'une entreprise peut librement choisir de mettre en oeuvre à condition d'en respecter la forme et l'esprit.

Le système proposé par **Copie Publique n'est pas une caisse partagée par plusieurs entreprise** : chaque entreprise  choisissant d'adopter le mécanisme Copie Publique abonde seule à son propre fonds, sur la base d'une assiette annuelle qu'elle choisit et rend publique. Une entreprise peut ainsi consacrer par exemple 1% de son CA, ou 3% de ses excédents nets de gestion, ou encore un montant fixe par an. La seule condition est que la règle soit claire, transparente, et permette d'abonder au fonds de façon récurrente.

Différents aspects du mécanisme Copie Publique sont intéressants à souligner, notamment ceux relatifs au **processus de sélection des projets soutenus** qu'a choisi l'entreprise Code Lutin et aux dynamiques que ce processus engendre :
:::
:::

:::{.text-nano-grey}
:::{.columns}
:::{.column width="50%"}
Le choix des projets soutenus se décompose en 2 phases en parallèle, partageant en deux enveloppes disctinctes la somme à redistribuer initialement :

- <b>une phase collective</b>, visant à identifier et choisir un nombre réduit de projets grâce à un "appel à candidatures" : les candidats envoient un descriptif du projet pour lequel ils demandent un soutien, et précisent également leurs besoins. Le collectif en choisit entre 1 et 3, suite à la mise en place d'un jury.
- <b>une phase plus individuelle</b>, où chaque salarié.e a la possibilité de redistribuer une enveloppe à un ou plusieurs projets de son choix (issus de l'appel à candidature ou non). Cette phase permet de soutenir davantage de projets de tailles plus modestes.
:::
:::{.column width="50%"}
<b>Tou.te.s les salarié.e.s de l'entreprise se mobilisent</b> durant le processus d'identification, d'évaluation, de sélection, et de rétribution des projets à soutenir. Chaque personne - indivuellement ou collectivement - peut ainsi contribuer activement au soutien de projets libres tiers, liberté qui n'existe pas dans n'importe quelle entreprise.

Suite à l'appel à candidature, un des moments décrit comme des plus enthousiasmants est celui de <b>la rencontre (présentielle ou en visio) avec les candidats</b>. Cette étape permet aux membres de l'entreprise de découvrir des individus et des technologies originales, de manière directe en conservant la dimension humaine et sociale des projets.
:::
:::
:::

:::{.text-nano-grey}
_Un chaleureux merci aux membres de [Code Lutin](https://www.codelutin.com/) avec qui l'auteur a pu échanger à Nantes, et qui ont pu décrire avec précision et passion leur intiative Copie Publique_
:::
