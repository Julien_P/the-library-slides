
# Problématique

::: {.img-medium}
![&nbsp;](images/hamlet-meme.jpg)
:::

:::fragment
::: {.text-center}
**Economiquement, industriellement et démocratiquement &nbsp; <br> &nbsp;une question de vie ou de mort...**
:::
:::

## Pas d'internet sans communs numériques

:::{.columns .align-center}

::: {.column width="65%"}
![&nbsp;](images/logos-open-source.png)
:::

::: {.column width="35%" .text-micro}
Les <b>services publics</b> pourraient-ils aujourd'hui se passer de tout ou partie des quelques outils libres montrés ici (liste absolument non exhaustive) ?

Ces mêmes outils sont par ailleurs [eux-mêmes basés sur des couches plus ou moins basses d'une multitude d'autres outils _open source_](https://medium.com/graph-commons/analyzing-the-npm-dependency-network-e2cf318c1d0d) (langages, librairies, bases de données, standards...).

Certaines entreprises dont les services reposent sur ces briques _open source_ ont pleinement conscience de cette <b>dépendance à un éco-système</b> extrêmement vaste et divers.

Parmi ces entreprises d'aucunes [contribuent et redistribuent la valeur](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/) qu'elles génèrent, d'autres [tentent à bas bruit de ré-internaliser ces briques et les personnes qui les maîtrisent le mieux](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/).
:::
:::

## Les enjeux principaux

:::incremental

- **Pérenniser** et **améliorer** des communs existants
- Aider à l'**émergence de nouveaux communs** numériques
- Développer la contribution aux communs comme **activité économique** à part entière
- Renforcer le **plaidoyer** pour une société des communs à l'heure où s'accentuent les risques d'épuisement des ressources
- Anticiper et prévenir les risques de perte de **souveraineté** numérique
- Préciser la **place des pouvoirs publics** dans le soutien aux communs

:::

## Les principaux constats

:::incremental

- La pérennité des communs numériques pose des **enjeux infrastructurels et stratégiques, voire démocratiques**
- De nombreux communs numériques sont **invisibles** car dans des couches plus ou moins basses des infrastructures numériques
- Les modèles économiques des communs numériques supposent - souvent - une logique de **valorisation de services autour des communs**, donc supposant d'être développés _ex-post_ après des investissements importants en R&D, en temps...
- L'équilibre économique des communs peut reposer sur des **sources de revenus très diversifiées** : dons, subventions, bénévolat, mécénat, ...
- Le **soutien direct des acteurs publics** peut aujourd'hui se décliner sous différentes formes : appels à projet, subventions, commande publique...
- L'économie générale des communs numériques et des _commoners_ est **singulière dans le paysage économique** et est de surcroît complexe à évaluer

:::

<!-- ::::::::: {data-visibility="hidden"} -->
## En quelques questions

:::incremental

- Comment développer l'**économie des communs numériques** comme une **filière industrielle essentielle** voire **désirable** ?
- Et si on considérait les “commoners” (contributeurs à des communs numériques) comme des **auteurs**, au sens du droit d’auteur ?
- Quelles sont les **exemples** qui dans l’histoire font écho à ces problématiques ?
- **Quelle structure** inventer pour répondre à ces enjeux ?
- Si les entreprises du numérique ont compris l'intérêt stratégique et économique des communs sur lesquels elles se sont développées, quel est le **niveau de prise de conscience des pouvoirs publics** ?

:::

<!-- ::::::::: -->

## Points d'attention

:::::: {.columns}

::: {.column}
:::fragment
### Se méfier du _business as usual..._
![&nbsp;](images/opensource-money-meme.jpg)

::: {.text-center}
**Consolider l'économie des communs numériques &nbsp; <br> &nbsp;en rémunérant directement le travail de leurs auteurs**
:::
:::
:::

::: {.column}
:::fragment
### _Or just shot yourself in the foot_
![&nbsp;](images/opensource-remove-meme.jpg)

::: {.text-center}
**Sans communs numériques &nbsp; <br> &nbsp;nos infrastructures numériques s'effondrent**
:::
:::
:::

::::::
