---
title: The Librery
subtitle: |
  <strong>2% pour les communs &nbsp; <br>&nbsp;et leurs auteur.e.s</strong>
  <br>
  <img src="images/qr-code-slides.png" alt="QR code" height="175px" style="margin: 5% 3%"/>
author: '
  <code>
    Version 1.2
  </code><br><br>
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _développeur FLOSS fullstack_
  </span><br>
  <span class="text-micro text-author-details">
    _co-fondateur de la [coopérative multi](https://multi.coop)_
  </span>
'
date: '
  <br>Navigation avec les flèches <i class="ri-drag-move-2-fill"></i> / Plan en pressant <span class="text-nano">`echap`</span>
  <br>
  <br>
  <span style="font-size: .7rem;" xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/Julien_P/the-librery-slides">The Librery - slides</a> par <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/Julien_P">J. Paris</a> est sous licence <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:.7rem!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:.7rem!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:.7rem!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></span> 
'
title-slide-attributes:
  data-background-image: "images/XKCD.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 45%"
  data-background-position: "right 20% top 40%"
---

# Sommaire

::: {.text-micro}

- [Prologue](#prologue)
- [Problématique](#problématique)
- [Les communs, condition et moteur de l’innovation](#les-communs-condition-et-moteur-de-linnovation)
- [Quelques notions centrales](#quelques-notions-centrales)
- [Pourquoi 2% de la commande publique ?](#pourquoi-2-de-la-commande-publique)
- [L’intérêt du droit d’auteur pour soutenir l’économie des communs](#lintérêt-du-droit-dauteur-pour-soutenir-léconomie-des-communs)
- [The Librery](#the-librery)
- [Collecte et redistribution](#collecte-et-redistribution)
- [Réflexions sur la gouvernance](#réflexions-sur-la-gouvernance)
- [Comment poursuivre cette réflexion ?](#comment-poursuivre-cette-réflexion)
- [Références et documentation](#références-et-documentation)
- [Remerciements](#remerciements)
- [Versions du document](#versions-du-document)
:::

# Prologue

::: {.text-center}
[https://julien_p.gitlab.io/the-librery-slides](https://julien_p.gitlab.io/the-librery-slides)
:::

Les <b>propos</b> figurant dans cette présentation <b>n'engagent que leur auteur</b>.

Ces réflexions et ces propositions <b>ne sont en rien figées</b> et représentent davantage <b>une étape dans une démarche de réflexion collective.</b>

Cet exercice de communication est à considérer avant tout comme une manière d'<b>ouvrir ces idées à la critique</b>, dans l'optique d'<b>aboutir à terme à un socle de propositions concrètes</b> pouvant être portées par un consortium le plus large possible d'acteurs des communs.

Tous les éléments présentés ici sont ouverts à la discussion et à la critique, notamment sur un [pad de notes](https://hackmd.io/@Jpy/the-librery) <b>ouvert en écriture et en commentaires</b>.

# Résumé des propositions phares

<!-- :::incremental
- Le **"2% communs numériques"** sur la commande publique
- Un fonds **géré de façon indépendante** par les acteurs des communs
- Une structure pouvant recevoir des **contributions financières publiques ou privées**
- Un organisme de redistribution **transparent**, **paritaire** et **à but non lucratif**
::: -->

:::::: {.columns}
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-money-euro-circle-fill"></i><br>
Le "**2% communs**" <br>sur les chantiers numériques publics
:::
:::
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-group-fill"></i><br>
Un fonds **géré de façon indépendante** <br>par les acteurs des communs
:::
:::
::::::

:::::: {.columns}
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-download-2-fill"></i><br>
Une structure pouvant recevoir des<br> **contributions financières publiques** <br> **ou privées**
:::
:::
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-scales-3-fill"></i><br>
Un organisme de redistribution <br>**transparent, paritaire** <br>et **à but non lucratif**
:::
:::
::::::

# Problématique

::: {.img-medium}
![&nbsp;](images/hamlet-meme.jpg)
:::

:::fragment
::: {.text-center}
**Economiquement, industriellement et démocratiquement &nbsp; <br> &nbsp;une question de vie ou de mort...**
:::
:::

## Pas d'internet sans communs numériques

:::{.columns .align-center}

::: {.column width="65%"}
![&nbsp;](images/logos-open-source.png)
:::

::: {.column width="35%" .text-micro}
Les <b>services publics</b> pourraient-ils aujourd'hui se passer de tout ou partie des quelques outils libres montrés ici (liste absolument non exhaustive) ?

Ces mêmes outils sont par ailleurs [eux-mêmes basés sur des couches plus ou moins basses d'une multitude d'autres outils _open source_](https://medium.com/graph-commons/analyzing-the-npm-dependency-network-e2cf318c1d0d) (langages, librairies, bases de données, standards...).

Certaines entreprises dont les services reposent sur ces briques _open source_ ont pleinement conscience de cette <b>dépendance à un éco-système</b> extrêmement vaste et divers.

Parmi ces entreprises d'aucunes [contribuent et redistribuent la valeur](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/) qu'elles génèrent, d'autres [tentent à bas bruit de ré-internaliser ces briques et les personnes qui les maîtrisent le mieux](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/).
:::
:::

## Les enjeux principaux

:::incremental

- **Pérenniser** et **améliorer** des communs existants
- Aider à l'**émergence de nouveaux communs** numériques
- Développer la contribution aux communs comme **activité économique** à part entière
- Renforcer le **plaidoyer** pour une société des communs à l'heure où s'accentuent les risques d'épuisement des ressources
- Anticiper et prévenir les risques de perte de **souveraineté** numérique
- Préciser la **place des pouvoirs publics** dans le soutien aux communs

:::

## Les principaux constats

:::incremental

- La pérennité des communs numériques pose des **enjeux infrastructurels et stratégiques, voire démocratiques**
- De nombreux communs numériques sont **invisibles** car dans des couches plus ou moins basses des infrastructures numériques
- Les modèles économiques des communs numériques supposent - souvent - une logique de **valorisation de services autour des communs**, donc supposant d'être développés _ex-post_ après des investissements importants en R&D, en temps...
- L'équilibre économique des communs peut reposer sur des **sources de revenus très diversifiées** : dons, subventions, bénévolat, mécénat, ...
- Le **soutien direct des acteurs publics** peut aujourd'hui se décliner sous différentes formes : appels à projet, subventions, commande publique...
- L'économie générale des communs numériques et des _commoners_ est **singulière dans le paysage économique** et est de surcroît complexe à évaluer

:::

<!-- ::::::::: {data-visibility="hidden"} -->
## En quelques questions

:::incremental

- Comment développer l'**économie des communs numériques** comme une **filière industrielle essentielle** voire **désirable** ?
- Et si on considérait les “commoners” (contributeurs à des communs numériques) comme des **auteurs**, au sens du droit d’auteur ?
- Quelles sont les **exemples** qui dans l’histoire font écho à ces problématiques ?
- **Quelle structure** inventer pour répondre à ces enjeux ?
- Si les entreprises du numérique ont compris l'intérêt stratégique et économique des communs sur lesquels elles se sont développées, quel est le **niveau de prise de conscience des pouvoirs publics** ?

:::

<!-- ::::::::: -->

## Points d'attention

:::::: {.columns}

::: {.column}
:::fragment
### Se méfier du _business as usual..._
![&nbsp;](images/opensource-money-meme.jpg)

::: {.text-center}
**Consolider l'économie des communs numériques &nbsp; <br> &nbsp;en rémunérant directement le travail de leurs auteurs**
:::
:::
:::

::: {.column}
:::fragment
### _Or just shot yourself in the foot_
![&nbsp;](images/opensource-remove-meme.jpg)

::: {.text-center}
**Sans communs numériques &nbsp; <br> &nbsp;nos infrastructures numériques s'effondrent**
:::
:::
:::

::::::

# Les communs, condition et moteur de l'innovation

:::{.r-stack .no-margin}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-00.png)
:::
:::{.fragment .fade-in data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-00-bis.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-01.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-02.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-03.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-04.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05-a.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05-b.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-06.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-06-bis.png)
:::
:::

## Le risque systémique

:::{.columns}
:::{.column width="50%"}
:::{.fragment .img-shadow .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/curl.png)
:::
:::
:::{.column width="50%"}
:::{.fragment .img-shadow .fade-in data-fragment-index="1"}
![&nbsp;](images/open-ssl.png)
:::
:::
:::

:::{.text-micro .text-center}
:::fragment
Exemples trouvés sur la page d'accueil de [Github sponsors](https://github.com/sponsors)
:::
:::

## L'économie des services publics <br> et des communs numériques

:::{.fragment .text-center .semi-fade-out .opaque data-fragment-index="1"}
::: {.img-small}
![&nbsp;](images/2-percents.png)
:::
:::

:::{.fragment .text-center data-fragment-index="1"}
<br>
<b>Un moteur qui tourne sur la réserve ?</b>
:::

# Quelques notions centrales

<br>

:::::: {.columns}
:::{.column .text-center .card width="25%"}
<i class="ri-shield-check-fill"></i><br>
Les communs **garants des infrastructures** et des **services publics** numériques
:::
:::{.column .text-center .card width="25%"}
<i class="ri-building-3-fill"></i><br>
Les communs comme **filière industrielle**
:::
::::::

:::::: {.columns}
:::{.column .text-center .card width="25%"}
<i class="ri-quill-pen-fill"></i><br>
Le.la _commoner_ comme **auteur.e**
:::
:::{.column .text-center .card width="25%"}
<i class="ri-money-euro-circle-fill"></i><br>
La **mutualisation** des fonds et de la gestion
:::
::::::

# Pourquoi 2% <br>de la commande publique ?

:::::: {.columns}
:::{.column .text-center .card width="33%"}
:::fragment
Un slogan **simple**
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Des **cadres réglementaires et juridiques** existants
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Des **sommes acceptables** sur des budgets de chantiers publics
:::
:::
::::::

:::::: {.columns}
:::{.column .text-center .card width="33%"}
:::fragment
Un mécanisme **essaimable** pour un passage à l'échelle
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Faire prendre conscience des **enjeux de souveraineté**
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Faire évoluer la **place de l'acteur public** dans le soutien aux communs
:::
:::
::::::

## Un slogan et un mécanisme simple

L'ambition est d'atteindre certains <b>ordres de grandeur en termes de montants collectés</b>, que ceux-ci soient suffisants pour irriguer et consolider une économie industrielle des communs.

Cette ambition suppose donc de s'adresser à de nombreux interlocuteurs dans l'administration et dans le privé, ayant chacun **des préoccupations, des métiers, des priorités ou des cadres d'action différents**.

Afin de pouvoir <b>généraliser le principe d'une collecte et d'une mutualisation</b> pour le soutien aux communs, il est donc nécessaire de proposer à tous les commanditaires un **mécanisme simple à comprendre** d'emblée et **simple à mettre en oeuvre**, quel que soit leur rattachement administratif ou leur secteur d'activité.

## Chiffres de cadrage et ordres de grandeurs

:::::: {.columns}

::: {.column .text-micro width="55%"}

<b>Panorama des grands projets SI de l'Etat</b>
<br>Septembre 2023, source [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/panorama-des-grands-projets-numeriques-de-letat/)
<br>Soit un total de **3 625 millions d'euros** environ

::: {.table-micro}

| Ministère nom complet                                                                 | Nom du projet              | Début        | Durée prévisionnelle en année | Phase du projet en cours          | Coût estimé |
|---------------------------------------------------------------------------------------|----------------------------|--------------|-------------------------------|-----------------------------------|-------------|
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | PAYSAGE                    | octobre 14   | 9,5                           | Déploiement / Bilan intermédiaire | 53,1        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | CFVR                       | juillet 13   | 9,5                           | Terminé                           | 34,5        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | PILAT                      | juin 18      | 7,6                           | Conception / Réalisation          | 123,5       |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | GMBI                       | septembre 18 | 5,6                           | Conception / Réalisation          | 35,7        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | NRP (NOUVEAU RÉSEAU DGFIP) | janvier 18   | 5,8                           | Conception / Réalisation          | 38,5        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | ROCSP                      | février 19   | 8,9                           | Expérimentation                   | 96,4        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | 3D                         | février 20   | 3,9                           | Expérimentation                   | 31,9        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | TNCP                       | janvier 20   | 4,4                           | Expérimentation                   | 21,1        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | PCR                        | octobre 19   | 3,2                           | Terminé                           | 52,8        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FRANCE SESAME              | janvier 20   | 4                             | Conception / Réalisation          | 10,9        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FONCIER INNOVANT           | novembre 20  | 3,2                           | Déploiement / Bilan intermédiaire | 33,1        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FICOBA 3                   | novembre 20  | 4                             | Conception / Réalisation          | 21          |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | Chorus – Projet S_4HANA    | septembre 22 | 2,1                           | Conception / Réalisation          | 87          |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FACTURATION ELECTRONIQUE   | mars 21      | 7,8                           | Conception / Réalisation          | 231         |
| Ministère de l'Europe et des Affaires étrangères                                      | SAPHIR                     | décembre 15  | 8                             | Déploiement / Bilan intermédiaire | 10,4        |
| Ministère de la Santé et de la Prévention                                             | SI SAMU                    | septembre 14 | 10,3                          | Conception / Réalisation          | 218,4       |
| Ministère de la Santé et de la Prévention                                             | Mon Espace Santé           | janvier 20   | 4                             | Conception / Réalisation          | 227,4       |
| Ministère de la Santé et de la Prévention                                             | ROR                        | janvier 21   | 5,5                           | Conception / Réalisation          | 24,9        |
| Ministère de la Santé et de la Prévention                                             | SI APA                     | juin 22      | 3,6                           | Conception / Réalisation          | 63,4        |
| Ministère de l'Agriculture et de la Souveraineté alimentaire                          | EXPADON 2                  | janvier 13   | 11                            | Déploiement / Bilan intermédiaire | 30,9        |
| Ministère de la Culture                                                               | MISAOA                     | juin 20      | 4                             | Conception / Réalisation          | 11,2        |
| Ministère des Armées                                                                  | ARCHIPEL                   | avril 15     | 8,8                           | Déploiement / Bilan intermédiaire | 14,3        |
| Ministère des Armées                                                                  | SOURCE WEB                 | janvier 14   | 9,8                           | Conception / Réalisation          | 15,3        |
| Ministère des Armées                                                                  | ROC                        | mars 16      | 8,1                           | Conception / Réalisation          | 15,6        |
| Ministère des Armées                                                                  | EUREKA                     | octobre 17   | 6,4                           | Conception / Réalisation          | 21,9        |
| Ministère des Armées                                                                  | SSLD-II                    | novembre 20  | 2,8                           | Conception / Réalisation          | 28,8        |
| Ministère des Armées                                                                  | SPARTA                     | février 18   | 6,3                           | Conception / Réalisation          | 15,8        |
| Services du Premier Ministre                                                          | NOPN                       | janvier 21   | 5,5                           | Conception / Réalisation          | 26,9        |
| Ministère de l'Éducation nationale et de la Jeunesse                                  | OP@LE                      | septembre 14 | 11                            | Déploiement / Bilan intermédiaire | 91,3        |
| Ministère de l'Éducation nationale et de la Jeunesse                                  | MIGRATION SIRH             | janvier 20   | 5,3                           | Conception / Réalisation          | 57,9        |
| Ministère de l'Éducation nationale et de la Jeunesse                                  | REURBANISATION SIRH        | janvier 20   | 5,3                           | Conception / Réalisation          | 68,7        |
| Ministère de la Transition écologique et de la Cohésion des territoires               | GEOPLATEFORME              | janvier 19   | 5,4                           | Déploiement / Bilan intermédiaire | 23,2        |
| Ministère de la Transition écologique et de la Cohésion des territoires               | OCSGE                      | septembre 19 | 5,8                           | Déploiement / Bilan intermédiaire | 30,4        |
| Ministère de la Transition écologique et de la Cohésion des territoires               | GUNENV Phase 2             | mars 23      | 4                             | Conception / Réalisation          | 37          |
| Ministère de l'Intérieur et des Outre-mer                                             | MCIC-2                     | février 15   | 9,9                           | Expérimentation                   | 24,2        |
| Ministère de l'Intérieur et des Outre-mer                                             | RRF                        | octobre 22   | 8                             | Conception / Réalisation          | 900,3       |
| Ministère de l'Intérieur et des Outre-mer                                             | NexSIS Version 1           | avril 17     | 8,2                           | Déploiement / Bilan intermédiaire | 72          |
| Ministère de l'Intérieur et des Outre-mer                                             | ANEF                       | janvier 18   | 5,9                           | Déploiement / Bilan intermédiaire | 54,5        |
| Ministère de l'Intérieur et des Outre-mer                                             | LOG-MI                     | septembre 17 | 7,8                           | Déploiement / Bilan intermédiaire | 28,1        |
| Ministère de l'Intérieur et des Outre-mer                                             | ERPC                       | mai 19       | 4,8                           | Déploiement / Bilan intermédiaire | 82,8        |
| Ministère de l'Intérieur et des Outre-mer                                             | KIOSQUES - PFSF            | avril 20     | 4                             | Déploiement / Bilan intermédiaire | 26,3        |
| Ministère de l'Intérieur et des Outre-mer                                             | FIN                        | février 20   | 5,3                           | Déploiement / Bilan intermédiaire | 68,3        |
| Ministère de l'Intérieur et des Outre-mer                                             | SIV                        | février 20   | En cadrage                    | Cadrage                           | En cadrage  |
| Ministère de l'Intérieur et des Outre-mer                                             | M@GRH                      | avril 21     | 2,8                           | Déploiement / Bilan intermédiaire | 14          |
| Ministère de la Justice                                                               | ASTREA                     | janvier 12   | 14                            | Conception / Réalisation          | 77,3        |
| Ministère de la Justice                                                               | PORTALIS                   | mars 14      | 12,8                          | Conception / Réalisation          | 98,9        |
| Ministère de la Justice                                                               | PROJAE – AXONE             | juin 17      | 7,1                           | Conception / Réalisation          | 14,5        |
| Ministère de la Justice                                                               | NED                        | janvier 18   | 4,8                           | Terminé                           | 8,7         |
| Ministère de la Justice                                                               | PPN                        | janvier 20   | 6                             | Conception / Réalisation          | 145,1       |
| Ministère de la Justice                                                               | ATIGIP                     | janvier 20   | 4,4                           | Déploiement / Bilan intermédiaire | 43,2        |
| Ministère du Travail, du Plein emploi et de l'Insertion                               | SI EMPLOI                  | novembre 21  | 4                             | Conception / Réalisation          | 33          |
| Ministère du Travail, du Plein emploi et de l'Insertion                               | SI FSE                     | janvier 20   | 6                             | Déploiement / Bilan intermédiaire | 30,1        |

:::
:::

::: {.column .text-micro width="45%"}

<b>Conduite des grands projets numériques de l'Etat</b>
<br>2020, source [Cour des Comptes](https://www.ccomptes.fr/fr/publications/la-conduite-des-grands-projets-numeriques-de-letat)
<br>Soit une moyenne de **343,631  millions d'euros / an** entre 2016 et 2018

![&nbsp;](images/cdc/cdc-07.png)

:::
::::::

## Prendre conscience <br>des enjeux de souveraineté

- Les outils libres situés dans les couches basses et peu visibles des différents services numériques mis en place par les pouvoirs publics posent des **questions de criticité et de risques** (voir [la note du Quai d'Orsay](https://www.diplomatie.gouv.fr/IMG/pdf/20200731-note-complete-communs_cle021839.pdf)) : sécurité, complexité technique, mises à jour, maintenabilité...

- Le bon fonctionnement des outils "_low level_" peut être une dimension sous-évaluée lors des **calculs de risques** des chantiers numériques, chantiers qui ont parfois une **dimension infrastructurelle** (_cloud_ souverain, données de santé, logiciel de paie des armées...).

- Des outils "_low level_" parfois critiques peuvent être **maintenus par des individus** hors des frontières hexagonales, **hors des compétences sectorielles** des administrations commanditaires, et pourtant se révéler essentielles au bon fonctionnement des services mis en oeuvre ([exemples de `cURL` ou `openSSL`](https://github.com/sponsors), ou celui de la librairie [`core.js`](https://github.com/zloirock/core-js/blob/master/docs/2023-02-14-so-whats-next.md)).

## La place de l'Etat

### L'Etat **facilitateur des communs**

:::{.text-micro}
Avant de jouer un rôle d'acteur ou producteur de communs, l'Etat peut endosser en priorité le rôle de <b>garant du cadre</b> pour la production de communs d'intérêt général par la société civile. Ce cadre est avant tout juridique et législatif (RGPD, RGAA, DMA...) mais le rôle de garant de ce cadre se joue également par des actions d'accompagnement et d'animation, comme le proposent déjà des acteurs publics tels que l'ANCT, l'IGN ou la DINUM. 
:::

### L'Etat porteur d'une **politique industrielle**

:::{.text-micro}
L'Etat, mais également toutes les administrations annexes (agences) ou décentralisées (collectivités), peuvent <b>soutenir l'économie industrielle et les emplois que représentent les acteurs privés produisant les communs</b>, en pérennisant des modes de financements directs divers dont le "2% communs", en complément des autres modes de soutien historiques.
:::

## Un mécanisme de financement pérenne

- Une **alternative aux financements ponctuels** (subventions, appels à projets, fonds _ad hoc_...)

- Une prise en compte des **ordres de grandeur** nécessaires au soutien à une réelle économie des communs

- Une capacité à "**passer à l'échelle**" : soit en jouant sur le pourcentage, soit en élargissant à d'autres secteurs que le numérique

## Inspirations

:::{.text-micro}
- Le mécanisme existant du "**1% artistique**" dans les [constructions publiques](https://www.culture.gouv.fr/Thematiques/Arts-plastiques/Commande-artistique/Le-1-artistique)

- La proposition de "**2% pour 2 degrés**" de l'[Institut Rousseau](https://institut-rousseau.fr/2-pour-2c-resume-executif/)

- Différentes initiatives venant du monde du logiciel libre ou d'institutions :
:::

:::{.columns .text-micro}
:::{.column width="60%"}
- [AFNIC](https://www.afnic.fr/en/)
- [Compte commun des Communs](https://wiki.resilience-territoire.ademe.fr/wiki/LE_COMPTE_COMMUN_DES_COMMUNS)
- [Contributive.org](https://contributive.org/fr/)
- [Copie Publique](https://copiepublique.fr/) par Code Lutin
- [Document Foundation](https://www.documentfoundation.org/) par et pour Libre Office
- [Fondation Blue Hat](https://bzg.fr/le-logiciel-libre-a-besoin-d-une-vraie-strategie-de-mutualisation-au-sein-de-letat/)
- [Fondation Wikimedia](https://wikimediafoundation.org/fr/) par et pour Wikimedia
- [Fonds de Dotation du Libre](https://www.fdl-lef.org/)
- [FOSS funders](https://fossfunders.com/)
- [Fund for Defense of Net Neutrality](https://fdn2.org/en/presentation.html)
- [Github sponsors](https://github.com/sponsors)
- [Libera Pay](https://liberapay.com/)
:::
:::{.column width="40%"}
- [NLnet](https://nlnet.nl/) - [NGI e-Commons Fund](https://nlnet.nl/news/2023/20230719-eCommonsFund.html)
- [Oceco](https://movilab.org/wiki/Oceco) par Communecter
- [Open Collective](https://opencollective.com/opensource)
- [Open Source Initiative](https://members.opensource.org/donate/)
- [OWASP Foundation](https://owasp.org/donate/)
- [Proton Lifetime Fundraiser](https://proton.me/blog/lifetime-fundraiser-survey-2023)
- [SARD - Société de Répartition des Dons](https://framablog.org/2009/09/05/sard-hadopi/)
- [Software Freedom Conservancy](https://sfconservancy.org/)
- [Sovereign Tech Fund](https://sovereigntechfund.de/en/) (Allemange)
- [Stakes.social](https://stakes.social/)
- [Thanks.dev](https://thanks.dev/home)
- [TOSIT - The Open Source I Trust](https://tosit.fr/)
:::
:::

## Focus sur l'initiative Copie Publique

:::{.columns}
:::{.column width="15%"}
![&nbsp;](images/copie-publique-logo.svg)
:::

:::{.column .text-micro .text-no-margin-top width="85%"}
L'initiative [Copie Publique](https://copiepublique.fr/) consiste en la description d'un **mécanisme d'allocation puis de redistribution** de fonds à des projets _open source_, qu'une entreprise peut librement choisir de mettre en oeuvre à condition d'en respecter la forme et l'esprit.

Le système proposé par **Copie Publique n'est pas une caisse partagée par plusieurs entreprise** : chaque entreprise  choisissant d'adopter le mécanisme Copie Publique abonde seule à son propre fonds, sur la base d'une assiette annuelle qu'elle choisit et rend publique. Une entreprise peut ainsi consacrer par exemple 1% de son CA, ou 3% de ses excédents nets de gestion, ou encore un montant fixe par an. La seule condition est que la règle soit claire, transparente, et permette d'abonder au fonds de façon récurrente.

Différents aspects du mécanisme Copie Publique sont intéressants à souligner, notamment ceux relatifs au **processus de sélection des projets soutenus** qu'a choisi l'entreprise Code Lutin et aux dynamiques que ce processus engendre :
:::
:::

:::{.text-nano-grey}
:::{.columns}
:::{.column width="50%"}
Le choix des projets soutenus se décompose en 2 phases en parallèle, partageant en deux enveloppes disctinctes la somme à redistribuer initialement :

- <b>une phase collective</b>, visant à identifier et choisir un nombre réduit de projets grâce à un "appel à candidatures" : les candidats envoient un descriptif du projet pour lequel ils demandent un soutien, et précisent également leurs besoins. Le collectif en choisit entre 1 et 3, suite à la mise en place d'un jury.
- <b>une phase plus individuelle</b>, où chaque salarié.e a la possibilité de redistribuer une enveloppe à un ou plusieurs projets de son choix (issus de l'appel à candidature ou non). Cette phase permet de soutenir davantage de projets de tailles plus modestes.
:::
:::{.column width="50%"}
<b>Tou.te.s les salarié.e.s de l'entreprise se mobilisent</b> durant le processus d'identification, d'évaluation, de sélection, et de rétribution des projets à soutenir. Chaque personne - indivuellement ou collectivement - peut ainsi contribuer activement au soutien de projets libres tiers, liberté qui n'existe pas dans n'importe quelle entreprise.

Suite à l'appel à candidature, un des moments décrit comme des plus enthousiasmants est celui de <b>la rencontre (présentielle ou en visio) avec les candidats</b>. Cette étape permet aux membres de l'entreprise de découvrir des individus et des technologies originales, de manière directe en conservant la dimension humaine et sociale des projets.
:::
:::
:::

:::{.text-nano-grey}
_Un chaleureux merci aux membres de [Code Lutin](https://www.codelutin.com/) avec qui l'auteur a pu échanger à Nantes, et qui ont pu décrire avec précision et passion leur intiative Copie Publique_
:::

## Une mise en oeuvre à préciser

- Le **cadre réglementaire existant est-il suffisant** ? Y a-t-il un risque juridique d'être accusé de distorsion de concurrence ?

- Quels chantiers, dans quelles administrations partenaires, avec quels alliés, pour **expérimenter dès maintenant** ?

- Quelle stratégie pour **aller porter ce principe devant les décisionnaires** dans l'administration ?

## Quelques idées complémentaires

- Commencer par le public, élargir au privé (ou partage de l'effort) : serait-il intéressant (pour des raisons politiques ou économiques) de décomposer le "2%" en "**2% = 1% public + 1% privé**" ? C'est-à-dire 1% de la commande publique en amont, et 1% du CA ou des factures des entreprises privées en maîtrise d'oeuvre ?

- Mettre **des chiffres en face des idées** : évaluer quels sont les montants d'argent public actuellement dépensés en chantiers numériques pour établir une sorte de prévisionnel

# L'intérêt du droit d'auteur pour <br>soutenir l'économie des communs

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
Les communs numériques sont des **oeuvres collectives ou de collaboration**
:::
:::{.column .card width="25%"}
Les _commoners_ sont des **auteur.e.s**
:::
:::{.column .card width="25%"}
Contribuer peut être reconnu comme un **travail**
:::
:::{.column .card width="25%"}
La gestion des droits d'auteurs libres demande une structure **mutualisée**
:::
::::::

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
L'**économie des communs numériques** est singulière et très diversifiée
:::
:::{.column .card width="25%"}
Utiliser le **droit existant et communautaire** comme levier
:::
:::{.column .card width="25%"}
Apprendre des **expériences économiques et juridiques passées**
:::
:::{.column .card .card-transparent width="25%"}
&nbsp;
:::
::::::

## Les communs numériques sont <br>des oeuvres collectives ou de collaboration

- Le numérique est une **industrie culturelle**, au même titre que l'édition, le cinéma ou la musique

- Les communs numériques (logiciels, bases de données) peuvent être considérés comme des **oeuvres** de l'esprit

## Les _commoners_ sont des auteur.e.s

- Les **contributeurs** et/ou initiateurs de communs numériques peuvent être considérés comme **des auteurs ou co-auteurs** (cf URSSAF Limousin)

- **La notion d'auteur est encadrée juridiquement** en France : un auteur de logiciel libre conserve des droits patrimoniaux vis-à-vis de son oeuvre

- Une oeuvre libre / commun numérique est potentiellement une **oeuvre collective ou de collaboration** (d'où la notion de co-auteur)

## Contribuer est un travail

- Contribuer à un logiciel ou à une base de données est une **activité à haute valeur ajoutée**

- On peut **cumuler le fait d'être auteur et avoir une activité salariée**

- Une oeuvre libre peut être développée par un salarié **au sein d'une entreprise**
  - L'auteur reste le salarié (une personne morale ne peut pas être auteur)
  - L'entreprise est titulaire des droits, sous condition que cela soit précisé dans un contrat de cession de droits d'auteurs
  - Cas particulier des fonctionnaires développant des communs dans le cadre de leurs fonctions.

## La gestion des droits d'auteurs libres <br>demande une structure mutualisée

- Tout comme cela a été le cas dans le domaine de la musique ou de l'édition, il est en pratique **impossible pour un auteur particulier de faire lui-même** :

  - le **suivi** de l'ensemble des modes d'exploitation de ses oeuvres

  - la **défense** de ses droits auprès des diffuseurs

- Les nouvelles oeuvres libres sont **initiées par des personnes physiques**, parfois en tant que salarié dans des entreprises mais pas toujours.

## L'économie des communs numériques <br>est singulière

::: {.text-center}
`"Un logiciel est libre lorsqu'il est payé"`
:::

- Une oeuvre libre n'est - en première approximation - pas destinée à directement recevoir de revenus d'exploitation, l'une des spécificités des communs numériques est leur **gratuité dans leur exploitation et leur diffusion**.

- Les flux financiers liés à la production d'un commun numérique ne peuvent se trouver qu'**en amont de leur publication**

## Utiliser le droit comme levier

Une <b>licence libre</b> ne signifie pas que l'auteur n'a plus de droits, or :

- quelle entité existe-t-il pour **défendre ces droits** ?

- quand bien même y aurait-il une entité pour défendre ces droits il n'y a pas clairement de **flux financiers** fléchés en vue de leur soutien, mais au contraire une jungle de modèles économiques différents

## Apprendre des expériences passées

- Il est nécessaire de **défendre et promouvoir les personnes / auteurs**, leur proposer un modèle de revenus, pour inciter les personnes à contribuer à des communs numériques et surtout d'en tirer des revenus (en partie ou totalement)

- Il existe déjà et il y a eu des **expérimentations** / propositions d'évolution de la loi (licence globale, revenu universel, ...), mais ces propositions n'ont pas toujours connu de succès et/ou sont encore peu opérationnelles

- Plutôt que chercher à ajouter à la loi et à la réglementation il est possible de **s'appuyer sur le droit existant**, de s'inspirer d'exemples historiques divers tels que la [redevance copie privée](https://www.culture.gouv.fr/Espace-documentation/Rapports/Rapport-du-Gouvernement-au-Parlement-sur-la-remuneration-pour-copie-privee-octobre-2022), la création d'[organismes de gestion collective](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006069414/LEGISCTA000006146355/2222-02-22) comme la SACEM ou la SACD, ou encore de la notion d' ["exception culturelle" à la française](https://fr.wikipedia.org/wiki/Exception_culturelle)

# <span style="display:none">The Librery</span>

:::{.img-librery .img-no-margin .img-no-caption}
![&nbsp;](images/Librery-logo-baudot-black.png)
:::

:::{.text-center}
<b>Penser un mécanisme de financement pérenne 
  <br> et un organisme de gestion collective (OGC)
  <br> par et pour les auteur.e.s de communs
</b>
:::

::::::{.columns .text-center .text-micro}
:::{.column .card .card-secondary width="25%"}
<i class="ri-download-2-fill"></i><br>
La **collecte,**<br>**la mutualisation,**<br>et la **redistribution** <br>des fonds
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-scales-3-fill"></i><br>
Une exigence forte de **transparence, de non-lucrativité et de parité**
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-share-fill"></i><br>
Des formes diversifiées de **soutien aux communs et aux _commoners_**
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-shield-check-line"></i><br>
La possibilité <br>d'un **label**
:::
::::::

## Economie générale

:::{.r-stack}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-03-bis.png)
:::
:::{.fragment .fade-in data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-07.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-08.png)
:::
:::

## Missions de l'OGC

- **collecter** les contributions financières des diffuseurs
- **recenser** des communs et leurs auteurs
- **calculer** ce qui doit être réparti (clés de répartition)
- **redistribuer** en droits / déclarer à l'URSSAF (pour simplifier la vie des auteurs)
- **défendre** la cause et les droits des auteurs de communs numériques
- **promouvoir** le statut d'auteur de commun numériques
- **faire entrer dans la loi** des clauses équivalentes à celles relatives au CNC / SACD / etc... (régime de contribution obligatoire ?)

## Contributions financières <br>en soutien à l'économie des communs

Dans un souci de mutualisation l'organisme de gestion collective doit être en capacité de pouvoir <b>recevoir différents types de flux financiers</b> potentiellement "entrants", argent public ou argent privé :

::: {.text-micro}
:::{.columns}
:::{.column width="50%"}

**Flux financiers potentiels d'origine publique**

- Plaidoyer pour le principe de <b>2% communs dans la commande publique</b> sur des chantiers numériques (inspiré du "[2% pour 2 degrés](https://institut-rousseau.fr/2-pour-2c-resume-executif/)", institut Rousseau ; ou du "[1% artistique](https://www.culture.gouv.fr/Thematiques/Arts-plastiques/Commande-artistique/Le-1-artistique)")
- Contributions financières de type <b>subvention</b> de certaines institutions publiques (IGN, ADEME, DINUM...)
- Intervention <b>au niveau européen</b> de soutien aux communs numériques, à l'image de [NGI](https://www.ngi.eu/), de [NLnet](https://nlnet.nl/), ou du projet [EDIC](https://digital-strategy.ec.europa.eu/en/policies/edic)
:::

:::{.column width="50%"}
**Flux financiers potentiels d'origine privée**

- Contributions financières (type don) de certaines <b>entreprises privées</b> bénéficiant/diffusant des communs numériques
- <b>Prestations de services</b> type audit de code, _sourcing_...
- Plaidoyer pour le principe d'une obligation de contribution minimale obligatoire - ou <b>redevance</b> - de certaines industries dont l'activité repose sur l'exploitation extensive de communs numériques stratégiques
<!-- - Possibilité de proposer à des acteurs privés la **garantie de bon fonctionnement de certaines librairies libres critiques**, en échange d'apports financiers redistribués directement vers les mainteneur.e.s de ces librairies. -->
:::
:::
:::

## Conditions de gouvernance

- Les communs relevant tout à la fois de la sphère publique que de la sphère privée il est souhaitable que **toutes les parties puissent être représentées** au sein de l'OGC, toutefois en assurant une bonne réprésentation des auteurs/individus, étant donné que la mission de l'OGC est bien de les défendre

- En tant que structure défendant une filière (les communs) l'OGC a davantage de **légitimité** en tant que structure de droit privé, sous réserve que les différentes parties prenantes puissent être justement représentées

- Intégrer à la gouvernance autant les **auteurs** que les **éditeurs** ou les **financeurs** et **diffuseurs** de communs numériques

## Conditions structurelles

Sur la base des critiques faites aux OGC culturelles (cf. Benhamou) on peut lister quelques <b>conditions nécessaires (mais pas suffisantes) pour éviter certains travers</b> des OGC historiques identifiés par les observateurs et le champ académique :

- **Frais de gestion** ne dépassant pas 5% à 10%
- **Suivi et automatisation de l'enregistrement** des auteur.e.s et de leur rétribution
- **Plafonnement** des salaires des dirigeant.e.s de l'OGC
- **Représentation** de tous les auteur.e.s (1 personne == 1 voix)
- **Transparence** totale de la gouvernance et du fonctionnement, notamment concernant le choix et la mise en oeuvre des clés de répartitions

## Equilibre de fonctionnement<br>(hypothèses)

::::::{.columns}
:::{.column width="50%"}
**Coûts de fonctionnement**

:::{.text-micro}
En comptant une équipe au complet (5 personnes à temps plein, voir plus loin dans la section "Réflexions sur la gouvernance") avec des salaires de 4&nbsp;000 € nets/mois, des bureaux, et quelques services complémentaires (comptable, abonnements, services en ligne...) :

en première approximation le <b>coût total de fonctionnement et de gestion tournerait autour de 550 k€ / an.</b>
:::
:::

:::{.column width="50%"}
**Collecte**

:::{.text-micro}
Si on maintient des coûts de fonctionnement et de gestion autour de 10%, cela signifie que l'<b>OGC doit être en mesure de collecter 5,5 millions € / an.</b>

En partant d'une base de "2% communs" sur les chantiers numériques, ce montant de collecte correspond à un ensemble de <b>chantiers numériques dont les budgets cumulés représenteraient 275 millions € / an</b> avant contribution aux communs.

:::
:::
::::::

**Redistribution**

:::{.text-micro}
En résumé, en appliquant le principe de "2% communs" sur un ensemble de chantiers numériques à hauteur de 275 M€ / an,
l'OGC serait en capacité de redistribuer : 5,5 M€ (collectés) - 0,55 M€ (frais de gestion)

soit <b>une redistribution de 4,95 M€ / an à des communs numériques.</b>
:::

## Véhicules juridiques possibles

Différents types de structures juridiques peuvent être imaginées pour remplir les missions de The Librery, avec pour point commun d'être à <b>but non lucratif</b> ou du moins à lucrativité limitée.

- L'**OGP** (Organisme de Gestion Collective)
- La **SGCDA** (Société de Gestion Collective des Droits d'Auteur )
- La **Fondation**
- La **Fiducie** (ou _trust_)
- La **SCIC** (Société Coopérative d'Intérêt Collectif)
- Le **GIP** (Groupement d'Intérêt Public)
- La **Mutuelle**
- L'**Association loi de 1901**

## Le label comme incitation

- L'existence d'un organisme légitime auprès de toutes les parties prenantes des communs numériques permettrait d'imaginer la création d'un "**label communs**", que l'organisme pourrait délivrer aux projets comme aux structures.

- La notion de "label communs" peut être un axe intéressant à développer pour **inciter les administrations et les entreprises à contribuer** à hauter de 2% de leurs chantiers numériques.

- La notion de label pose les questions des **critères objectifs** permettant de qualifier un commun numérique comme tel.

- L'autre pendant de la notion de label serait d'arriver à **identifier et suivre des indices de valorisation économique** (financiers, temps alloué, économies...) permettant de mieux situer les apports des communs numériques au sein de la filière industrielle.

## Idées de logo

<br>

:::{.columns .text-micro}
:::{.column width="50%"}
### Inspiré du [XKCD n°2347](https://xkcd.com/2347/)

![&nbsp;](images/Librery-logo-long-white.png)

![&nbsp;](images/Librery-logo-long-dots-01.png)
:::
:::{.column width="50%"}
### Inspiré du [code Baudot](https://fr.wikipedia.org/wiki/Code_Baudot)

![&nbsp;](images/code-baudot.png)

![&nbsp;](images/Librery-logo-baudot-black.png)
:::
:::

# Collecte et redistribution

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
Distinguer<br>**oeuvre,** **auteur,**<br>**diffuseur,** et **éditeur**
:::
:::{.column .card width="33%"}
Une approche de type <br>"**marché biface**"
:::
:::{.column .card width="33%"}
La notion de **contrats de rétribution** pour intégrer les particularités des parties prenantes
:::
::::::

## Notions principales

Les parallèles avec les autres industries culturelles sont les suivants :

- **Oeuvre** (individuelle ou de collaboration) : 1 logiciel libre (=== 1 chanson)
- **Auteur** : 1 développeur / designer (=== 1 artiste)
- **Diffuseur** : 1 entité/administration déployant un logiciel libre( === 1 radio)
- **Editeur** : 1 entreprise privée développant un produit libre (=== 1 maison de production)

## Mécanisme général

- **Auteurs** : il faut que les auteurs s'inscrivent, et qu'il déclarent à quelles oeuvres libres ils contribuent
- **Oeuvres** : il faut un catalogue d'oeuvres (lociels libres, BDD libres) sur lesquelles on peut avoir un suivi des contributions
- **Diffuseurs** : il faut qu'ils déclarent quelles oeuvres ils diffusent, et qu'ils contribuent financièrement à une caisse commune (via des conventions). Cette contribution peut être imaginée de différentes manières, cumulativement :
  - cotisations
  - et/ou contribution forfaitaire
  - et/ou contribution forfaitaire par commun
  - et/ou indexation sur un budget alloué à du développement de communs

## A/ Processus côté diffuseurs de communs

:::::::::::::: {.columns}
::: {.column width="50%"}
![&nbsp;](images/diffusors.png)
:::
::: {.column width="50%"}
::: {.text-micro}

- Est considéré comme **diffuseur** une entité morale ou physique prête à verser volontairement une somme afin de rémunerer les auteurs et co-auteurs d'un ou plusieurs communs

- Un diffuseur signe un **contrat de rétribution aux communs** avec l'OGC, l'OGC s'engage à redistribuer les montants volontairement versés par le diffuseur.

- Le contrat porte sur un **montant** de contribution volontaire sur un ou plusieurs communs, voire sans commun particulier, le tout sur une période donnée

- Ce contrat peut porter des clauses et **règles de redistribution**

- Le contrat est alors ventilé par **montants alloués par année et par commun**

- Tous les contrats et leurs montants associés sont **agregés par commun**, cela constitue un fonds par commun (et par année)
:::
:::
::::::::::::::

## A/ La notion de contrats de rétribution

En amont - c'est-à-dire vis-à-vis des contributeurs financiers (processus A/) - il serait possible d'associer à chaque contribution financière des **contrats de rétribution** précisant les différents <b>emplois des sommes</b>, les <b>conditions et règles</b> de redistribution, ainsi que les <b>parts allouées</b> à des communs spécifiques ou au contraire sans fléchage précis.

:::{.text-micro}
On pourrait imaginer qu'à **chaque contribution financière** puisse être associé un contrat précisant :

- **une ou plusieurs parts fléchées** sur un/des communs choisis par le contributeur / diffuseur, en raison de son intérêt direct à en déléguer la maintenance pour le bon fonctionnement de ses propres services.
- **une part plancher sans fléchage**, en vue d'une redistribution choisie par l'OGC sur d'autres communs "_low level_" ou à faible visibilité (part qui pourrait par exemple être reversée à des entités telles que Thanks.dev ou équivalent).
:::

## B/ Processus côté auteurs de communs

:::::::::::::: {.columns}
::: {.column width="50%"}
![&nbsp;](images/authors.png)
:::
::: {.column width="50%"}
::: {.text-micro}
- **Un.e auteur.e s'enregistre** à l'OGC en précisant à quelles oeuvres/communs il contribue

- Un **contrat de cession/gestion** de ses droits d'auteur est etabli entre l'OGC et l'auteur

- Ses **contributions** à des communs sont calculées et enregistrées au cours de l'année, commun par commun

- En fin d'année ses contributions permettent de **calculer le montant de la rétribution** qui lui est allouée, commun par commun, en fonction du fonds associé et des règles de redistribution

- L'OGC établit sa **déclaration de droits d'auteurs** auprès de l'URSSAF et transmet à l'auteur la preuve de déclaration

- _Nota_ : le processus côté auteur.e.s est celui qui demande à être le plus **rapide et automatisé** possible afin d'éviter toute lourdeur administrative et coûts exponentiels de gestion.
:::
:::
::::::::::::::

## C/ Cas particulier <br>des éditeurs de communs numériques

::: {.text-micro}

- Un éditeur de communs emploie **un.e ou des salarié.es, considéré.es comme les auteurs initiaux** des communs édités

- L'**éditeur est considéré comme titulaire des droits** d'auteurs de ses employé.e.s

- L'éditeur permet la **gestion des droits en son nom par l'OGC** auprès de l'URSSAF

- L'éditeur **enregistre ses employé.e.s** comme auteurs sur l'OGC

- L'OGC calcule et déclare les rétributions aux auteurs comme en B/, avec la particularité d'**enregistrer ces sommes à l'URSSAF au nom de l'éditeur** aussi mais envoie les documents à l'éditeur

:::

## Modèle de données complet simplifié

Voir le [document](https://gitlab.com/Julien_P/the-librery-slides/-/blob/main/Model.md) au format _markdown_ sur le _repo_.

:::::::::::::: {.columns}

::: {.column}
::: {.text-micro}
Partie 1/2
:::
::: {.img-no-margin}
![&nbsp;](images/model-simplified-01.png)
:::
:::

::: {.column}
::: {.text-micro}
Partie 2/2
:::
::: {.img-no-margin}
![&nbsp;](images/model-simplified-02.png)
:::
:::

::::::::::::::

# Réflexions sur la gouvernance

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
Un représentation de **toutes les parties prenantes** des communs
:::
:::{.column .card width="25%"}
Une majorité structurelle pour les **auteur.e.s**
:::
:::{.column .card width="25%"}
Des **ressources humaines et techniques** pour la mise en oeuvre des missions
:::
:::{.column .card width="25%"}
Des exigences de **transparence** et de **non-lucrativité** fortes
:::
::::::

## Diagramme général

:::::: {.columns}

::: {.column width="60%"}
![&nbsp;](images/global.png)
:::

::: {.column width="40%"}
::: {.text-micro}
Diagramme général de principe de The Librery (réalisé avec [Whimsical](https://whimsical.com/the-librery-organigramme-RimouvNB2jVq7pcu59zP4H))

De haut en bas :

- **Les parties prenantes** :
  - Auteurs
  - Oeuvres (communs)
  - Diffuseurs
  - Editeurs
  - Sympathisants
- **L'équipe de l'OGC**
- **Le fonds mutualisé** et ses différents usages
  - Gestion des droits
  - Aides ciblées
  - Programmes / actions
  - Promotion des communs
- **Les outils** mis à disposition
  - Portail web (+ backoffice)
  - Services tiers

:::
:::

::::::

## Les parties prenantes

![&nbsp;](images/stakeholders.png)

## Les moyens financiers : le fonds

![&nbsp;](images/fund.png)

## Les moyens humains : l'équipe

![&nbsp;](images/team.png)

## Les moyens techniques : les outils

![&nbsp;](images/tools.png)

# Comment poursuivre <br>cette réflexion ?

:::{.text-micro}

<i class="ri-share-line mr-1" style="color: black;"></i>
**Partager et débattre** de ces idées auprès des acteurs des communs

<i class="ri-edit-2-line mr-1" style="color: black;"></i>
**Contribuer** sur le [pad de notes](https://hackmd.io/@Jpy/the-librery) ouvert en écriture et en commentaires

<i class="ri-list-unordered mr-1" style="color: black;"></i>
**Recenser** les expérimentations de mutualisation de moyens financiers en soutien aux communs

<i class="ri-map-pin-line mr-1" style="color: black;"></i>
**Cartographier** les acteurs / diffuseurs / producteurs / produits

<i class="ri-focus-3-line mr-1" style="color: black;"></i>
Identifier, classer et hiérarchiser une _shortlist_ de **communs “stratégiques”** pour l’Etat ou les collectivités

<i class="ri-message-2-line mr-1" style="color: black;"></i>
Recueillir **des témoignages et de nouvelles idées** auprès des producteurs de communs numériques

<i class="ri-pages-line mr-1" style="color: black;"></i>
Regrouper ces idées, la documentation, les témoignages sur un **site** en ligne

:::

# Références et documentation

Quelques éléments bibliographiques, classés par supports et grandes thématiques, en complément des notes générales relatives aux réflexions autour de [The Librery](https://hackmd.io/@Jpy/the-librery).

<i class="ri-file-text-line"></i> [Pad de notes](https://hackmd.io/@Jpy/the-librery) ouvert en écriture et en commentaires

<i class="ri-file-pdf-2-line"></i> [Articles / livres en pdf (nextcloud)](https://nuage.liiib.re/s/dx5eribMSRm6RTm) en accès libre

<i class="ri-book-2-line"></i> [Bibliographie (Zotero)](https://www.zotero.org/groups/5255274/librery/library)

<i class="ri-movie-line"></i> [Vidéos / conférences (playlist Youtube)](https://www.youtube.com/playlist?list=PLfXKG51wk2sTBeAJtuL8wsUNvD-6lVDPM)

<i class="ri-gitlab-line"></i> [Fichier source](https://gitlab.com/Julien_P/the-librery-slides/-/blob/main/presentation-fr.md) et [ensemble de la présentation](https://gitlab.com/Julien_P/the-librery-slides) sur Gitlab

## Autres ressources en ligne remarquables

:::{.text-nano}
:::{.columns}
:::{.column width="50%"}
- ADEME (wiki) : [Résilience des territoires](https://wiki.resilience-territoire.ademe.fr/wiki/Accueil)
- ADEME (forum) : [Sobriété & Résilience des territoires](https://forum.resilience-territoire.ademe.fr/)
- ADULLACT (site) : [L'association](https://adullact.org/)
- AFULL - Association Francophone des Utilisateurs de Logiciels Libres (site) : [L'association](https://aful.org/)
- ANCT (site) : [Labo Société Numérique](https://labo.societenumerique.gouv.fr/fr/recherche?thematics=communs)
- Annuaire des Communs (site) : [Annuaire](https://annuaire.lescommuns.org/category/collectifs/)
- L'Assemblée des communs (site) : [Site internet](https://assemblee.lescommuns.org/)
- La Chambre des Communs (site) : [Site internet](https://chambre.lescommuns.org/gouvernance/)
- Les communs d'abord ! (média) : [Site internet](https://www.les-communs-dabord.org) / [Chat](https://chat.lescommuns.org/home)
- La Comptoir du Libre (site) : [Catalogue](https://comptoir-du-libre.org/fr/)
- Contributive Commons (licence) : [Portail](https://contributivecommons.org/)
- La Coop des Communs (site) : [L'association](https://coopdescommuns.org/fr/association/)
- La Fabmob (pdf) : [Socle de compréhension des communs numériques de la mobilité](https://cloud.fabmob.io/s/L3gJsa3EnYQNSTk)
:::
:::{.column width="50%"}
- La Fabrique à communs des Tiers-lieux (wiki) : [Movilab](https://movilab.org/wiki/Fabrique_%C3%A0_communs_des_tiers-lieux)
- IGN (wiki) : [Le Commun des communs](https://interlab.cc/wikign/?LeCommundescommuns)
- IGN (pdf) : [Guide des communs](https://www.ign.fr/files/default/2023-10/guide_communs_ouishare.pdf)
- Inno<sup>3</sup> (outil) : [Canevas pour la gouvernance de communs numériques](https://inno3.fr/realisation/canevas-pour-la-gouvernance-de-commun-numerique/)
- P2P Foundation (site) : [Site internet](https://p2pfoundation.net/)
- Le portail des communs (site) : [Portail](https://lescommuns.org/)
- NEC - Numérique en commun\[s\] (site) : [Ressources](https://numerique-en-communs.fr/les-ressources-nec/)
- La 27<sup>ème</sup> Région (site) : [Enacting the commons](https://enactingthecommons.la27eregion.fr/)
- Matti Schneider (site) : [Construire des communs numériques](https://communs.mattischneider.fr/)
- S.I.Lex (blog) : [Site internet](https://scinfolex.com/)
- SILL - Socle Interministériel des Logiciels Libres (site) : [Catalogue](https://code.gouv.fr/sill/)
- Le wiki des communs (wiki) : [lescommuns.org](https://wiki.lescommuns.org/)
:::
:::
:::

## Crédits

Présentation réalisée avec :

- [Pandoc](https://pandoc.org/)
- [Reveal.js](https://revealjs.com/)
- [Remix Icons](https://remixicon.com/)
- [Slider-template](https://gitlab.com/multi-coop/slider-template)

# Remerciements

**Les propos publiés dans cette présentation n'engagent que leur auteur.**

Toutefois l'auteur souhaite adresser <b>un grand merci</b> à toutes les personnes qui contribuent à enrichir cette réflexion et à <b>soutenir le mouvement pour une société des communs</b>.

::::::{.columns}

:::{.column}
::: {.text-nano-grey .text-center .text-compact}

**Relectures ou commentaires**

<br>

Vincent Bachelet

Jeanne Bretécher

Maïa Dereva

Brendan Brieg

Sébastien Broca

Margot Godefroi

Bastien Guerry

Alain Imbaud

Tibor Katelbach

Nicolas Loubet

Alex Morel

Benjamin Poussin

:::
:::

:::{.column}
::: {.text-nano-grey .text-center .text-compact}

**Discussions informelles**

<br>

:::{.columns}

:::{.column width="50%"}

Jaime Arredondo

Vincent Bergeot

Alexandre Bigot-Verdier

Mathilde Bras

Héloïse Calvier

Bertrand Denoncin

Virgile Deville

Angie Gaudion

Rémy Gerbet

Benjamin Jean

Claire-Marie Meriaux
:::

:::{.column width="50%"}

Gabriel Plassat

Benoît Ribon

Johan Richer

Simon Sarazin

Sébastien Shulz

:::

:::

:::
:::

::::::

## Un dernier mot...

<br>

:::{.columns}

:::{.column width="30%" .img-shadow}
![&nbsp;](images/an-01.jpg)
:::

:::{.column width="70%" .text-center}

:::{.text-large}
<br>
**L’utopie, ça réduit à la cuisson,**<br>
**c’est pourquoi**<br>
**il en faut énormément au départ**
:::

<br>

:::{.text-micro}
Citation de Gébé, ["L'an 01"](https://www.lassociation.fr/catalogue/l-an-01-livre-et-dvd/)
:::
:::

:::

# Versions du document

:::{.text-nano-grey}

- **Version 1.x** - novembre 2023:
  - Version 1.2 : Mises à jour
    - Ajouts dans la page "[Ressources remarquables](#autres-ressources-en-ligne-remarquables)"
    - Ajouts dans la page "[Remerciements](#remerciements)"
    - Ajout de la page "[Focus sur Copie Publique](#focus-sur-linitiative-copie-publique)"
    - Ajout de la _Software Freedom Conservancy_ dans la page "[Inspirations](#inspirations)"
  - Version 1.1 : Ajout de la slide "[Place de l'Etat](#la-place-de-letat)" et modifications mineures
  - Version 1.0 : Première version du document
  - Version 1.0 : Présentation lors de l'évènement NEC local à Lille "[Les communs pour un numérique au service de tous](https://openagenda.com/assembleurs/events/les-communs-pour-un-numerique-au-service-de-tous)"
:::

#

::: {.text-center}
**Merci pour votre attention !**

[https://julien_p.gitlab.io/the-librery-slides](https://julien_p.gitlab.io/the-librery-slides)

:::

::: {.img-medium .img-no-margin .img-no-caption}
![&nbsp;](images/XKCD.png)
:::
::: {.text-nano .text-center style="margin: -10px 0 40px 0"}
[XKCD n°2347](https://xkcd.com/2347/)
:::

::: {.text-center}
<i class="ri-mail-line"></i>&nbsp; julien.paris<i class="ri-at-line"></i>multi.coop
:::
