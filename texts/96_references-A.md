
# Références et documentation

Quelques éléments bibliographiques, classés par supports et grandes thématiques, en complément des notes générales relatives aux réflexions autour de [The Librery](https://hackmd.io/@Jpy/the-librery).

<i class="ri-message-2-line"></i> [Salon Matrix](https://matrix.to/#/#the-librery:multi.coop) pour continuer la discussion

<i class="ri-file-pdf-2-line"></i> [Articles / livres en pdf (nextcloud)](https://nuage.liiib.re/s/dx5eribMSRm6RTm) en accès libre

<i class="ri-book-2-line"></i> [Bibliographies (Zotero)](https://www.zotero.org/groups/5255274/librery/library)

<i class="ri-file-text-line"></i> [Pad de notes](https://hackmd.io/@Jpy/the-librery) ouvert en écriture et en commentaires

<i class="ri-movie-line"></i> [Vidéos / conférences (playlist Youtube)](https://www.youtube.com/playlist?list=PLfXKG51wk2sTBeAJtuL8wsUNvD-6lVDPM)

<i class="ri-gitlab-line"></i> [Fichiers sources](https://gitlab.com/Julien_P/the-librery-slides/-/tree/main/texts) et [ensemble de la présentation](https://gitlab.com/Julien_P/the-librery-slides) sur Gitlab

## Autres ressources en ligne remarquables
