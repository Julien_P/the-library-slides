
# Scénarios

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
Un **mécanisme simple**
:::
:::{.column .card width="33%"}
Approche **contractuelle**
:::
:::{.column .card width="33%"}
Différentes approches selon des **acteurs publics ou privés**
:::
::::::
