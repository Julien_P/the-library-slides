
# Internationalisation & décentralisation

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
Pouvoir s'adapter aux **spécificités juridiques locales**
:::
:::{.column .card width="33%"}
Pouvoir **mutualiser des jeux de données** (registres des auteurs ou des communs)
:::
:::{.column .card width="33%"}
Avoir une **architecture d'information décentralisée**
:::
::::::

## Pourquoi anticiper une approche internationale ?

#### Les contraintes de la production de communs numériques

::: {.text-micro}
- Les communs numériques peuvent être développés <b>partout dans le monde</b>.
- Les <b>commoners</b> contribuant à un même commun peuvent eux-mêmes être <b>basés dans des pays différents</b>.
- Les <b>contributeurs financiers aux communs</b> peuvent également être basés dans des pays différents.
- Un OGC est nécessairement basé juridiquement dans un pays, <b>le statut juridique d'un OGC peut donc être différent d'un pays à l'autre</b> (fondation, trust, association, fiducie...).
- Un OGC n'a pas forcément vocation - ou la capacité - à traiter l'ensemble des communs au niveau mondial, <b>il est plus réaliste d'envisager qu'un OGC ait un ancrage territorial et/ou sectoriel précis</b>.

Il est donc nécessaire que tous les OGC partout dans le monde puissent **partager des registres communs pour éviter les doublons et les incohérences juridiques** : registre des communs, registre des commoners, montants des contributions financières, allocations des sommes vers les communs et les commoners...
:::

## Architecture décentralisée

::: {.text-micro}
- Il serait envisageable - voire plus réaliste - qu'il existe **plusieurs OGC répartis partout dans le monde**, et/ou différents OGC "spécialisés" sur certaines verticales (secteur industriel, language de programmation, ...)

- Pour qu'un tel système fonctionne à l'échelle internationale l'**infrastructure d'information doit être partagée**, tout en permettant à chaque OGC de gérer son propre corpus de données.

- Techniquement il serait possible de proposer à tous les OGC un outil numérique libre de gestion basé sur [ActivityPods](https://activitypods.org) afin de pouvoir à la fois **décentraliser le stockage des données**, tout en permettant à **chaque instance de gérer ses propres jeux de données**.

:::

## Diagramme de principe

:::{.r-stack .no-margin}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/decentralized/diagram-01.png)
:::
:::fragment
![&nbsp;](images/diagrams/decentralized/diagram-02.png)
:::
:::fragment
![&nbsp;](images/diagrams/decentralized/diagram-03.png)
:::
:::fragment
![&nbsp;](images/diagrams/decentralized/diagram-99.png)
:::
:::
