
# Sommaire

::: {.text-micro}

- [Prologue](#prologue)
- [Problématique](#problématique)
- [Les communs, condition et moteur de l’innovation](#les-communs-condition-et-moteur-de-linnovation)
- [The Librery](#the-librery)
- [Collecte et redistribution](#collecte-et-redistribution)
- [Réflexions sur la gouvernance](#réflexions-sur-la-gouvernance)
- [Internationalisation & décentralisation](#internationalisation-décentralisation)
- [Scénarios](#scénarios)
- [Inspirations](#inspirations)
- [Comment poursuivre cette réflexion ?](#comment-poursuivre-cette-réflexion)
- [Références et documentation](#références-et-documentation)
- [Remerciements](#remerciements)
- [Versions du document](#versions-du-document)
:::
