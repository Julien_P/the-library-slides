
#

::: {.text-center}
**Merci pour votre attention !**

[https://julien_p.gitlab.io/the-librery-slides](https://julien_p.gitlab.io/the-librery-slides)

:::

::: {.img-medium .img-no-margin .img-no-caption}
![&nbsp;](images/XKCD.png)
:::
::: {.text-nano .text-center style="margin: -10px 0 40px 0"}
[XKCD n°2347](https://xkcd.com/2347/)
:::

::: {.text-center}
<i class="ri-mail-line"></i>&nbsp; julien.paris<i class="ri-at-line"></i>multi.coop
<br><i class="ri-message-2-line"></i> [salon Matrix](https://matrix.to/#/#the-librery:multi.coop)

:::
