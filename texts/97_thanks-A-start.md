
# Remerciements

**Les propos publiés dans cette présentation n'engagent que leur auteur.**

Toutefois l'auteur souhaite adresser <b>un grand merci</b> à toutes les personnes qui contribuent à enrichir cette réflexion et à <b>soutenir le mouvement pour une société des communs</b>.

::::::{.columns}

:::{.column}
::: {.text-nano-grey .text-center .text-compact}

**Relectures ou commentaires**

<br>
