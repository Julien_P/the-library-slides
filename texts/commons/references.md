
:::{.text-nano}
:::{.columns}

:::{.column width="50%"}

- ADEME (wiki) : [Résilience des territoires](https://wiki.resilience-territoire.ademe.fr/wiki/Accueil)
- ADEME (forum) : [Sobriété & Résilience des territoires](https://forum.resilience-territoire.ademe.fr/)
- ADULLACT (site) : [L'association](https://adullact.org/)
- AFULL - Association Francophone des Utilisateurs de Logiciels Libres (site) : [L'association](https://aful.org/)
- AIC - Accélérateur d'Inititaves Citoyennes : [Site internet](https://communs.beta.gouv.fr/)
- ANCT (site) : [Labo Société Numérique](https://labo.societenumerique.gouv.fr/fr/recherche?thematics=communs)
- Annuaire des Communs (site) : [Annuaire](https://annuaire.lescommuns.org/category/collectifs/)
- L'Assemblée des communs (site) : [Site internet](https://assemblee.lescommuns.org/)
- Chaire Socio Economie des Communs (univ. Lille) : [Laboratoire CLERSE](https://clerse.univ-lille.fr/)
- La Chambre des Communs (site) : [Site internet](https://chambre.lescommuns.org/gouvernance/)
- Les communs d'abord ! (média) : [Site internet](https://www.les-communs-dabord.org) / [Chat](https://chat.lescommuns.org/home)
- La Comptoir du Libre (site) : [Catalogue](https://comptoir-du-libre.org/fr/)
- Contributive Commons (licence) : [Portail](https://contributivecommons.org/)
- La Coop des Communs (site) : [L'association](https://coopdescommuns.org/fr/association/)
- Collectif de Recherche sur les Initiatives, Transformations et Institutions des Communs (site) : [Site internet](https://critic-communs.ca/)
- Digital Transparency Lab (non-profit organisation) : [Site internet](https://www.transparencylab.ca/)
- En communs (revue) : [Site internet](https://www.encommuns.net/)
- La Fabmob (pdf) : [Socle de compréhension des communs numériques de la mobilité](https://cloud.fabmob.io/s/L3gJsa3EnYQNSTk)
- La Fabrique à communs des Tiers-lieux (wiki) : [Movilab](https://movilab.org/wiki/Fabrique_%C3%A0_communs_des_tiers-lieux)
- Liraries.io (site) : [Site internet](https://libraries.io/)
- FOSS Sustainability : [Site internet](https://fosssustainability.com/)

:::

:::{.column width="50%"}

- Funding the commons (rencontres) : [Site internet](https://fundingthecommons.io/)
- Green Software Foundation : [Site internet](https://greensoftware.foundation/)
- IGN (wiki) : [Le Commun des communs](https://interlab.cc/wikign/?LeCommundescommuns)
- IGN (pdf) : [Guide des communs](https://www.ign.fr/files/default/2023-10/guide_communs_ouishare.pdf)
- Imaginaire Communs (revue) : [Site internet](https://anis-catalyst.org/communs/imaginaire-communs/) 
- Inno<sup>3</sup> (outil) : [Canevas pour la gouvernance de communs numériques](https://inno3.fr/realisation/canevas-pour-la-gouvernance-de-commun-numerique/)
- Matti Schneider (site) : [Construire des communs numériques](https://communs.mattischneider.fr/)
- NEC - Numérique en commun\[s\] (site) : [Ressources](https://numerique-en-communs.fr/les-ressources-nec/)
- Numérique d'Intérêt Général - Cadre de référence : [Site internet](https://www.numeriqueinteretgeneral.org/)
- Open Future : [Publication](https://openfuture.eu/publication/european-public-digital-infrastructure-fund/)
- Open Infra : [Site internet](https://openinfra.dev/)
- Open North : [Site internet](https://opennorth.ca/)
- Open Source Initiative : [Site internet](https://opensource.org/)
- Organisations funding the commons (curated list by Jaime Arredondo) : [Page Notion](https://boldandopen.notion.site/Organisations-Funding-the-Commons-Open-source-projects-93c482ba51d7498eac4ecc50275b9d7f)
- P2P Foundation (site) : [Site internet](https://p2pfoundation.net/)
- Le portail des communs (site) : [Portail](https://lescommuns.org/)
- "Qu'est-ce qui vient après l'open source ?" article de Mathis Lucas sur une présentation de [Bruce Perens](https://en.wikipedia.org/wiki/Bruce_Perens) : [article et vidéo](https://programmation.developpez.com/actu/352421/Qu-est-ce-qui-vient-apres-l-open-source-Un-pionnier-du-mouvement-de-l-open-source-affirme-qu-il-faut-changer-de-paradigme-et-trouver-un-moyen-equitable-de-remunerer-les-developpeurs/)
- S.I.Lex (blog) : [Site internet](https://scinfolex.com/)
- SILL - Socle Interministériel des Logiciels Libres (site) : [Catalogue](https://code.gouv.fr/sill/)
- Sustain OSS (podcast) : [Site internet](https://podcast.sustainoss.org/)
- Transcendance (article) : [Site internet](https://democratie-action.notion.site/Transcendance-outil-opensource-et-collaboratif-pour-l-mergence-de-mod-les-conomiques-d-int-r-t-g--0ce0f7039dc04661921c9416a35b9796)
- Le wiki des communs (wiki) : [lescommuns.org](https://wiki.lescommuns.org/)
- La 27<sup>ème</sup> Région (site) : [Enacting the commons](https://enactingthecommons.la27eregion.fr/)

:::

:::
:::
