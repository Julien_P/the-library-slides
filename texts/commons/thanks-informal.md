
:::{.columns}

:::{.column width="50%"}

[Jaime Arredondo](https://www.linkedin.com/in/jaimearredondo/)

[Vincent Bergeot](https://www.linkedin.com/in/vincent-bergeot-aa95b82b/)

[Alexandre Bigot-Verdier](https://www.linkedin.com/in/alexandre-bigot-verdier-27348920/)

[Mathilde Bras](https://www.linkedin.com/in/mathildebras/)

[Dorie Bruyas](https://www.linkedin.com/in/dorie-bruyas/)

[Héloïse Calvier](https://www.linkedin.com/in/h%C3%A9lo%C3%AFse-calvier-040a85154/)

[Valentin Chaput](https://www.linkedin.com/in/valentinchaput/)

[Bertrand Denoncin](https://www.linkedin.com/in/bertranddenoncin/)

[Virgile Deville](https://www.linkedin.com/in/virgiledeville/)

[Angie Gaudion](https://www.linkedin.com/in/aggaudion/)

[Rémy Gerbet](https://www.linkedin.com/in/r%C3%A9my-gerbet-511249a1/)

[Maxime Guedj](https://www.linkedin.com/in/maximeguedj/)

:::

:::{.column width="50%"}
[Jan Krewer](https://www.linkedin.com/in/jan-krewer/)

[Benjamin Jean](https://www.linkedin.com/in/benjaminjean/)

[Tobie Langel](https://www.linkedin.com/in/tobielangel/)

[Claire-Marie Meriaux](https://www.linkedin.com/in/clairemarie/)

[Aude Nanquette](https://www.linkedin.com/in/aude-nanquette-53218947/)

[Samuel Paccoud](https://www.linkedin.com/in/spaccoud/)

[Gabriel Plassat](https://www.linkedin.com/in/plassat/)

[Benoît Ribon](https://www.linkedin.com/in/benoit-ribon/)

[Johan Richer](https://www.linkedin.com/in/johanricher/)

[Simon Sarazin](https://www.linkedin.com/in/simonsarazin/)

[Sébastien Shulz](https://www.linkedin.com/in/sebastien-shulz/)

[Xavier von Aarburg](https://www.linkedin.com/in/xaviervonaarburg/)

:::

:::
