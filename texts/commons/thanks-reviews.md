
:::{.columns}

:::{.column width="50%"}

[Luis Roman Arciniega Gil](https://www.linkedin.com/in/luis-roman-arciniega-gil/)

[Vincent Bachelet](https://www.linkedin.com/in/vincent-bachelet-831663aa/)

[Alex Bourreau](https://www.linkedin.com/in/alexbourreau/)

[Jeanne Bretécher](https://www.linkedin.com/in/jeanne-bret%C3%A9cher-267b0110/)

[Brendan Brieg Le Ny](https://www.codelutin.com/lutins)

[Sébastien Broca](https://www.linkedin.com/in/sebastien-broca-96b39a264/)

[Olivier Charbonneau](https://www.linkedin.com/in/olivier-charbonneau-b210491/)

[Benoît De Haas](https://www.linkedin.com/in/beno%C3%AEt-de-haas-78141653/)

[Maïa Dereva](https://blog.p2pfoundation.net/author/maia-dereva)

[Ludovic Dubost](https://www.linkedin.com/in/ldubost)

[Margot Godefroi](https://www.linkedin.com/in/margot-godefroi-40689b154/)

[Marguerite Grandjean](https://www.linkedin.com/in/margueritegrandjean/)

[Bastien Guerry](https://bzg.fr/)
:::

:::{.column width="50%"}
[Serkan Holat](https://www.linkedin.com/in/serkanholat/)

[Alain Imbaud](https://www.linkedin.com/in/alain-imbaud-7632b22a/)

[Nicolas Jullien](https://www.linkedin.com/in/nicolasjullien/)

[Laure Kassem](https://www.linkedin.com/in/laure-kassem/)

[Arnaud Levy](https://www.linkedin.com/in/arnaudlevy/)

[Tibor Katelbach](https://www.linkedin.com/in/tiborkatelbach/)

[Nicolas Loubet](https://www.linkedin.com/in/nicolasloubet/)

[Thomas Menant](https://www.linkedin.com/in/thomas-menant-95171937/)

[Alex Morel](https://twitter.com/alex_morel_)

[Bruce Perens](https://perens.com/)

[Benjamin Poussin](https://www.linkedin.com/in/benjamin-poussin-15b9a/)

[Pierre-Louis Rolle](https://www.linkedin.com/in/pierrelouisrolle/)

[Caroline Span](https://www.linkedin.com/in/caroline-span)

:::

:::
