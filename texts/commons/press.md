
#### 2024

:::{.text-nano}

- 2024-11-19 / Github : ["Announcing GitHub Secure Open Source Fund: Help secure the open source ecosystem for everyone"](https://github.blog/news-insights/company-news/announcing-github-secure-open-source-fund/)
- 2024-11-14 / IEE Spectrum : ["Open-Source Software Is in Crisis"](https://spectrum.ieee.org/open-source-crisis)
- 2024-11-10 / TechCrunch: ["Open source projects draw equity-free funding from corporates, startups, and even VCs"](https://techcrunch.com/2024/11/10/open-source-projects-draw-equity-free-funding-from-corporates-startups-and-even-vcs/)
- 2024-10-15 / FLOSS fund: ["Announcing FLOSS Fund: $1M per year for free and open source projects"](https://floss.fund/blog/announcing-floss-fund/)
- 2024-09-30 / People Vs BigTech : ["Beyond Big Tech: A manifesto for a new digital economy"](https://peoplevsbig.tech/beyond-big-tech-a-manifesto-for-a-new-digital-economy/)
- 2024-09-19 / The Register : ["Kelsey Hightower: If governments rely on FOSS, they should fund it"](https://www.theregister.com/2024/09/19/kelsey_hightower_civo)
- 2024-05-24 / Public Interest Digital Innovation : ["Beyond big tech regulation : funding & scaling public good"](https://player.clevercast.com/?account_id=AkN3yR&item_id=2O3mE6)
- 2024-05-04 / Sequoia : ["New Fellowship: How Sequoia is Supporting Open Source"](https://www.sequoiacap.com/article/sequoia-open-source-fellowship/)
- 2024-04-17 / UK Day One : ["A UK Open-Source Fund to Support Software Innovation and Maintenance"](https://ukdayone.org/briefings/a-uk-open-source-fund)
- 2024-04-04 / Matrix.org : ["Open Source Infrastructure must be a publicly funded service."](https://matrix.org/blog/2024/04/open-source-publicly-funded-service/)
- 2024-04-03 / ITPro : [""We got lucky": What the XZ Utils backdoor says about the strength and insecurities of open source"](https://www.itpro.com/software/open-source/we-got-lucky-what-the-xz-utils-backdoor-says-about-the-strength-and-insecurities-of-open-source)
- 2024-02-16 / Jacobian.org : ["Paying people to work on open source is good actually"](https://jacobian.org/2024/feb/16/paying-maintainers-is-good/)
- 2024-01-19 / openpath.chadwhitacre.com : ["The Open Source Sustainability Crisis"](https://openpath.chadwhitacre.com/2024/the-open-source-sustainability-crisis/)

:::

---

#### 2023

:::{.text-nano}

- 2023-12-28 / Mathis Lucas : ["Qu'est-ce qui vient après l'open source ?"](https://programmation.developpez.com/actu/352421/Qu-est-ce-qui-vient-apres-l-open-source-Un-pionnier-du-mouvement-de-l-open-source-affirme-qu-il-faut-changer-de-paradigme-et-trouver-un-moyen-equitable-de-remunerer-les-developpeurs/)
- 2023-12-27 / linuxfr.org : ["La mission logiciels libres (DINUM) propose 4 prix BlueHats de 10000€ pour soutenir des mainteneurs"](https://linuxfr.org/news/la-mission-logiciels-libres-dinum-propose-4-prix-bluehats-de-10000-pour-soutenir-des-mainteneurs)
- 2023-11-09 / lemmy.ndlug.org : ["GNOME Recognized as Public Interest Infrastructure"](https://lemmy.ndlug.org/post/367541)
- 2023-10-24 / blog.sentry.io : ["We Just Gave $500,000 to Open Source Maintainers"](https://blog.sentry.io/we-just-gave-500-000-dollars-to-open-source-maintainers/)
- 2023-09-17 / nikclayton.writeas.com : ["Stepping back from the Tusky project"](https://nikclayton.writeas.com/update-5-on-stepping-back) serie
- 2023-07-27 / Serkan Holat : ["Open source public fund experiment - One and a half years update"](https://dev.to/coni2k/open-source-public-fund-experiment-one-and-a-half-years-update-367d)
- 2023-02-14 / Denis Pushkarev (core-js) : ["So, what's next ?"](https://github.com/zloirock/core-js/blob/master/docs/2023-02-14-so-whats-next.md)

:::

#### 2022

:::{.text-nano}

- 2022-12-14 / Startin' Blox : ["Co-funding in open source #1"](https://startinblox.com/en/2022/12/14/co-funding-in-open-source-1/)
- 2022-06-21 / Open Futuress : ["Digital commons are a pillar of european digital sovereignty"](https://openfuture.eu/blog/digital-commons-are-a-pillar-of-european-digital-sovereignty/)
- 2022-06 / diplomatie.gouv.fr (blog) : ["Les communs au service d’un modèle européen de souveraineté numérique non hégémonique"](https://www.diplomatie.gouv.fr/fr/politique-etrangere-de-la-france/diplomatie-numerique/blog-de-l-equipe/article/les-communs-au-service-d-un-modele-europeen-de-souverainete-numerique-non)
- 2022-05-16 / Tech Crunch : ["Tech giants pledge $30M to boost open source software security"](https://techcrunch.com/2022/05/16/white-house-open-source-security/)
- 2019-07-17 / unrealengine.com : ["Epic Games supports Blender with $1.2 million Epic MegaGrant"](https://www.unrealengine.com/en-US/blog/epic-games-supports-blender-with-1-2-million-epic-megagrant)
- 2018-06-24 / scinfolex.com : ["Les Communs numériques sont-il condamnés à devenir des « Communs du capital » ?"](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/)
- 2016-04-05 / Burak Arikan : ["Analyzing the NPM dependency network"](https://medium.com/graph-commons/analyzing-the-npm-dependency-network-e2cf318c1d0d)

:::
