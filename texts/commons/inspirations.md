
:::{.columns .text-nano}
:::{.column width="60%"}

- [AFNIC](https://www.afnic.fr/en/)
- [Compte commun des Communs](https://wiki.resilience-territoire.ademe.fr/wiki/LE_COMPTE_COMMUN_DES_COMMUNS)
- [Contributive.org](https://contributive.org/fr/)
- [Copie Publique](https://copiepublique.fr/) by Code Lutin
- [Drips](https://www.drips.network/)
- [Document Foundation](https://www.documentfoundation.org/) by and for Libre Office
- [Fiducies d'intérêt social](https://tiess.ca/nouveaux-outils-fus/) by [TIESS](https://tiess.ca/)
- [Fondation Blue Hat](https://bzg.fr/le-logiciel-libre-a-besoin-d-une-vraie-strategie-de-mutualisation-au-sein-de-letat/)
- [Fondation Wikimedia](https://wikimediafoundation.org/fr/) by and for Wikimedia
- [Fonds de Dotation du Libre](https://www.fdl-lef.org/)
- [FOSS funders](https://fossfunders.com/)
- [Fund for Defense of Net Neutrality](https://fdn2.org/en/presentation.html)
- [Github sponsors](https://github.com/sponsors)
- [KDE e.V](https://ev.kde.org/)
- [Libera Pay](https://liberapay.com/)
- [NLnet](https://nlnet.nl/) - [NGI e-Commons Fund](https://nlnet.nl/news/2023/20230719-eCommonsFund.html)
- [Oceco](https://movilab.org/wiki/Oceco) by Communecter
:::
:::{.column width="40%"}
- [Open Collective](https://opencollective.com/opensource)
- [Open Patent](https://www.boldandopen.com/open-patent)
- [Open Source Collective](https://oscollective.org/)
- [Open Source Initiative](https://members.opensource.org/donate/)
- [Open Source Pledge](https://osspledge.com/)
- [Open Source Security Foundation](https://openssf.org/)
- [Open Technology Fund](https://www.opentech.fund/)
- [OWASP Foundation](https://owasp.org/donate/)
- [Post-Open licence](https://perens.com/?s=post-open) by [Bruce Perens](https://perens.com)
- [Proton Lifetime Fundraiser](https://proton.me/blog/lifetime-fundraiser-survey-2023)
- [Prototype Fund](https://prototypefund.de/en/) (Allemagne)
- [SARD - Société de Répartition des Dons](https://framablog.org/2009/09/05/sard-hadopi/)
- [Software Freedom Conservancy](https://sfconservancy.org/)
- [Sovereign Tech Fund](https://sovereigntechfund.de/en/) (Allemagne)
- [Stakes.social](https://stakes.social/)
- [Thanks.dev](https://thanks.dev/home)
- [TOSIT - The Open Source I Trust](https://tosit.fr/)

:::
:::
