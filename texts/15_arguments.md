

## Grille argumentaire <br>pour les contributeurs financiers

<table class="grid">
  <thead>
    <tr>
      <th></th>
      <th class="text-center"><b>PUBLIC</b></th>
      <th class="text-center"><b>PRIVE</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>**CONTRIBUTION**</td>
      <td>
        2% du montant <br>
        de chantiers numériques
      </td>
      <td>
        Selon le type et la taille de l'entreprise :<br>
        - x % des revenus annuels récurrents<br>
        - x % des excédents de gestion / bénéfices annuels<br>
        - x € annuellement par développeur<br>
        - somme fixe par an
      </td>
    </tr>
    <tr>
      <td>**OPTIONS**</td>
      <td>
Convention ponctuelle<br>
Convention pluri-annuelle <br>
Conventions par projet<br> 
(ou) avec une administration
      </td>
      <td>
Convention ponctuelle ou pluri-annuelle <br>
Dons<br>
Promesse de dons
      </td>
    </tr>
    <tr>
      <td>**CONTRACTUALISATION**</td>
      <td class="text-center" colspan="2">
45% du don : fléchage par la structure vers un ou plusieurs communs<br>
45% du don : fléchage par OGC vers un ou plusieurs communs<br>
10% du don : frais de gestion de l'OGC
      </td>
    </tr>
    <tr>
      <td>**CONTREPARTIES**</td>
      <td class="text-center" colspan="2">
Assurance de maintenance de communs critiques<br>
Labellisation <br>
Encapacitation des équipes<br>
Choix par structure de flécher vers des communs<br>
Choix par structure de préciser certains mécanismes de rétribution (clés de répartition)<br>
Défiscalisation (si structure privée)
      </td>
    </tr>
    <tr>
      <td>**GESTION**</td>
      <td class="text-center" colspan="2">
Transparence de l'OGC et participation à sa gouvernance<br>
Rapports automatiques sur l'utilisation des dons<br>
Gestion en ligne des contrats de contribution<br>
Automatisation des démarches administratives<br>
Certificat pour défiscalisation (pour structures privées)
      </td>
    </tr>
  </tbody>
</table>
