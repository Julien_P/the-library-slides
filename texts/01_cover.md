---
title: The Librery
subtitle: |
  <strong>2% pour les communs &nbsp; <br>&nbsp;et leurs auteur.e.s</strong>
  <br>
  <img src="images/qr-code-slides.png" alt="QR code" height="175px" style="margin: 5% 3%"/>
author: '
  <code>
    Version 1.7
  </code><br><br>
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _développeur FLOSS fullstack_
  </span><br>
  <span class="text-micro text-author-details">
    _co-fondateur de la [coopérative multi](https://multi.coop)_
  </span>
'
date: '
  Illustration : [XKCD n°2347](https://xkcd.com/2347/)
  <br>
  <br><b>Navigation avec les flèches</b> <i class="ri-drag-move-2-fill"></i> / Plan en pressant <span class="text-nano">`echap`</span>
  <br>
  <br>
  <span style="font-size: .7rem;" xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/Julien_P/the-librery-slides">The Librery - slides</a> par <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/Julien_P">J. Paris</a> est sous licence <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:.7rem!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:.7rem!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:.7rem!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></span> 
'
title-slide-attributes:
  data-background-image: "images/XKCD.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 45%"
  data-background-position: "right 20% top 40%"
---
