
# Résumé des propositions phares

:::::: {.columns}
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-money-euro-circle-fill"></i><br>
Le "**2% communs**" <br>sur les chantiers numériques publics
:::
:::
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-group-fill"></i><br>
Un fonds **géré de façon indépendante** <br>par les acteurs des communs
:::
:::
::::::

:::::: {.columns}
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-download-2-fill"></i><br>
Une structure pouvant recevoir des<br> **contributions financières publiques** <br> **ou privées**
:::
:::
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-scales-3-fill"></i><br>
Un organisme de redistribution <br>**transparent, paritaire** <br>et **à but non lucratif**
:::
:::
::::::

## Economie générale

:::{.opaque .img-no-caption}
![&nbsp;](images/diagrams/diagram-08.png)
:::

## Distribution des fonds collectés

:::{.opaque .img-no-caption}
![&nbsp;](images/librery-fund-sankey-en.png)
:::

## Internationalisation

:::{.opaque .img-no-caption}
![&nbsp;](images/diagrams/decentralized/diagram-99.png)
:::

## Financer les communs numériques comme un commun

::: {.opaque .img-xl .img-no-caption .img-no-margin}
![&nbsp;](images/diagrams/Handler.ashx_.jpeg)
:::

::: {.text-micro .text-center}
<i class="ri-book-2-line"></i>
Voir le livre <b>"Governing the commons"</b> par [Elinor Orstom](https://en.wikipedia.org/wiki/Elinor_Ostrom)
:::
