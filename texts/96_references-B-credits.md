
## Crédits

Présentation réalisée avec :

- [Pandoc](https://pandoc.org/)
- [Reveal.js](https://revealjs.com/)
- [Remix Icons](https://remixicon.com/)
- [Slider-template](https://gitlab.com/multi-coop/slider-template)
