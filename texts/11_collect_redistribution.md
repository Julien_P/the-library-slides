
# Collecte et redistribution

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
Distinguer<br>**oeuvre,** **auteur,**<br>**diffuseur,** et **éditeur**
:::
:::{.column .card width="33%"}
Une approche de type <br>"**marché biface**"
:::
:::{.column .card width="33%"}
La notion de **contrats de rétribution** pour intégrer les particularités des parties prenantes
:::
::::::

## Notions principales

Les parallèles avec les autres industries culturelles sont les suivants :

- **Oeuvre** (individuelle ou de collaboration) : 1 logiciel libre (=== 1 chanson)
- **Auteur** : 1 développeur / designer (=== 1 artiste)
- **Diffuseur** : 1 entité/administration déployant un logiciel libre( === 1 radio)
- **Editeur** : 1 entreprise privée développant un produit libre (=== 1 maison de production)

## Mécanisme général

- **Auteurs** : il faut que les auteurs s'inscrivent, et qu'il déclarent à quelles oeuvres libres ils contribuent
- **Oeuvres** : il faut un catalogue d'oeuvres (lociels libres, BDD libres) sur lesquelles on peut avoir un suivi des contributions
- **Diffuseurs** : il faut qu'ils déclarent quelles oeuvres ils diffusent, et qu'ils contribuent financièrement à une caisse commune (via des conventions). Cette contribution peut être imaginée de différentes manières, cumulativement :
  - cotisations
  - et/ou contribution forfaitaire
  - et/ou contribution forfaitaire par commun
  - et/ou indexation sur un budget alloué à du développement de communs

## Répartition des fonds collectés

:::{.opaque .img-no-caption}
![&nbsp;](images/librery-fund-sankey-fr.png)
:::

## A/ Processus côté diffuseurs de communs

:::::::::::::: {.columns}
::: {.column width="50%"}
![&nbsp;](images/diffusors.png)
:::
::: {.column width="50%"}
::: {.text-micro}

- Est considéré comme **diffuseur** une entité morale ou physique prête à verser volontairement une somme afin de rémunerer les auteurs et co-auteurs d'un ou plusieurs communs

- Un diffuseur signe un **contrat de rétribution aux communs** avec l'OGC, l'OGC s'engage à redistribuer les montants volontairement versés par le diffuseur.

- Le contrat porte sur un **montant** de contribution volontaire sur un ou plusieurs communs, voire sans commun particulier, le tout sur une période donnée

- Ce contrat peut porter des clauses et **règles de redistribution**

- Le contrat est alors ventilé par **montants alloués par année et par commun**

- Tous les contrats et leurs montants associés sont **agregés par commun**, cela constitue un fonds par commun (et par année)
:::
:::
::::::::::::::

## A/ La notion de contrats de rétribution

En amont - c'est-à-dire vis-à-vis des contributeurs financiers (processus A/) - il serait possible d'associer à chaque contribution financière des **contrats de rétribution** précisant les différents <b>emplois des sommes</b>, les <b>conditions et règles</b> de redistribution, ainsi que les <b>parts allouées</b> à des communs spécifiques ou au contraire sans fléchage précis.

:::{.text-micro}
On pourrait imaginer qu'à **chaque contribution financière** puisse être associé un contrat précisant :

- **une ou plusieurs parts fléchées** sur un/des communs choisis par le contributeur / diffuseur, en raison de son intérêt direct à en déléguer la maintenance pour le bon fonctionnement de ses propres services.
- **une part plancher sans fléchage**, en vue d'une redistribution choisie par l'OGC sur d'autres communs "_low level_" ou à faible visibilité (part qui pourrait par exemple être reversée à des entités telles que Thanks.dev ou équivalent).
:::

## B/ Processus côté auteurs de communs

:::::::::::::: {.columns}
::: {.column width="50%"}
![&nbsp;](images/authors.png)
:::
::: {.column width="50%"}
::: {.text-micro}
- **Un.e auteur.e s'enregistre** à l'OGC en précisant à quelles oeuvres/communs il contribue

- Un **contrat de cession/gestion** de ses droits d'auteur est etabli entre l'OGC et l'auteur

- Ses **contributions** à des communs sont calculées et enregistrées au cours de l'année, commun par commun

- En fin d'année ses contributions permettent de **calculer le montant de la rétribution** qui lui est allouée, commun par commun, en fonction du fonds associé et des règles de redistribution

- L'OGC établit sa **déclaration de droits d'auteurs** auprès de l'URSSAF et transmet à l'auteur la preuve de déclaration

- _Nota_ : le processus côté auteur.e.s est celui qui demande à être le plus **rapide et automatisé** possible afin d'éviter toute lourdeur administrative et coûts exponentiels de gestion.
:::
:::
::::::::::::::

## C/ Cas particulier <br>des éditeurs de communs numériques

::: {.text-micro}

- Un éditeur de communs emploie **un.e ou des salarié.es, considéré.es comme les auteurs initiaux** des communs édités

- L'**éditeur est considéré comme titulaire des droits** d'auteurs de ses employé.e.s

- L'éditeur permet la **gestion des droits en son nom par l'OGC** auprès de l'URSSAF

- L'éditeur **enregistre ses employé.e.s** comme auteurs sur l'OGC

- L'OGC calcule et déclare les rétributions aux auteurs comme en B/, avec la particularité d'**enregistrer ces sommes à l'URSSAF au nom de l'éditeur** aussi mais envoie les documents à l'éditeur

:::

## Modèle de données complet simplifié

Voir le [document](https://gitlab.com/Julien_P/the-librery-slides/-/blob/main/Model.md) au format _markdown_ sur le _repo_.

:::::::::::::: {.columns}

::: {.column}
::: {.text-micro}
Partie 1/2
:::
::: {.img-no-margin}
![&nbsp;](images/model-simplified-01.png)
:::
:::

::: {.column}
::: {.text-micro}
Partie 2/2
:::
::: {.img-no-margin}
![&nbsp;](images/model-simplified-02.png)
:::
:::

::::::::::::::
