
# Prologue

::: {.text-center}
[https://julien_p.gitlab.io/the-librery-slides](https://julien_p.gitlab.io/the-librery-slides)
:::

Les <b>propos</b> figurant dans cette présentation <b>n'engagent que leur auteur</b>.

Ces réflexions et ces propositions <b>ne sont en rien figées</b> et représentent davantage <b>une étape dans une démarche de réflexion collective.</b>

Cet exercice de communication est à considérer avant tout comme une manière d'<b>ouvrir ces idées à la critique</b>, dans l'optique d'<b>aboutir à terme à un socle de propositions concrètes</b> pouvant être portées par un consortium le plus large possible d'acteurs des communs.

Tous les éléments présentés ici sont ouverts à la discussion et à la critique, notamment sur un [pad de notes](https://hackmd.io/@Jpy/the-librery) <b>ouvert en écriture et en commentaires</b>.

## Présentations liées
