
# Quelques notions centrales

<br>

:::::: {.columns}
:::{.column .text-center .card width="25%"}
<i class="ri-shield-check-fill"></i><br>
Les communs **garants des infrastructures** et des **services publics** numériques
:::
:::{.column .text-center .card width="25%"}
<i class="ri-building-3-fill"></i><br>
Les communs comme **filière industrielle**
:::
::::::

:::::: {.columns}
:::{.column .text-center .card width="25%"}
<i class="ri-quill-pen-fill"></i><br>
Le.la _commoner_ comme **auteur.e**
:::
:::{.column .text-center .card width="25%"}
<i class="ri-money-euro-circle-fill"></i><br>
La **mutualisation** des fonds et de la gestion
:::
::::::
