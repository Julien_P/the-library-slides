
# Réflexions sur la gouvernance

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
Un représentation de **toutes les parties prenantes** des communs
:::
:::{.column .card width="25%"}
Une majorité structurelle pour les **auteur.e.s**
:::
:::{.column .card width="25%"}
Des **ressources humaines et techniques** pour la mise en oeuvre des missions
:::
:::{.column .card width="25%"}
Des exigences de **transparence** et de **non-lucrativité** fortes
:::
::::::

## Financer les communs numériques comme un commun

::: {.opaque .img-xl .img-no-caption .img-no-margin}
![&nbsp;](images/diagrams/Handler.ashx_.jpeg)
:::

::: {.text-micro .text-center}
<i class="ri-book-2-line"></i>
Voir le livre <b>"Governing the commons"</b> par [Elinor Orstom](https://en.wikipedia.org/wiki/Elinor_Ostrom)
:::

## Diagramme général

:::::: {.columns}

::: {.column width="60%"}
![&nbsp;](images/global.png)
:::

::: {.column width="40%"}
::: {.text-micro}
Diagramme général de principe de The Librery (réalisé avec [Whimsical](https://whimsical.com/the-librery-organigramme-RimouvNB2jVq7pcu59zP4H))

De haut en bas :

- **Les parties prenantes** :
  - Auteurs
  - Oeuvres (communs)
  - Diffuseurs
  - Editeurs
  - Sympathisants
- **L'équipe de l'OGC**
- **Le fonds mutualisé** et ses différents usages
  - Gestion des droits
  - Aides ciblées
  - Programmes / actions
  - Promotion des communs
- **Les outils** mis à disposition
  - Portail web (+ backoffice)
  - Services tiers

:::
:::

::::::

## Les parties prenantes

![&nbsp;](images/stakeholders.png)

## Les moyens financiers : le fonds

![&nbsp;](images/fund.png)

## Les moyens humains : l'équipe

![&nbsp;](images/team.png)

## Les moyens techniques : les outils

![&nbsp;](images/tools.png)
