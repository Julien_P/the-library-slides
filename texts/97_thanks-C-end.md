
:::

:::

::::::

## Un dernier mot...

<br>

:::{.columns}

:::{.column width="30%" .img-shadow}
![&nbsp;](images/an-01.jpg)
:::

:::{.column width="70%" .text-center}

:::{.text-large}
<br>
**L’utopie, ça réduit à la cuisson,**<br>
**c’est pourquoi**<br>
**il en faut énormément au départ**
:::

<br>

:::{.text-micro}
Citation de Gébé, ["L'an 01"](https://www.lassociation.fr/catalogue/l-an-01-livre-et-dvd/)
:::
:::

:::
