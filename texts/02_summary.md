
# Sommaire

::: {.text-micro}

- [Prologue](#prologue)
- [Résumé des propositions phares](#résumé-des-propositions-phares)
- [Problématique](#problématique)
- [Les communs, condition et moteur de l’innovation](#les-communs-condition-et-moteur-de-linnovation)
- [Quelques notions centrales](#quelques-notions-centrales)
- [Pourquoi 2% de la commande publique ?](#pourquoi-2-de-la-commande-publique)
- [L’intérêt du droit d’auteur pour soutenir l’économie des communs](#lintérêt-du-droit-dauteur-pour-soutenir-léconomie-des-communs)
- [The Librery](#the-librery)
- [Collecte et redistribution](#collecte-et-redistribution)
- [Réflexions sur la gouvernance](#réflexions-sur-la-gouvernance)
- [Internationalisation & décentralisation](#internationalisation-décentralisation)
- [Inspirations](#inspirations)
- [Scénarios](#scénarios)
- [Comment poursuivre cette réflexion ?](#comment-poursuivre-cette-réflexion)
- [Références et documentation](#références-et-documentation)
- [Remerciements](#remerciements)
- [Versions du document](#versions-du-document)
:::
