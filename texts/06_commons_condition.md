
# Les communs, condition et moteur de l'innovation

:::{.r-stack .no-margin}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-00.png)
:::
:::{.fragment .fade-in data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-00-bis.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-01.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-02.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-03.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-04.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05-a.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05-b.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-06.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-06-bis.png)
:::
:::

## Le risque d'épuisement de l'écosystème

:::{.img-shadow .opaque .img-no-caption .img-large}
![&nbsp;](images/maintenance-paradox.png)
:::

<br>

:::{.text-micro .text-center}
Voir la présentation de [Tobie Langel](https://speaking.unlockopen.com/nBXJS5/1-billion-dollars-for-open-source-maintainers) donné lors de [State Of Open Con](https://stateofopencon.com/) 2024.
:::

## Le risque de faille de sécurité

:::{.columns}

:::{.column width="50%"}
:::{.text-micro}
Le risque de sécurité évité de justesse en 2024 sur la libraire XZ Utils ([source](https://www.itpro.com/software/open-source/we-got-lucky-what-the-xz-utils-backdoor-says-about-the-strength-and-insecurities-of-open-source))
:::
:::{.img-shadow .opaque}
![&nbsp;](images/xz-fail.png)
:::
:::

:::{.column width="50%"}
:::{.text-micro}
"Un milliard pour l'open source". Voir la présentation de [Tobie Langel](https://speaking.unlockopen.com/nBXJS5/1-billion-dollars-for-open-source-maintainers) donné lors de [State Of Open Con](https://stateofopencon.com/) 2024.
:::
:::{.img-shadow .opaque}
![&nbsp;](images/one-billion-maintain.png)
:::
:::

:::

:::{.text-micro .text-center}
Les risques de sécurité (voir le [rapport FOSSEPS](https://joinup.ec.europa.eu/collection/fosseps/news/fosseps-critical-open-source-software-study-report) de la Commission Européenne en 2022) de nos infrastructures numériques posent de nouveau la question du <b>manque de moyens alloués à la maintenance</b> de projets open source.
:::

## Le risque systémique

:::{.columns}
:::{.column width="50%"}
:::{.fragment .img-shadow .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/curl.png)
:::
:::
:::{.column width="50%"}
:::{.fragment .img-shadow .fade-in data-fragment-index="1"}
![&nbsp;](images/open-ssl.png)
:::
:::
:::

:::{.text-micro .text-center}
:::fragment
Exemples trouvés sur la page d'accueil de [Github sponsors](https://github.com/sponsors)
:::
:::

## L'économie des services publics <br> et des communs numériques

:::{.fragment .text-center .semi-fade-out .opaque data-fragment-index="1"}
::: {.img-small}
![&nbsp;](images/2-percents.png)
:::
:::

:::{.fragment .text-center data-fragment-index="1"}
<br>
<b>Un moteur qui tourne sur la réserve ?</b>
:::
