
# <span style="display:none">The Librery</span>

:::{.img-librery .img-no-margin .img-no-caption}
![&nbsp;](images/Librery-logo-baudot-black.png)
:::

:::{.text-center}
<b>Penser un mécanisme de financement pérenne 
  <br> et un organisme de gestion collective (OGC)
  <br> par et pour les auteur.e.s de communs
</b>
:::

::::::{.columns .text-center .text-micro}
:::{.column .card .card-secondary width="25%"}
<i class="ri-download-2-fill"></i><br>
La **collecte,**<br>**la mutualisation,**<br>et la **redistribution** <br>des fonds
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-scales-3-fill"></i><br>
Une exigence forte de **transparence, de non-lucrativité et de parité**
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-share-fill"></i><br>
Des formes diversifiées de **soutien aux communs et aux _commoners_**
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-shield-check-line"></i><br>
La possibilité <br>d'un **label**
:::
::::::

## Economie générale

:::{.r-stack}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-03-bis.png)
:::
:::{.fragment .fade-in data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-07.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-08.png)
:::
:::

## Missions de l'OGC

- **collecter** les contributions financières des diffuseurs
- **recenser** des communs et leurs auteurs
- **calculer** ce qui doit être réparti (clés de répartition)
- **redistribuer** en droits / déclarer à l'URSSAF (pour simplifier la vie des auteurs)
- **défendre** la cause et les droits des auteurs de communs numériques
- **promouvoir** le statut d'auteur de commun numériques
- **faire entrer dans la loi** des clauses équivalentes à celles relatives au CNC / SACD / etc... (régime de contribution obligatoire ?)

## Contributions financières <br>en soutien à l'économie des communs

Dans un souci de mutualisation l'organisme de gestion collective doit être en capacité de pouvoir <b>recevoir différents types de flux financiers</b> potentiellement "entrants", argent public ou argent privé :

::: {.text-micro}
:::{.columns}
:::{.column width="50%"}

**Flux financiers potentiels d'origine publique**

- Plaidoyer pour le principe de <b>2% communs dans la commande publique</b> sur des chantiers numériques (inspiré du "[2% pour 2 degrés](https://institut-rousseau.fr/2-pour-2c-resume-executif/)", institut Rousseau ; ou du "[1% artistique](https://www.culture.gouv.fr/Thematiques/Arts-plastiques/Commande-artistique/Le-1-artistique)")
- Contributions financières de type <b>subvention</b> de certaines institutions publiques (IGN, ADEME, DINUM...)
- Intervention <b>au niveau européen</b> de soutien aux communs numériques, à l'image de [NGI](https://www.ngi.eu/), de [NLnet](https://nlnet.nl/), ou du projet [EDIC](https://digital-strategy.ec.europa.eu/en/policies/edic)
:::

:::{.column width="50%"}
**Flux financiers potentiels d'origine privée**

- Contributions financières (type don) de certaines <b>entreprises privées</b> bénéficiant/diffusant des communs numériques
- <b>Prestations de services</b> type audit de code, _sourcing_...
- Plaidoyer pour le principe d'une obligation de contribution minimale obligatoire - ou <b>redevance</b> - de certaines industries dont l'activité repose sur l'exploitation extensive de communs numériques stratégiques
<!-- - Possibilité de proposer à des acteurs privés la **garantie de bon fonctionnement de certaines librairies libres critiques**, en échange d'apports financiers redistribués directement vers les mainteneur.e.s de ces librairies. -->
:::
:::
:::

## Conditions de gouvernance

- Les communs relevant tout à la fois de la sphère publique que de la sphère privée il est souhaitable que **toutes les parties puissent être représentées** au sein de l'OGC, toutefois en assurant une bonne réprésentation des auteurs/individus, étant donné que la mission de l'OGC est bien de les défendre

- En tant que structure défendant une filière (les communs) l'OGC a davantage de **légitimité** en tant que structure de droit privé, sous réserve que les différentes parties prenantes puissent être justement représentées

- Intégrer à la gouvernance autant les **auteurs** que les **éditeurs** ou les **financeurs** et **diffuseurs** de communs numériques

## Conditions structurelles

Sur la base des critiques faites aux OGC culturelles (cf. Benhamou) on peut lister quelques <b>conditions nécessaires (mais pas suffisantes) pour éviter certains travers</b> des OGC historiques identifiés par les observateurs et le champ académique :

- **Frais de gestion** ne dépassant pas 5% à 10%
- **Suivi et automatisation de l'enregistrement** des auteur.e.s et de leur rétribution
- **Plafonnement** des salaires des dirigeant.e.s de l'OGC
- **Représentation** de tous les auteur.e.s (1 personne == 1 voix)
- **Transparence** totale de la gouvernance et du fonctionnement, notamment concernant le choix et la mise en oeuvre des clés de répartitions

## Equilibre de fonctionnement<br>(hypothèses)

::::::{.columns}
:::{.column width="50%"}
**Coûts de fonctionnement**

:::{.text-micro}
En comptant une équipe au complet (5 personnes à temps plein, voir plus loin dans la section "Réflexions sur la gouvernance") avec des salaires de 4&nbsp;000 € nets/mois, des bureaux, et quelques services complémentaires (comptable, abonnements, services en ligne...) :

en première approximation le <b>coût total de fonctionnement et de gestion tournerait autour de 550 k€ / an.</b>
:::
:::

:::{.column width="50%"}
**Collecte**

:::{.text-micro}
Si on maintient des coûts de fonctionnement et de gestion autour de 10%, cela signifie que l'<b>OGC doit être en mesure de collecter 5,5 millions € / an.</b>

En partant d'une base de "2% communs" sur les chantiers numériques, ce montant de collecte correspond à un ensemble de <b>chantiers numériques dont les budgets cumulés représenteraient 275 millions € / an</b> avant contribution aux communs.

:::
:::
::::::

**Redistribution**

:::{.text-micro}
En résumé, en appliquant le principe de "2% communs" sur un ensemble de chantiers numériques à hauteur de 275 M€ / an,
l'OGC serait en capacité de redistribuer : 5,5 M€ (collectés) - 0,55 M€ (frais de gestion)

soit <b>une redistribution de 4,95 M€ / an à des communs numériques.</b>
:::

## Véhicules juridiques possibles

Différents types de structures juridiques peuvent être imaginées pour remplir les missions de The Librery, avec pour point commun d'être à <b>but non lucratif</b> ou du moins à lucrativité limitée.

:::{.text-micro}
- L'**OGP** (Organisme de Gestion Collective)
- La **SGCDA** (Société de Gestion Collective des Droits d'Auteur )
- La **Fondation**
- La **Fiducie** (ou _trust_)
- La **SCIC** (Société Coopérative d'Intérêt Collectif)
- Le **GIP** (Groupement d'Intérêt Public)
- La **Mutuelle**
- Le **Fonds d'investissement**
- Le **Fonds de dotation**
- L'**Association loi de 1901**
:::

## Le label comme incitation

- L'existence d'un organisme légitime auprès de toutes les parties prenantes des communs numériques permettrait d'imaginer la création d'un "**label communs**", que l'organisme pourrait délivrer aux projets comme aux structures.

- La notion de "label communs" peut être un axe intéressant à développer pour **inciter les administrations et les entreprises à contribuer** à hauter de 2% de leurs chantiers numériques.

- La notion de label pose les questions des **critères objectifs** permettant de qualifier un commun numérique comme tel.

- L'autre pendant de la notion de label serait d'arriver à **identifier et suivre des indices de valorisation économique** (financiers, temps alloué, économies...) permettant de mieux situer les apports des communs numériques au sein de la filière industrielle.

## Idées de logo

<br>

:::{.columns .text-micro}
:::{.column width="50%"}
### Inspiré du [XKCD n°2347](https://xkcd.com/2347/)

![&nbsp;](images/Librery-logo-long-white.png)

![&nbsp;](images/Librery-logo-long-dots-01.png)
:::
:::{.column width="50%"}
### Inspiré du [code Baudot](https://fr.wikipedia.org/wiki/Code_Baudot)

![&nbsp;](images/code-baudot.png)

![&nbsp;](images/Librery-logo-baudot-black.png)
:::
:::
