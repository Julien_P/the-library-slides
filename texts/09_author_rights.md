
# L'intérêt du droit d'auteur pour <br>soutenir l'économie des communs

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
Les communs numériques sont des **oeuvres collectives ou de collaboration**
:::
:::{.column .card width="25%"}
Les _commoners_ sont des **auteur.e.s**
:::
:::{.column .card width="25%"}
Contribuer peut être reconnu comme un **travail**
:::
:::{.column .card width="25%"}
La gestion des droits d'auteurs libres demande une structure **mutualisée**
:::
::::::

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
L'**économie des communs numériques** est singulière et très diversifiée
:::
:::{.column .card width="25%"}
Utiliser le **droit existant et communautaire** comme levier
:::
:::{.column .card width="25%"}
Apprendre des **expériences économiques et juridiques passées**
:::
:::{.column .card .card-transparent width="25%"}
&nbsp;
:::
::::::

## Les communs numériques sont <br>des oeuvres collectives ou de collaboration

- Le numérique est une **industrie culturelle**, au même titre que l'édition, le cinéma ou la musique

- Les communs numériques (logiciels, bases de données) peuvent être considérés comme des **oeuvres** de l'esprit

## Les _commoners_ sont des auteur.e.s

- Les **contributeurs** et/ou initiateurs de communs numériques peuvent être considérés comme **des auteurs ou co-auteurs** (cf URSSAF Limousin)

- **La notion d'auteur est encadrée juridiquement** en France : un auteur de logiciel libre conserve des droits patrimoniaux vis-à-vis de son oeuvre

- Une oeuvre libre / commun numérique est potentiellement une **oeuvre collective ou de collaboration** (d'où la notion de co-auteur)

## Contribuer est un travail

- Contribuer à un logiciel ou à une base de données est une **activité à haute valeur ajoutée**

- On peut **cumuler le fait d'être auteur et avoir une activité salariée**

- Une oeuvre libre peut être développée par un salarié **au sein d'une entreprise**
  - L'auteur reste le salarié (une personne morale ne peut pas être auteur)
  - L'entreprise est titulaire des droits, sous condition que cela soit précisé dans un contrat de cession de droits d'auteurs
  - Cas particulier des fonctionnaires développant des communs dans le cadre de leurs fonctions.

## La gestion des droits d'auteurs libres <br>demande une structure mutualisée

- Tout comme cela a été le cas dans le domaine de la musique ou de l'édition, il est en pratique **impossible pour un auteur particulier de faire lui-même** :

  - le **suivi** de l'ensemble des modes d'exploitation de ses oeuvres

  - la **défense** de ses droits auprès des diffuseurs

- Les nouvelles oeuvres libres sont **initiées par des personnes physiques**, parfois en tant que salarié dans des entreprises mais pas toujours.

## L'économie des communs numériques <br>est singulière

::: {.text-center}
`"Un logiciel est libre lorsqu'il est payé"`
:::

- Une oeuvre libre n'est - en première approximation - pas destinée à directement recevoir de revenus d'exploitation, l'une des spécificités des communs numériques est leur **gratuité dans leur exploitation et leur diffusion**.

- Les flux financiers liés à la production d'un commun numérique ne peuvent se trouver qu'**en amont de leur publication**

## Utiliser le droit comme levier

Une <b>licence libre</b> ne signifie pas que l'auteur n'a plus de droits, or :

- quelle entité existe-t-il pour **défendre ces droits** ?

- quand bien même y aurait-il une entité pour défendre ces droits il n'y a pas clairement de **flux financiers** fléchés en vue de leur soutien, mais au contraire une jungle de modèles économiques différents

## Apprendre des expériences passées

- Il est nécessaire de **défendre et promouvoir les personnes / auteurs**, leur proposer un modèle de revenus, pour inciter les personnes à contribuer à des communs numériques et surtout d'en tirer des revenus (en partie ou totalement)

- Il existe déjà et il y a eu des **expérimentations** / propositions d'évolution de la loi (licence globale, revenu universel, ...), mais ces propositions n'ont pas toujours connu de succès et/ou sont encore peu opérationnelles

- Plutôt que chercher à ajouter à la loi et à la réglementation il est possible de **s'appuyer sur le droit existant**, de s'inspirer d'exemples historiques divers tels que la [redevance copie privée](https://www.culture.gouv.fr/Espace-documentation/Rapports/Rapport-du-Gouvernement-au-Parlement-sur-la-remuneration-pour-copie-privee-octobre-2022), la création d'[organismes de gestion collective](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006069414/LEGISCTA000006146355/2222-02-22) comme la SACEM ou la SACD, ou encore de la notion d' ["exception culturelle" à la française](https://fr.wikipedia.org/wiki/Exception_culturelle)
