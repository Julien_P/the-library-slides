
# Inspirations

:::{.text-micro}
- Le mécanisme existant du "**1% artistique**" dans les [constructions publiques](https://www.culture.gouv.fr/Thematiques/Arts-plastiques/Commande-artistique/Le-1-artistique)

- La proposition de "**2% pour 2 degrés**" de l'[Institut Rousseau](https://institut-rousseau.fr/2-pour-2c-resume-executif/)

- Différentes initiatives venant du monde du logiciel libre ou d'institutions :
:::
