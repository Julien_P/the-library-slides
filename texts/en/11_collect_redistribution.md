
# Collect and redistribution

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
Distinguish<br>**work,** **author,**<br>**broadcaster,** and **editor**
:::
:::{.column .card width="33%"}
A "**two-sided market**" type approach
:::
:::{.column .card width="33%"}
The notion of **apportionment contracts** to integrate the particularities of the stakeholders
:::
::::::

## Main concepts

The parallels with other cultural industries are as follows:

- **Work** (individual or collaborative): 1 free software (=== 1 song)
- **Author** : 1 developer / designer (=== 1 artist)
- **Broadcaster** : 1 entity/administration deploying free software (=== 1 radio station)
- **Editor** : 1 private company developing a free product (=== 1 production house)

## General mecanism

- **Authors** : authors must register, and declare which free works they contribute to
- **Works** : you need a catalog of works (free software, free databases) on which you can track contributions
- **Broadcasters** : they must declare which works they broadcast, and they must contribute financially to a common fund (via agreements). This contribution can be imagined in different ways, cumulatively:
  - contributions
  - and/or flat-rate contribution
  - and/or flat-rate contribution per community
  - and/or indexation on a budget allocated to the development of common areas

## Distribution of collected funds

:::{.opaque .img-no-caption}
![&nbsp;](images/librery-fund-sankey-en.png)
:::

## A/ Process from the commons broadcasters side

:::::::::::::: {.columns}
::: {.column width="50%"}
![&nbsp;](images/diffusors.png)
:::
::: {.column width="50%"}
::: {.text-micro}

- **A broadcaster** is considered to be a legal or physical entity ready to voluntarily pay a sum in order to remunerate the authors and co-authors of one or more publications.

- A broadcaster signs a **commons apportionment contract** with the OGC, the OGC undertakes to redistribute the amounts voluntarily paid by the broadcaster.

- The contract indicates an **amount** of voluntary contribution on one or more common areas, or even without a particular common area, all over a given period.

- This contract may include **redistribution** clauses and rules

- The contract is then broken down by **amounts allocated per year and per common**.

- All contracts and their associated amounts are **aggregated by common**, this constitutes one fund per common (and per year)

:::
:::
::::::::::::::

## A/ The concept of apportionment contracts

Upstream - that is to say with respect to the financial contributors (process A/) - it would be possible to associate with each financial contribution **apportionment contracts** specifying the different <b>uses of the sums</b>, the <b>conditions and rules</b> of redistribution, as well as the <b>shares allocated</b> to specific common areas or on the contrary without precise marking.

:::{.text-micro}
We could imagine that **each financial contribution** could be associated with a contract specifying:

- **one or more shares marked** on one or more common areas chosen by the contributor/broadcaster, due to their direct interest in delegating maintenance for the proper functioning of their own services.
- **a floor share without signage**, with a view to redistribution chosen by the OGC to other “ low level ” or low visibility commons (share which could for example be donated to entities such as Thanks.dev or equivalent).

:::

## B/ Process from the authors’ side

:::::::::::::: {.columns}
::: {.column width="50%"}
![&nbsp;](images/authors.png)
:::
::: {.column width="50%"}
::: {.text-micro}
- An **author registers** with the OGC, specifying which works/communities he contributes to.

- A **contract for the transfer/management of copyright** is established between the OGC and the author.

- Its **contributions** to commons are calculated and recorded during the year, common by common

- At the end of the year, his contributions make it possible to **calculate the amount of remuneration** allocated to him, common by common, according to the associated fund and the redistribution rules.

- The OGC establishes its **copyright declaration** with the authorities and sends the proof of declaration to the author

- _Note_ : the process on the author's side is the one that needs to be as **fast and automated** as possible in order to avoid any administrative burden and exponential management costs.
:::
:::
::::::::::::::

## C/ Special case of digital commons editors

::: {.text-micro}
- A commons editor employs **one or more employees, considered to be the initial authors** of the published commons

- The editor is considered the **manager of the copyright of its employees**.

- The editor allows the **management of rights in his name by the OGC** with the URSSAF

- The editor **registers its employees as authors** on the OGC

- The OGC calculates and declares the remunerations to the authors as in B/, with the particularity of **declaring these sums to the authorities in the name of the editor** also but sends the documents to the editor

:::

## Simplified full data model

See the markdown [document](https://gitlab.com/Julien_P/the-librery-slides/-/blob/main/Model.md) on the repository.

:::::::::::::: {.columns}

::: {.column}
::: {.text-micro}
Part 1/2
:::
::: {.img-no-margin}
![&nbsp;](images/model-simplified-01.png)
:::
:::

::: {.column}
::: {.text-micro}
Part 2/2
:::
::: {.img-no-margin}
![&nbsp;](images/model-simplified-02.png)
:::
:::

::::::::::::::
