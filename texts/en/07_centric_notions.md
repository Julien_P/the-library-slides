
# Some central notions

<br>

:::::: {.columns}
:::{.column .text-center .card width="25%"}
<i class="ri-shield-check-fill"></i><br>
Common **guarantors of infrastructure** and digital **public services**
:::
:::{.column .text-center .card width="25%"}
<i class="ri-building-3-fill"></i><br>
The commons as an **industrial sector**
:::
::::::

:::::: {.columns}
:::{.column .text-center .card width="25%"}
<i class="ri-quill-pen-fill"></i><br>
The commoner as an **author**
:::
:::{.column .text-center .card width="25%"}
<i class="ri-money-euro-circle-fill"></i><br>
**Pooling** of funds and management
:::
::::::
