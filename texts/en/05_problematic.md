
# Problematic

::: {.img-medium}
![&nbsp;](images/hamlet-meme.jpg)
:::

:::fragment
::: {.text-center}
**Economically, industrially and democratically &nbsp; <br> &nbsp; a question of life and death...**
:::
:::

## No internet without digital commons

:::{.columns .align-center}

::: {.column width="65%"}
![&nbsp;](images/logos-open-source.png)
:::

::: {.column width="35%" .text-micro}
Could <b>public services</b> today do without all or part of the few free tools shown here (absolutely non-exhaustive list)?

These same tools are also themselves based on more or less low layers of a [multitude of other open source tools]((https://medium.com/graph-commons/analyzing-the-npm-dependency-network-e2cf318c1d0d)) (languages, libraries, databases, standards, etc.).

Some companies whose services rely on these open source building blocks are fully aware of this <b>dependence on an extremely large and diverse eco-system</b>.

Among these companies, some [contribute and redistribute the value they generate](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/), others quietly try to r[e-internalize these building blocks and the people](https://scinfolex.com/2018/06/24/les-communs-numeriques-sont-il-condamnes-a-devenir-des-communs-du-capital/) who master them best .
:::
:::

## The main issues

:::incremental

- **Sustain** and **improve** existing commons
- Helping the **emergence of new digital commons**
- Develop the contribution to the commons as **an economic activity** of tis own
- Strengthen **advocacy** for a society of the commons at a time when the risks of resource exhaustion are increasing
- Anticipate and prevent the risks of loss of digital **sovereignty**
- Specify the place of **public authorities policies** in supporting the commons

:::

## Main considerations

:::incremental

- The sustainability of digital commons poses **infrastructural and strategic, even democratic, challenges**
- Many digital commons are **invisible** because they are in more or less low layers of digital infrastructures
- The economic models of digital commons assume - often - a logic of **valorization of services around the commons** , therefore assuming to be developed ex-post after significant investments in R&D, in time…
- The economic balance of the commons can be based on very **diversified sources of income** : donations, subsidies, volunteering, patronage, etc.
- **Direct support from public actors** can today be available in different forms: calls for projects, subsidies, public procurement, etc.
- The general economy of digital commons and commoners is **unique in the economic landscape** and is also complex to evaluate
:::

<!-- ::::::::: {data-visibility="hidden"} -->
## Some questions

:::incremental

- How can we develop the **digital commons economy** as an **essential** or even **desirable** industrial sector ?
- What if we considered “commoners” (contributors to digital commons) as **authors** , in the sense of copyright?
- What are the **examples** in recent history that echo these issues?
- What **kind of structure** should we invent to respond to these challenges?
- If digital companies have understood the strategic and economic interest of the commons on which they have developed, what is the **level of awareness of public authorities** ?

:::

<!-- ::::::::: -->

## Attention points

:::::: {.columns}

::: {.column}
:::fragment
### Be wary of _business as usual..._
![&nbsp;](images/opensource-money-meme.jpg)

::: {.text-center}
**Consolidate the digital commons economy  &nbsp; <br> &nbsp; by directly remunerating the authors**
:::
:::
:::

::: {.column}
:::fragment
### _Or just shot yourself in the foot_
![&nbsp;](images/opensource-remove-meme.jpg)

::: {.text-center}
**Without digital commons &nbsp; <br> &nbsp; our digital infrastructures will collapse**
:::
:::
:::

::::::
