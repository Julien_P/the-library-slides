
# Inspirations

:::{.text-micro}
- The existing mechanism of  "**artistic 1%**" in  [public construction (in France)](https://www.culture.gouv.fr/Thematiques/Arts-plastiques/Commande-artistique/Le-1-artistique)

- Proposal "**2% for 2 degrees**" from the [Institut Rousseau](https://institut-rousseau.fr/2-pour-2c-resume-executif/)

- Different initiatives coming from the world of free software or institutions:
:::
