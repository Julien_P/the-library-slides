
# <span style="display:none">The Librery</span>

:::{.img-librery .img-no-margin .img-no-caption}
![&nbsp;](images/Librery-logo-baudot-black.png)
:::

:::{.text-center}
<b>A sustainable financing mechanism
  <br>and a collective management organization (CMO)
  <br>by and for the authors of commons
</b>
:::

::::::{.columns .text-center .text-micro}
:::{.column .card .card-secondary width="25%"}
<i class="ri-download-2-fill"></i><br>
**Collect,**<br>**pooling,**<br>and **redistribution** <br>of funds
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-scales-3-fill"></i><br>
A strong requirement for **transparency, non-profit making and parity**
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-share-fill"></i><br>
Diversified forms of **support for the commons and commoners**
:::
:::{.column .card .card-secondary width="25%"}
<i class="ri-shield-check-line"></i><br>
The possibility of a **label**
:::
::::::

## General economy

:::{.r-stack}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-03-bis.png)
:::
:::{.fragment .fade-in data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-07.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-08.png)
:::
:::

## OGC missions

- **collect** financial contributions from broadcasters
- **identify** common areas and their authors
- **calculate** what must be distributed (distribution keys)
- **redistribute** in rights / declare to URSSAF (to make life easier for authors)
- **defend** the cause and rights of authors of digital commons
- **promote** the status of authorship of digital commons
- **introduce into the law** clauses equivalent to those relating to the CNC / SACD / etc… (compulsory contribution regime?)

## Financial contributions to support the economy of the commons

For the sake of pooling resources, the collective management organization must be able to <b>receive different types of potentially “incoming” financial flows</b>, public or private money:

::: {.text-micro}
:::{.columns}
:::{.column width="50%"}

**Potential financial flows of public origin**

- Plea for the principle of <b>2% common in public procurement</b> on digital projects (inspired by “ [2% for 2 degrees](https://institut-rousseau.fr/2-pour-2c-resume-executif/) ”, from the Rousseau Institute; or “ [1% for art in France](https://www.culture.gouv.fr/Thematiques/Arts-plastiques/Commande-artistique/Le-1-artistique)”)
- <b>Subsidy-type</b> financial contributions from public institutions (IGN, ADEME, DINUM, etc.)
- Intervention at <b>European level</b> to support digital commons, such as [NGI](https://www.ngi.eu/), [NLnet](https://nlnet.nl/), or projects such as [EDIC](https://digital-strategy.ec.europa.eu/en/policies/edic)
:::

:::{.column width="50%"}
**Potential financial flows of private origin**

- Financial contributions (donation type) from certain <b>private companies</b> benefiting from/disseminating digital commons
- <b>Services</b> such as code audits, sourcing , etc.
- Advocacy for the principle of a mandatory minimum contribution obligation - or <b>royalty</b> - for industries whose activity is based on the extensive exploitation of strategic digital commons

<!-- - Possibilité de proposer à des acteurs privés la **garantie de bon fonctionnement de certaines librairies libres critiques**, en échange d'apports financiers redistribués directement vers les mainteneur.e.s de ces librairies. -->
:::
:::
:::

## Governance conditions

- The commons falling both in the public sphere and in the private sphere, it is desirable that **all parties can be represented** within the OGC, however ensuring good representation of authors/individuals, given that the mission of the OGC is good at defending them

- As a structure defending a sector (the commons) the OGC has more **legitimacy** as a private law structure, provided that the different stakeholders can be fairly represented

- **Include authors** as well as **publishers**, **funders** and **broadcasters** of digital commons into the governance

## Structural conditions

On the basis of the criticisms made of cultural OGCs (see Benhamou) we can list some <b>necessary (but not sufficient) conditions to avoid certain shortcomings</b> of historical OGCs identified by observers and the academic field:

- **Management** fees not exceeding 5% to 10%
- **Monitoring** and automation of author registration and remuneration
- **Capping** of salaries of OGC managers
- **Representation** of all authors (1 person == 1 vote)
- **Total transparency** of governance and operation, particularly regarding the choice and implementation of distribution keys

## Operating equilibrium<br>(assumptions)

::::::{.columns}
:::{.column width="50%"}
**Operating costs**

:::{.text-micro}
With a full team (5 full-time people, see further in the “Reflections on governance” section) with salaries of €4,000 net/month, offices, and some additional services (accounting, subscriptions, services online…) :

as a first approximation the total <b>operating and management cost would be around €550k/year</b>.
:::
:::

:::{.column width="50%"}
**Collect**

:::{.text-micro}
If we maintain operating and management costs around 10%, this means that the <b>OGC must be able to collect €5.5 million/year.</b>

Starting from a base of “common 2%” on digital projects, this collection amount corresponds to a set of <b>digital projects whose cumulative budgets would represent €275 million / year</b> before contribution to the commons.

:::
:::
::::::

**Redistribution**

:::{.text-micro}
In summary, by applying the principle of “common 2%” on a set of digital projects to the tune of €275 million / year, the OGC would be able to redistribute: €5.5 million (collected) - 0.55 million € (management fees)

soit <b>i.e. a redistribution of €4.95 million / year to digital commons.</b>
:::

## Possible legal vehicles

Different types of legal structures can be imagined to fulfill the missions of The Librery, with the common point of being <b>non-profit</b> or at least limited profit.

:::{.text-micro}
- The **Collective Management Organization**
- The **Collective Copyright Management Society**
- The **Foundation**
- The **"Fiducie"** (or _trust_)
- The **Cooperative Society of Collective Interest** (SCIC in France)
- THE **Public Interest Group** (GIP in France)
- The **"Mutuelle"**
- The **Investment fund**
- The **Endowment fund**
- The **Association / NGO** (French 1901 law)
:::

## The label as an incentive

- The existence of a legitimate organization for all stakeholders in the digital commons would make it possible to imagine the creation of a “ **common label** ”, which the organization could issue to projects and structures.

- The notion of “common labels” can be an interesting area to develop to **encourage administrations and businesses to contribute** up to 2% of their digital projects budgets.

- The notion of label raises the questions of **objective criteria** allowing a digital common to be qualified as such.

- The other counterpart of the notion of label would be to be able to **identify and monitor economic valuation indices** (financial, time allocated, savings, etc.) making it possible to better situate the contributions of digital commons within the industrial sector.

## Logo ideas

<br>

:::{.columns .text-micro}
:::{.column width="50%"}
### Inspired from [XKCD n°2347](https://xkcd.com/2347/)

![&nbsp;](images/Librery-logo-long-white.png)

![&nbsp;](images/Librery-logo-long-dots-01.png)
:::
:::{.column width="50%"}
### Inspired from [Baudot codex](https://fr.wikipedia.org/wiki/Code_Baudot)

![&nbsp;](images/code-baudot.png)

![&nbsp;](images/Librery-logo-baudot-black.png)
:::
:::
