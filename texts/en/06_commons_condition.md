
# The commons, condition and driver of innovation

:::{.r-stack .no-margin}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-00.png)
:::
:::{.fragment .fade-in data-fragment-index="1"}
![&nbsp;](images/diagrams/diagram-00-bis.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-01.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-02.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-03.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-04.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05-a.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05-b.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-05.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-06.png)
:::
:::fragment
![&nbsp;](images/diagrams/diagram-06-bis.png)
:::
:::

## The risk of ecosystem exhaustion

:::{.img-shadow .opaque .img-no-caption .img-large}
![&nbsp;](images/maintenance-paradox.png)
:::

<br>

:::{.text-micro .text-center}
See the presentation by [Tobie Langel](https://speaking.unlockopen.com/nBXJS5/1-billion-dollars-for-open-source-maintainers) given during [State Of Open Con](https://stateofopencon.com/) 2024.
:::

## Security risk

:::{.columns}

:::{.column width="50%"}
:::{.text-micro}
The security risk narrowly avoided in 2024 on the XZ Utils library ([source](https://www.itpro.com/software/open-source/we-got-lucky-what-the-xz-utils-backdoor -says-about-the-strength-and-insecurities-of-open-source))
:::
:::{.img-shadow .opaque}
![&nbsp;](images/xz-fail.png)
:::
:::

:::{.column width="50%"}
:::{.text-micro}
“A billion for open source”. See the presentation by [Tobie Langel](https://speaking.unlockopen.com/nBXJS5/1-billion-dollars-for-open-source-maintainers) 2024.
:::
:::{.img-shadow .opaque}
![&nbsp;](images/one-billion-maintain.png)
:::
:::

:::

:::{.text-micro .text-center}
The security risks (see the [FOSSEPS report](https://joinup.ec.europa.eu/collection/fosseps/news/fosseps-critical-open-source-software-study-report) from the European Commission in 2022) of our digital infrastructures once again raise the question of the <b>lack of resources allocated to the maintenance</b> of open source projects.
:::

## Systemic risk

:::{.columns}
:::{.column width="50%"}
:::{.fragment .img-shadow .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/curl.png)
:::
:::
:::{.column width="50%"}
:::{.fragment .img-shadow .fade-in data-fragment-index="1"}
![&nbsp;](images/open-ssl.png)
:::
:::
:::

:::{.text-micro .text-center}
:::fragment
Examples found on the [Github sponsors](https://github.com/sponsors) homepage
:::
:::

## The economics of public services and digital commons

:::{.fragment .text-center .semi-fade-out .opaque data-fragment-index="1"}
::: {.img-small}
![&nbsp;](images/2-percents.png)
:::
:::

:::{.fragment .text-center data-fragment-index="1"}
<br>
<b>An engine running on reserve ?</b>
:::
