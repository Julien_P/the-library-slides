
# Scenarios

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
A **simple mechanism**
:::
:::{.column .card width="33%"}
**Contractual** approach
:::
:::{.column .card width="33%"}
Different approaches depending on **public or private actors**
:::
::::::
