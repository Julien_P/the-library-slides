
# The interest of copyright in supporting the economy of the commons

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
Digital commons are **collective or collaborative works**
:::
:::{.column .card width="25%"}
Commoners are **authors​**
:::
:::{.column .card width="25%"}
Contributing can be recognized as **work**
:::
:::{.column .card width="25%"}
Managing free copyright requires a **shared** structure
:::
::::::

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
The **digital commons economy** is unique and very diversified
:::
:::{.column .card width="25%"}
Use **existing and community law** as leverage
:::
:::{.column .card width="25%"}
Learn from **past economic and legal experiences**
:::
:::{.column .card .card-transparent width="25%"}
&nbsp;
:::
::::::

## Digital commons are <br> collective or collaborative works

- Digital is a **cultural industry**, just like publishing, cinema or music

- Digital commons (software, databases) can be considered **works of the mind**


## Commoners are *authors*

- Contributors and/or initiators of digital commons can be considered as **authors or co-authors** (at least in France)

- **The notion of author is legally regulated in France**: an author of free software retains economic rights over his work.

- A free/digital common work is potentially a **collective or collaborative work** (hence the notion of co-author)

## Contributing is a job

- Contributing to software or a database is a **high value-added activity**

- You can **combine the fact of being an author and having a salaried activity**

- A free work can be developed by an employee **within a company**
  - The author remains the employee (a legal entity cannot be an author)
  - The company is the owner of the rights, provided that this is specified in a copyright assignment contract.
  - Special case of civil servants developing commons within the framework of their functions.

## Managing free copyrights <br>requires a shared structure

- Just as has been the case in the field of music or publishing, it is in practice **impossible for an individual author to do himself** :

  - **monitoring** all modes of exploitation of its works

  - **defending** its rights with broadcasters

- New free works are initiated by individuals , sometimes as employees in companies but not always.

## The digital commons economy is unique


::: {.text-center}
`"A software is free once it is paid"`
:::

- A free work is - as a first approximation - not intended to directly receive operating income, one of the specificities of digital commons is their **free use and distribution**.

- Financial flows linked to the production of a digital commons can only be found **upstream of their publication**

## Use the law as leverage

A <b>free license</b> does not mean that the author no longer has rights, but:

- what entity exists to **defend these right**s ?

- Even if there were an entity to defend these rights, there are no clearly defined **financial flows** for their support, but on the contrary a jungle of different economic models

## Learn from past experiences

- It is necessary to **defend and promote people/authors**, offer them an income model, to encourage people to contribute to digital commons and above all to earn income from it (partially or totally)

- There already exist and there have been **experiments** for changing the law (global license, universal income, etc.), but these proposals have not always been successful and/or are still not very operational.

- Rather than seeking to add to the law and regulations, it is possible to **rely on existing law**, to draw inspiration from various historical examples such as the [private copy royalties](https://www.culture.gouv.fr/Espace-documentation/Rapports/Rapport-du-Gouvernement-au-Parlement-sur-la-remuneration-pour-copie-privee-octobre-2022), the creation of[collective rights management organisations](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006069414/LEGISCTA000006146355/2222-02-22) such as the SACEM or the SACD, or even the concept of [French "cultural exception"](https://fr.wikipedia.org/wiki/Exception_culturelle)
