
# How can we continue <br>this reflection?

:::{.text-micro}

<i class="ri-share-line mr-1" style="color: black;"></i>
**Share and debate** these ideas with commons stakeholders

<i class="ri-edit-2-line mr-1" style="color: black;"></i>
**Contribute** on the [pad](https://hackmd.io/@Jpy/the-librery) open in writing and comments

<i class="ri-list-unordered mr-1" style="color: black;"></i>
**Identify** experiments in pooling financial resources to support the commons

<i class="ri-map-pin-line mr-1" style="color: black;"></i>
**Map** the actors / broadcasters / producers / products

<i class="ri-focus-3-line mr-1" style="color: black;"></i>
 Identify, classify and prioritize a shortlist of **strategic commons** for the State or communities

<i class="ri-message-2-line mr-1" style="color: black;"></i>
Collect **testimonials and new ideas** from digital commons producers

<i class="ri-pages-line mr-1" style="color: black;"></i>
Bring together these ideas, documentation, testimonials on an online **website**

:::
