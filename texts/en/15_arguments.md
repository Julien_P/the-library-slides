
## Argument grid <br>for financial contributors

<table class="grid">
  <thead>
    <tr>
      <th></th>
      <th class="text-center"><b>PUBLIC</b></th>
      <th class="text-center"><b>PRIVATE</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>**CONTRIBUTION**</td>
      <td>
        2% of the amount <br>of digital projects
      </td>
      <td>
        Depending on the type and size of the business:<br>
        - x % of annual recurring revenue<br>
        - x % of management surplus / annual profits<br>
        - x € yearly by developer<br>
        - fixed sum annually
      </td>
    </tr>
    <tr>
      <td>**OPTIONS**</td>
      <td>
One-off agreement <br>
Multi-annual agreement <br>
Agreements per project<br>
(or) with an administration
      </td>
      <td>
One-off or multi-annual agreement <br>
Donations<br>
Pledge of donations
      </td>
    </tr>
    <tr>
      <td>**CONTRACTUALISATION**</td>
      <td class="text-center" colspan="2">
45% of the donation: direction by the structure towards one or more common areas<br>
45% of the donation: direction by OGC towards one or more communities<br>
10% of the donation: OGC management fees
      </td>
    </tr>
    <tr>
      <td>**COUNTERPARTS**</td>
      <td class="text-center" colspan="2">
Critical commons maintenance insurance<br>
Labeling <br>
Team empowerment<br>
Choice by structure to direct towards common areas<br>
Choice by structure to specify certain remuneration mechanisms (distribution keys)<br>
Tax exemption (if private structure)
      </td>
    </tr>
    <tr>
      <td>**FUNDS MANAGEMENT**</td>
      <td class="text-center" colspan="2">
Transparency of the OGC and participation in its governance<br>
Automatic reporting on donation usage<br>
Online management of contribution contracts<br>
Automation of administrative procedures<br>
Certificate for tax exemption (for private structures)
      </td>
    </tr>
  </tbody>
</table>
