
# Thoughts on the governance

:::::: {.columns .text-center .text-micro}
:::{.column .card width="25%"}
A representation of **all commons stakeholders**
:::
:::{.column .card width="25%"}
A structural majority for the **authors**
:::
:::{.column .card width="25%"}
**Human and technical resources** for the implementation of missions
:::
:::{.column .card width="25%"}
Strong **transparency** and **non-profit** requirements
:::
::::::

## Funding digital commons as a common

::: {.opaque .img-xl .img-no-caption .img-no-margin}
![&nbsp;](images/diagrams/Handler.ashx_.jpeg)
:::

::: {.text-micro .text-center}
<i class="ri-book-2-line"></i>
See the book <b>"Governing the commons"</b> by [Elinor Orstom](https://en.wikipedia.org/wiki/Elinor_Ostrom)
:::

## General diagram

:::::: {.columns}

::: {.column width="60%"}
![&nbsp;](images/global.png)
:::

::: {.column width="40%"}
::: {.text-micro}
General principle diagram of The Librery (made with[Whimsical](https://whimsical.com/the-librery-organigramme-RimouvNB2jVq7pcu59zP4H))

From top to bottom :

- **The stakeholders** :
  - Authors
  - Works (common)
  - Broadcasters
  - Editors
  - Sympathizers
- **The OGC team**
- **The pool fund** and its different uses
  - Rights management
  - Targeted aid
  - Programs / actions
  - Promotion of the commons
- **The available tools**
  - Web portal (+ backoffice)
  - Third-party services

:::
:::

::::::

## The stakeholders

![&nbsp;](images/stakeholders.png)

## The financial means: the fund

![&nbsp;](images/fund.png)

## The human resources: the team

![&nbsp;](images/team.png)

## The technical means: tools

![&nbsp;](images/tools.png)
