
# Summary

::: {.text-micro}

- [Prologue](#prologue)
- [Summary of key proposals](#summary-of-key-proposals)
- [Problematic](#problematic)
- [The commons, condition and driver of innovation](#the-commons-condition-and-driver-of-innovation)
- [Some central notions](#some-central-notions)
- [Why 2% of public procurement?](#why-2-of-public-procurement)
- [The interest of copyright in supporting the economy of the commons](#the-interest-of-copyright-in-supporting-the-economy-of-the-commons)
- [The Librery](#the-librery)
- [Collect and redistribution](#collect-and-redistribution)
- [Thoughts on governance](#thoughts-on-the-governance)
- [Internationalisation & decentralization](#internationalisation-decentralization)
- [Inspirations](#inspirations)
- [Scenarios](#scenarios)
- [How can we continue this reflection?](#how-can-we-continue-this-reflection)
- [References and documentation](#references-and-documentation)
- [Thanks](#thanks)
- [Document versions](#versions-du-document)
:::
