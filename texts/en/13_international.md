
# Internationalisation & decentralization

:::::: {.columns .text-center .text-micro}
:::{.column .card width="33%"}
Being able to adapt to **local legal specificities**
:::
:::{.column .card width="33%"}
Being able to **share datasets** (registers of authors or commons)
:::
:::{.column .card width="33%"}
Have a **decentralized information architecture**
:::
::::::

## Why anticipating an international approach?

#### The constraints of the production of digital commons

::: {.text-micro}
- Digital commons can be developed <b>anywhere in the world</b>.
- The <b>commoners</b> contributing to the same common can themselves be <b>based in different countries</b>.
- The <b>financial contributors to the commons</b> can also be based in different countries.
- An OGC is necessarily legally based in a country, <b>the legal status of an OGC can therefore be different from one country to another</b> (foundation, trust, association, trust, etc.).
- An OGC does not necessarily have the vocation - or the capacity - to deal with all of the commons at the global level, <b>it is more realistic to envisage that an OGC has a precise territorial and/or sectoral anchoring</b>.

It is therefore necessary that all CMOs throughout the world can **share common registers to avoid duplication and legal inconsistencies**: register of commons, register of commoners, amounts of financial contributions, allocation of sums towards the commons and the commoners...
:::

## Decentralized architecture

::: {.text-micro}
- It would be possible - or even more realistic - to have **several OGCs distributed throughout the world**, and/or different OGCs "specialized" in certain verticals (industrial sector, programming language, etc.)

- For such a system to operate on an international scale, the **information infrastructure must be shared**, while allowing each OGC to manage its own corpus.

- Technically it would be possible to offer all OGCs a free digital management tool based on [ActivityPods](https://activitypods.org) in order to be able to both **decentralize data storage**, while allowing **each instance to manage its own datasets**.

:::

## Principle diagram

:::{.r-stack .no-margin}
:::{.fragment .semi-fade-out .opaque data-fragment-index="1"}
![&nbsp;](images/diagrams/decentralized/diagram-01.png)
:::
:::fragment
![&nbsp;](images/diagrams/decentralized/diagram-02.png)
:::
:::fragment
![&nbsp;](images/diagrams/decentralized/diagram-03.png)
:::
:::fragment
![&nbsp;](images/diagrams/decentralized/diagram-99.png)
:::
:::
