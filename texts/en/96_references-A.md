
# References and documentation

Some bibliographical elements, classified by media and major themes, in addition to general notes relating to reflections around [The Librery](https://hackmd.io/@Jpy/the-librery).

<i class="ri-message-2-line"></i> [Channel on Matrix](https://matrix.to/#/#the-librery:multi.coop) for chatting

<i class="ri-file-pdf-2-line"></i> [Articles / books / pdf (nextcloud)](https://nuage.liiib.re/s/dx5eribMSRm6RTm) in open access

<i class="ri-book-2-line"></i> [Bibliographies (Zotero)](https://www.zotero.org/groups/5255274/librery/library)

<i class="ri-file-text-line"></i> [Note pad](https://hackmd.io/@Jpy/the-librery) open for writing and comments

<i class="ri-movie-line"></i> [Videos / conferences (playlist Youtube)](https://www.youtube.com/playlist?list=PLfXKG51wk2sTBeAJtuL8wsUNvD-6lVDPM)

<i class="ri-gitlab-line"></i> [Source files](https://gitlab.com/Julien_P/the-librery-slides/-/tree/main/texts) et [entire presentation](https://gitlab.com/Julien_P/the-librery-slides) on Gitlab

## Other notable online resources
