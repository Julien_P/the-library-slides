
# Thanks

**The comments published in this presentation are those of the author alone.**

However, the author would like to send a <b>big thanks</b> to all the people who contribute to enriching this reflection and <b>supporting the movement for a society of the commons</b>.

::::::{.columns}

:::{.column}
::: {.text-nano-grey .text-center .text-compact}

**Proofreadings or comments**

<br>
