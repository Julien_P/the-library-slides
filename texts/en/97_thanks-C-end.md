
:::

:::

::::::

## A last word...

<br>

:::{.columns}

:::{.column width="30%" .img-shadow}
![&nbsp;](images/an-01.jpg)
:::

:::{.column width="70%" .text-center}

:::{.text-large}
<br>
**Utopia reduces when cooking**<br>
**which is why**<br>
**you need a lot from the start**
:::

<br>

:::{.text-micro}
Quote from Gébé, ["L'an 01"](https://www.lassociation.fr/catalogue/l-an-01-livre-et-dvd/)
:::
:::

:::
