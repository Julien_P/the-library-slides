
#

::: {.text-center}
**Thanks for your attention !**

::: {.img-nano .img-no-caption}
![&nbsp;](images/Librery-logo-baudot-black.png)
:::

[https://julien_p.gitlab.io/the-librery-slides/librery.html](https://julien_p.gitlab.io/the-librery-slides/librery.html)

<i class="ri-mail-line"></i>&nbsp; julien.paris<i class="ri-at-line"></i>multi.coop
<br><i class="ri-message-2-line"></i> [channel Matrix for chatting](https://matrix.to/#/#the-librery:multi.coop)
:::
