
## Focus on the Public Copy initiative

:::{.columns}
:::{.column width="15%"}
![&nbsp;](images/copie-publique-logo.svg)
:::

:::{.column .text-micro .text-no-margin-top width="85%"}
The [Public Copy](https://copiepublique.fr/) initiative consists of the description of a **mechanism for allocating and then redistributing funds** to open source projects, which a company can freely choose to implement provided that it respects its form and spirit. .

The system proposed by **Public Copy is not a fund shared by several companies** : each company choosing to adopt the Copie Publique mechanism contributes alone to its own fund, on the basis of an annual base that it chooses and makes public. A company can thus devote, for example, 1% of its turnover, or 3% of its net management surplus, or even a fixed amount per year. The only condition is that the rule is clear, transparent, and allows contributions to the fund on a recurring basis.

Different aspects of the Public Copy mechanism are interesting to highlight, in particular those relating to the **process of selecting supported projects** chosen by the company Code Lutin and the dynamics that this process generates:
:::
:::

:::{.text-nano-grey}
:::{.columns}
:::{.column width="50%"}
The choice of supported projects is broken down into 2 phases in parallel, dividing the sum to be initially redistributed into two separate envelopes:

- <b>a collective phase</b>, aimed at identifying and choosing a reduced number of projects through a “call for applications”: candidates send a description of the project for which they are requesting support, and also specify their needs. The collective chooses between 1 and 3, following the establishment of a jury.
- <b>a more individual phase</b>, where each employee has the possibility of redistributing an envelope to one or more projects of their choice (from the call for applications or not). This phase makes it possible to support more projects of smaller sizes.
:::
:::{.column width="50%"}
<b>All the company's employees are involved</b> during the process of identification, evaluation, selection, and remuneration of the projects to be supported. Each person - individually or collectively - can thus actively contribute to supporting free third-party projects, a freedom that does not exist in any company.

Following the call for applications, one of the moments described as the most exciting is that of the <b>meeting (face-to-face or video) with the candidates</b>. This stage allows members of the company to discover individuals and original technologies, in a direct manner while retaining the human and social dimension of the projects.
:::
:::
:::

:::{.text-nano-grey}
_A warm thank you to the members of [Code Lutin](https://www.codelutin.com/) with whom the author was able to exchange in Nantes, and who were able to describe their Public Copy initiative with precision and passion_
:::
