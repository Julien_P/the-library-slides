
# Why 2% of public procurement?

:::::: {.columns}
:::{.column .text-center .card width="33%"}
:::fragment
A **simple** slogan
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Existing **regulatory and legal frameworks**
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
**Acceptable amounts** on public development budgets
:::
:::
::::::

:::::: {.columns}
:::{.column .text-center .card width="33%"}
:::fragment
A **swarmable** mechanism for scaling up
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Raise awareness of **sovereignty issues**
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Develop the **role of the public actor** in supporting the commons
:::
:::
::::::

## Simple slogan and mecanism

The ambition is to reach certain <b>orders of magnitude in terms of amounts collected</b>, so that these are sufficient to irrigate and consolidate an industrial economy of the commons.

This ambition therefore requires addressing numerous interlocutors in the administration and in the private sector, each with **different concerns, professions, priorities or action frameworks**.

In order to be able to <b>generalize the principle of collection and pooling</b> for support of the commons, it is therefore necessary to offer all sponsors a mechanism that is **simple to understand** from the outset and **simple to implement** , whatever their administrative affiliation or their sector of activity.

## Framing figures and orders of magnitude

:::::: {.columns}

::: {.column .text-micro width="55%"}

<b>Overview of major French State IT projects</b>
<br>September 2023, source [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/panorama-des-grands-projets-numeriques-de-letat/)
<br>For a total of approximately **3,625 million euros**

::: {.table-micro}

| Ministère nom complet                                                                 | Nom du projet              | Début        | Durée prévisionnelle en année | Phase du projet en cours          | Coût estimé |
|---------------------------------------------------------------------------------------|----------------------------|--------------|-------------------------------|-----------------------------------|-------------|
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | PAYSAGE                    | octobre 14   | 9,5                           | Déploiement / Bilan intermédiaire | 53,1        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | CFVR                       | juillet 13   | 9,5                           | Terminé                           | 34,5        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | PILAT                      | juin 18      | 7,6                           | Conception / Réalisation          | 123,5       |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | GMBI                       | septembre 18 | 5,6                           | Conception / Réalisation          | 35,7        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | NRP (NOUVEAU RÉSEAU DGFIP) | janvier 18   | 5,8                           | Conception / Réalisation          | 38,5        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | ROCSP                      | février 19   | 8,9                           | Expérimentation                   | 96,4        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | 3D                         | février 20   | 3,9                           | Expérimentation                   | 31,9        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | TNCP                       | janvier 20   | 4,4                           | Expérimentation                   | 21,1        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | PCR                        | octobre 19   | 3,2                           | Terminé                           | 52,8        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FRANCE SESAME              | janvier 20   | 4                             | Conception / Réalisation          | 10,9        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FONCIER INNOVANT           | novembre 20  | 3,2                           | Déploiement / Bilan intermédiaire | 33,1        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FICOBA 3                   | novembre 20  | 4                             | Conception / Réalisation          | 21          |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | Chorus – Projet S_4HANA    | septembre 22 | 2,1                           | Conception / Réalisation          | 87          |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FACTURATION ELECTRONIQUE   | mars 21      | 7,8                           | Conception / Réalisation          | 231         |
| Ministère de l'Europe et des Affaires étrangères                                      | SAPHIR                     | décembre 15  | 8                             | Déploiement / Bilan intermédiaire | 10,4        |
| Ministère de la Santé et de la Prévention                                             | SI SAMU                    | septembre 14 | 10,3                          | Conception / Réalisation          | 218,4       |
| Ministère de la Santé et de la Prévention                                             | Mon Espace Santé           | janvier 20   | 4                             | Conception / Réalisation          | 227,4       |
| Ministère de la Santé et de la Prévention                                             | ROR                        | janvier 21   | 5,5                           | Conception / Réalisation          | 24,9        |
| Ministère de la Santé et de la Prévention                                             | SI APA                     | juin 22      | 3,6                           | Conception / Réalisation          | 63,4        |
| Ministère de l'Agriculture et de la Souveraineté alimentaire                          | EXPADON 2                  | janvier 13   | 11                            | Déploiement / Bilan intermédiaire | 30,9        |
| Ministère de la Culture                                                               | MISAOA                     | juin 20      | 4                             | Conception / Réalisation          | 11,2        |
| Ministère des Armées                                                                  | ARCHIPEL                   | avril 15     | 8,8                           | Déploiement / Bilan intermédiaire | 14,3        |
| Ministère des Armées                                                                  | SOURCE WEB                 | janvier 14   | 9,8                           | Conception / Réalisation          | 15,3        |
| Ministère des Armées                                                                  | ROC                        | mars 16      | 8,1                           | Conception / Réalisation          | 15,6        |
| Ministère des Armées                                                                  | EUREKA                     | octobre 17   | 6,4                           | Conception / Réalisation          | 21,9        |
| Ministère des Armées                                                                  | SSLD-II                    | novembre 20  | 2,8                           | Conception / Réalisation          | 28,8        |
| Ministère des Armées                                                                  | SPARTA                     | février 18   | 6,3                           | Conception / Réalisation          | 15,8        |
| Services du Premier Ministre                                                          | NOPN                       | janvier 21   | 5,5                           | Conception / Réalisation          | 26,9        |
| Ministère de l'Éducation nationale et de la Jeunesse                                  | OP@LE                      | septembre 14 | 11                            | Déploiement / Bilan intermédiaire | 91,3        |
| Ministère de l'Éducation nationale et de la Jeunesse                                  | MIGRATION SIRH             | janvier 20   | 5,3                           | Conception / Réalisation          | 57,9        |
| Ministère de l'Éducation nationale et de la Jeunesse                                  | REURBANISATION SIRH        | janvier 20   | 5,3                           | Conception / Réalisation          | 68,7        |
| Ministère de la Transition écologique et de la Cohésion des territoires               | GEOPLATEFORME              | janvier 19   | 5,4                           | Déploiement / Bilan intermédiaire | 23,2        |
| Ministère de la Transition écologique et de la Cohésion des territoires               | OCSGE                      | septembre 19 | 5,8                           | Déploiement / Bilan intermédiaire | 30,4        |
| Ministère de la Transition écologique et de la Cohésion des territoires               | GUNENV Phase 2             | mars 23      | 4                             | Conception / Réalisation          | 37          |
| Ministère de l'Intérieur et des Outre-mer                                             | MCIC-2                     | février 15   | 9,9                           | Expérimentation                   | 24,2        |
| Ministère de l'Intérieur et des Outre-mer                                             | RRF                        | octobre 22   | 8                             | Conception / Réalisation          | 900,3       |
| Ministère de l'Intérieur et des Outre-mer                                             | NexSIS Version 1           | avril 17     | 8,2                           | Déploiement / Bilan intermédiaire | 72          |
| Ministère de l'Intérieur et des Outre-mer                                             | ANEF                       | janvier 18   | 5,9                           | Déploiement / Bilan intermédiaire | 54,5        |
| Ministère de l'Intérieur et des Outre-mer                                             | LOG-MI                     | septembre 17 | 7,8                           | Déploiement / Bilan intermédiaire | 28,1        |
| Ministère de l'Intérieur et des Outre-mer                                             | ERPC                       | mai 19       | 4,8                           | Déploiement / Bilan intermédiaire | 82,8        |
| Ministère de l'Intérieur et des Outre-mer                                             | KIOSQUES - PFSF            | avril 20     | 4                             | Déploiement / Bilan intermédiaire | 26,3        |
| Ministère de l'Intérieur et des Outre-mer                                             | FIN                        | février 20   | 5,3                           | Déploiement / Bilan intermédiaire | 68,3        |
| Ministère de l'Intérieur et des Outre-mer                                             | SIV                        | février 20   | En cadrage                    | Cadrage                           | En cadrage  |
| Ministère de l'Intérieur et des Outre-mer                                             | M@GRH                      | avril 21     | 2,8                           | Déploiement / Bilan intermédiaire | 14          |
| Ministère de la Justice                                                               | ASTREA                     | janvier 12   | 14                            | Conception / Réalisation          | 77,3        |
| Ministère de la Justice                                                               | PORTALIS                   | mars 14      | 12,8                          | Conception / Réalisation          | 98,9        |
| Ministère de la Justice                                                               | PROJAE – AXONE             | juin 17      | 7,1                           | Conception / Réalisation          | 14,5        |
| Ministère de la Justice                                                               | NED                        | janvier 18   | 4,8                           | Terminé                           | 8,7         |
| Ministère de la Justice                                                               | PPN                        | janvier 20   | 6                             | Conception / Réalisation          | 145,1       |
| Ministère de la Justice                                                               | ATIGIP                     | janvier 20   | 4,4                           | Déploiement / Bilan intermédiaire | 43,2        |
| Ministère du Travail, du Plein emploi et de l'Insertion                               | SI EMPLOI                  | novembre 21  | 4                             | Conception / Réalisation          | 33          |
| Ministère du Travail, du Plein emploi et de l'Insertion                               | SI FSE                     | janvier 20   | 6                             | Déploiement / Bilan intermédiaire | 30,1        |

:::
:::

::: {.column .text-micro width="45%"}

<b>Management of major French State digital projects</b>
<br>2020, source [Cour des Comptes](https://www.ccomptes.fr/fr/publications/la-conduite-des-grands-projets-numeriques-de-letat)
<br>an average of **343.631 million euros/year** between 2016 and 2018

![&nbsp;](images/cdc/cdc-07.png)

:::
::::::

## Become aware of sovereignty issues

- Free tools located in the lower and less visible layers of the various digital services set up by public authorities raise **questions of criticality and risks** (see the note from the [Quai d'Orsay](https://www.diplomatie.gouv.fr/IMG/pdf/20200731-note-complete-communs_cle021839.pdf) ): security, technical complexity, updates, maintainability…

- The proper functioning of “ low level ” tools can be an undervalued dimension when **calculating the risks of digital projects**, projects which sometimes have an **infrastructural dimension** ( sovereign cloud , health data, army payroll software, etc.).

- Sometimes critical “ low level ” tools can be **maintained by individuals** outside national borders, **outside the sectoral authority** of the sponsoring administrations, and yet prove essential to the proper functioning of the services implemented ( [examples of cURLor openSSL](https://github.com/sponsors) , or that of [core.js](https://github.com/zloirock/core-js/blob/master/docs/2023-02-14-so-whats-next.md) library).

## Role of the State

### The State as **facilitator for commons**

:::{.text-micro}
Before playing the role of actor or producer of commons, the State can take on as a priority the role of <b>guarantor of the framework</b> for the production of commons of general interest by civil society. This framework is above all legal and legislative (GDPR, RGAA, DMA, etc.) but the role of guarantor of this framework is also played through support and animation actions, as already proposed by public actors such as the ANCT , IGN or DINUM.
:::

### The State as **industrial policies** carrier

:::{.text-micro}
The State, but also all ancillary administrations (agencies) or decentralized (communities), can <b>support the industrial economy and the jobs represented by private actors producing the commons</b>, by perpetuating various direct financing methods including the “2% common”, in addition to other historical modes of support.
:::

## A sustainable financing mechanism

- An **alternative to one-off funding** (grants, calls for projects, ad hoc funds , etc.)

- Taking into account the **orders of magnitude** necessary to support a real economy of the commons

- An ability to “ **scale up** ”: either by playing on the percentage, or by expanding to sectors other than digital

## Implementation to be clarified

- Is the **existing regulatory framework sufficient** ? Is there a legal risk of being accused of distortion of competition?

- Which projects, in which partner administrations, with which allies, to **experiment with now** ?

- What strategy is there **to bring this principle before decision-makers** in the administration?

## Some additional ideas

- Start with the public, expand to the private (or share the effort): would it be interesting (for political or economic reasons) to break down the “2%” into “ **2% = 1% public + 1% private** ”? That is to say 1% of the public order upstream, and 1% of the turnover or invoices of private companies in project management?

- **Putting numbers against ideas** : assessing the amounts of public money currently spent on digital projects to establish a sort of forecast budget
