
# Summary of key proposals

:::::: {.columns}
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-money-euro-circle-fill"></i><br>
"**2% for commons**" <br>on public digital projects
:::
:::
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-group-fill"></i><br>
A fund managed **independently**<br>by commons stakeholders
:::
:::
::::::

:::::: {.columns}
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-download-2-fill"></i><br>
A structure that can receive public or private **financial contributions**
:::
:::
:::{.column .text-center .card .card-secondary width="50%"}
:::fragment
<i class="ri-scales-3-fill"></i><br>
A **transparent, equal and non-profit** redistribution organization
:::
:::
::::::

## General economy

:::{.opaque .img-no-caption}
![&nbsp;](images/diagrams/diagram-08.png)
:::

## Distribution of collected funds

:::{.opaque .img-no-caption}
![&nbsp;](images/librery-fund-sankey-en.png)
:::

## Internationalisation

:::{.opaque .img-no-caption}
![&nbsp;](images/diagrams/decentralized/diagram-99.png)
:::

## Funding digital commons as a common

::: {.opaque .img-xl .img-no-caption .img-no-margin}
![&nbsp;](images/diagrams/Handler.ashx_.jpeg)
:::

::: {.text-micro .text-center}
<i class="ri-book-2-line"></i>
See the book <b>"Governing the commons"</b> by [Elinor Orstom](https://en.wikipedia.org/wiki/Elinor_Ostrom)
:::
