
# Summary

::: {.text-micro}

- [Prologue](#prologue)
- [Problematic](#problematic)
- [The commons, condition and driver of innovation](#the-commons-condition-and-driver-of-innovation)
- [The Librery](#the-librery)
- [Collect and redistribution](#collect-and-redistribution)
- [Thoughts on governance](#réflexions-sur-la-gouvernance)
- [Internationalisation & decentralization](#internationalisation-decentralization)
- [Scenarios](#scenarios)
- [Inspirations](#inspirations)
- [How can we continue this reflection?](#how-can-we-continue-this-reflection)
- [References and documentation](#references-and-documentation)
- [Thanks](#thanks)
- [Document versions](#versions-du-document)
:::
