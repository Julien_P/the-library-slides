
# Prologue

::: {.text-center}
[https://julien_p.gitlab.io/the-librery-slides](https://julien_p.gitlab.io/the-librery-slides)
:::

The <b>comments</b> appearing in this presentation <b>are those of the author alone</b>.

These reflections and proposals are in <b>no way fixed</b> and represent more of a <b>step in a process of collective reflection</b>.

This communication exercise should be considered above all as a way of <b>opening these ideas to criticism </b>, with the aim of ultimately arriving at a base of <b>concrete proposals</b> that can be put forward by the broadest possible consortium of commons' stakeholders.

All elements presented here are open to discussion and criticism, including on a [notes pad](https://hackmd.io/@Jpy/the-librery) <b> open for writing and comments</b>.

## Related presentations
