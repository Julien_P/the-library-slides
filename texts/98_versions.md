
# Versions du document

:::{.text-nano-grey}

- **Version 1.x** - février 2024 / décembre 2024 :
  - Version 1.7 :
    - Ajout de la liste "Organisations funding the commons" dans la page [Ressources](##autres-ressources-en-ligne-remarquables)
    - Ajout de la slide [Présentations liées](#presentations-liées)
  - Version 1.6 :
    - Ajouts dans la page [Revue de presse](#revue-de-presse)
    - Ajout du lien vers le nouvel [espace Matrix](https://matrix.to/#/#the-librery:multi.coop) dédié
    - Ajout de [Open Source Pledge](https://osspledge.com/) dans la page [Inspirations](#inspirations) et ajouts dans la page [Remerciements](#remerciements)
    - Ajout du diagramme de Sankey sur la [répartition des fonds](#répartition-des-fonds-collectés)
    - Ajout de la slide [Revue de presse](#revue-de-presse)
    - Ajout de la slide sur les risques d'[épuisement de l'écosystème](le-risque-dépuisement-de-lécosystème) et de [sécurité](#le-risque-de-faille-de-sécurité)
    - Ajouts dans la page [Remerciements](#remerciements)
    - Ajout de la verticale de slides sur les[scénarios](#scenarios)
    - Ajout de la verticale de slides sur l'[internationalisation](#internationalisation)
  - Version 1.5 :
    - Updates dans la page [Remerciements](#remerciements) - ajout d'[Olivier Charbonneau](https://www.linkedin.com/in/olivier-charbonneau-b210491/)
    - Ajout du lien vers [Open Patent](https://www.boldandopen.com/open-patent) dans la page [Inspirations](#inspirations)
    - Ajout de la slide focus sur la licence "[Post-Open](#focus-sur-la-licence-post-open)"
    - Updates dans la page [Remerciements](#remerciements) + ajouts de liens + ajout de [Bruce Perens](https://perens.com/)
    - Ajout du lien vers des informations sur les [Fiducies d'intérêt social]() dans la page [Inspirations](#inspirations)
    - Ajout de la [version en anglais de la présentation](https://julien_p.gitlab.io/the-librery-slides/presentation-en.html)
    - Ajouts dans la page [Remerciements](#remerciements)
  - Version 1.4 :
    - Ajout du lien vers la publication du think tank [Open Future](https://openfuture.eu/publication/european-public-digital-infrastructure-fund/)
    - Ajout du lien vers l'article sur [Bruce Perens](https://programmation.developpez.com/actu/352421/Qu-est-ce-qui-vient-apres-l-open-source-Un-pionnier-du-mouvement-de-l-open-source-affirme-qu-il-faut-changer-de-paradigme-et-trouver-un-moyen-equitable-de-remunerer-les-developpeurs/), de [KDE e.V.](https://ev.kde.org/), et de [Open Source Initiative](https://opensource.org/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
:::

## Archives (v.1.0 - v.1.3)

:::{.text-nano-grey}

- **Version 1.x** - novembre 2023 / février 2024 :
  - Version 1.3 :
    - Ajouts dans la page [Remerciements](#remerciements)
    - Ajout des liens vers [Open Infra](https://openinfra.dev/), [Open North](https://opennorth.ca/), et [Green Software Foundation](https://greensoftware.foundation/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Ajout du lien vers [Funding the commons](https://fundingthecommons.io/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Ajout du lien vers le [Digital Transparency Lab](https://www.transparencylab.ca/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Ajout de la revue [Imaginaire Communs](https://anis-catalyst.org/communs/imaginaire-communs/) dans la page [Autres ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Chapitre à part pour la partie [Inspirations](#inspirations)
    - Version parallèle simplifiée, réduite uniquement sur Librery + [url dédiée](https://julien_p.gitlab.io/the-librery-slides/librery.html)
    - Ventilation des chapitres dans le répertoire `/texts` ([voir sur le repo](https://gitlab.com/Julien_P/the-librery-slides/-/tree/main/texts))
  - Version 1.2 : Mises à jour
    - Ajout du lien [Compte commun des communs](https://wiki.resilience-territoire.ademe.fr/wiki/LE_COMPTE_COMMUN_DES_COMMUNS) dans la page [Inspirations](#inspirations)
    - Ajouts dans la page [Ressources remarquables](#autres-ressources-en-ligne-remarquables)
    - Ajouts dans la page [Remerciements](#remerciements)
    - Ajout de la page [Focus sur Copie Publique](#focus-sur-linitiative-copie-publique)
    - Ajout de la _Software Freedom Conservancy_ dans la page [Inspirations](#inspirations)
  - Version 1.1 : Ajout de la slide [Place de l'Etat](#la-place-de-letat) et modifications mineures
  - Version 1.0 : Première version du document
  - Version 1.0 : Présentation lors de l'évènement NEC local à Lille "[Les communs pour un numérique au service de tous](https://openagenda.com/assembleurs/events/les-communs-pour-un-numerique-au-service-de-tous)"
:::
