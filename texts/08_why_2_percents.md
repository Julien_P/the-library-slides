
# Pourquoi 2% <br>de la commande publique ?

:::::: {.columns}
:::{.column .text-center .card width="33%"}
:::fragment
Un slogan **simple**
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Des **cadres réglementaires et juridiques** existants
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Des **sommes acceptables** sur des budgets de chantiers publics
:::
:::
::::::

:::::: {.columns}
:::{.column .text-center .card width="33%"}
:::fragment
Un mécanisme **essaimable** pour un passage à l'échelle
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Faire prendre conscience des **enjeux de souveraineté**
:::
:::
:::{.column .text-center .card width="33%"}
:::fragment
Faire évoluer la **place de l'acteur public** dans le soutien aux communs
:::
:::
::::::

## Un slogan et un mécanisme simple

L'ambition est d'atteindre certains <b>ordres de grandeur en termes de montants collectés</b>, que ceux-ci soient suffisants pour irriguer et consolider une économie industrielle des communs.

Cette ambition suppose donc de s'adresser à de nombreux interlocuteurs dans l'administration et dans le privé, ayant chacun **des préoccupations, des métiers, des priorités ou des cadres d'action différents**.

Afin de pouvoir <b>généraliser le principe d'une collecte et d'une mutualisation</b> pour le soutien aux communs, il est donc nécessaire de proposer à tous les commanditaires un **mécanisme simple à comprendre** d'emblée et **simple à mettre en oeuvre**, quel que soit leur rattachement administratif ou leur secteur d'activité.

## Chiffres de cadrage et ordres de grandeurs

:::::: {.columns}

::: {.column .text-micro width="55%"}

<b>Panorama des grands projets SI de l'Etat</b>
<br>Septembre 2023, source [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/panorama-des-grands-projets-numeriques-de-letat/)
<br>Soit un total de **3 625 millions d'euros** environ

::: {.table-micro}

| Ministère nom complet                                                                 | Nom du projet              | Début        | Durée prévisionnelle en année | Phase du projet en cours          | Coût estimé |
|---------------------------------------------------------------------------------------|----------------------------|--------------|-------------------------------|-----------------------------------|-------------|
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | PAYSAGE                    | octobre 14   | 9,5                           | Déploiement / Bilan intermédiaire | 53,1        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | CFVR                       | juillet 13   | 9,5                           | Terminé                           | 34,5        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | PILAT                      | juin 18      | 7,6                           | Conception / Réalisation          | 123,5       |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | GMBI                       | septembre 18 | 5,6                           | Conception / Réalisation          | 35,7        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | NRP (NOUVEAU RÉSEAU DGFIP) | janvier 18   | 5,8                           | Conception / Réalisation          | 38,5        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | ROCSP                      | février 19   | 8,9                           | Expérimentation                   | 96,4        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | 3D                         | février 20   | 3,9                           | Expérimentation                   | 31,9        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | TNCP                       | janvier 20   | 4,4                           | Expérimentation                   | 21,1        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | PCR                        | octobre 19   | 3,2                           | Terminé                           | 52,8        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FRANCE SESAME              | janvier 20   | 4                             | Conception / Réalisation          | 10,9        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FONCIER INNOVANT           | novembre 20  | 3,2                           | Déploiement / Bilan intermédiaire | 33,1        |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FICOBA 3                   | novembre 20  | 4                             | Conception / Réalisation          | 21          |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | Chorus – Projet S_4HANA    | septembre 22 | 2,1                           | Conception / Réalisation          | 87          |
| Ministère de l’Économie, des Finances et de la Souveraineté industrielle et numérique | FACTURATION ELECTRONIQUE   | mars 21      | 7,8                           | Conception / Réalisation          | 231         |
| Ministère de l'Europe et des Affaires étrangères                                      | SAPHIR                     | décembre 15  | 8                             | Déploiement / Bilan intermédiaire | 10,4        |
| Ministère de la Santé et de la Prévention                                             | SI SAMU                    | septembre 14 | 10,3                          | Conception / Réalisation          | 218,4       |
| Ministère de la Santé et de la Prévention                                             | Mon Espace Santé           | janvier 20   | 4                             | Conception / Réalisation          | 227,4       |
| Ministère de la Santé et de la Prévention                                             | ROR                        | janvier 21   | 5,5                           | Conception / Réalisation          | 24,9        |
| Ministère de la Santé et de la Prévention                                             | SI APA                     | juin 22      | 3,6                           | Conception / Réalisation          | 63,4        |
| Ministère de l'Agriculture et de la Souveraineté alimentaire                          | EXPADON 2                  | janvier 13   | 11                            | Déploiement / Bilan intermédiaire | 30,9        |
| Ministère de la Culture                                                               | MISAOA                     | juin 20      | 4                             | Conception / Réalisation          | 11,2        |
| Ministère des Armées                                                                  | ARCHIPEL                   | avril 15     | 8,8                           | Déploiement / Bilan intermédiaire | 14,3        |
| Ministère des Armées                                                                  | SOURCE WEB                 | janvier 14   | 9,8                           | Conception / Réalisation          | 15,3        |
| Ministère des Armées                                                                  | ROC                        | mars 16      | 8,1                           | Conception / Réalisation          | 15,6        |
| Ministère des Armées                                                                  | EUREKA                     | octobre 17   | 6,4                           | Conception / Réalisation          | 21,9        |
| Ministère des Armées                                                                  | SSLD-II                    | novembre 20  | 2,8                           | Conception / Réalisation          | 28,8        |
| Ministère des Armées                                                                  | SPARTA                     | février 18   | 6,3                           | Conception / Réalisation          | 15,8        |
| Services du Premier Ministre                                                          | NOPN                       | janvier 21   | 5,5                           | Conception / Réalisation          | 26,9        |
| Ministère de l'Éducation nationale et de la Jeunesse                                  | OP@LE                      | septembre 14 | 11                            | Déploiement / Bilan intermédiaire | 91,3        |
| Ministère de l'Éducation nationale et de la Jeunesse                                  | MIGRATION SIRH             | janvier 20   | 5,3                           | Conception / Réalisation          | 57,9        |
| Ministère de l'Éducation nationale et de la Jeunesse                                  | REURBANISATION SIRH        | janvier 20   | 5,3                           | Conception / Réalisation          | 68,7        |
| Ministère de la Transition écologique et de la Cohésion des territoires               | GEOPLATEFORME              | janvier 19   | 5,4                           | Déploiement / Bilan intermédiaire | 23,2        |
| Ministère de la Transition écologique et de la Cohésion des territoires               | OCSGE                      | septembre 19 | 5,8                           | Déploiement / Bilan intermédiaire | 30,4        |
| Ministère de la Transition écologique et de la Cohésion des territoires               | GUNENV Phase 2             | mars 23      | 4                             | Conception / Réalisation          | 37          |
| Ministère de l'Intérieur et des Outre-mer                                             | MCIC-2                     | février 15   | 9,9                           | Expérimentation                   | 24,2        |
| Ministère de l'Intérieur et des Outre-mer                                             | RRF                        | octobre 22   | 8                             | Conception / Réalisation          | 900,3       |
| Ministère de l'Intérieur et des Outre-mer                                             | NexSIS Version 1           | avril 17     | 8,2                           | Déploiement / Bilan intermédiaire | 72          |
| Ministère de l'Intérieur et des Outre-mer                                             | ANEF                       | janvier 18   | 5,9                           | Déploiement / Bilan intermédiaire | 54,5        |
| Ministère de l'Intérieur et des Outre-mer                                             | LOG-MI                     | septembre 17 | 7,8                           | Déploiement / Bilan intermédiaire | 28,1        |
| Ministère de l'Intérieur et des Outre-mer                                             | ERPC                       | mai 19       | 4,8                           | Déploiement / Bilan intermédiaire | 82,8        |
| Ministère de l'Intérieur et des Outre-mer                                             | KIOSQUES - PFSF            | avril 20     | 4                             | Déploiement / Bilan intermédiaire | 26,3        |
| Ministère de l'Intérieur et des Outre-mer                                             | FIN                        | février 20   | 5,3                           | Déploiement / Bilan intermédiaire | 68,3        |
| Ministère de l'Intérieur et des Outre-mer                                             | SIV                        | février 20   | En cadrage                    | Cadrage                           | En cadrage  |
| Ministère de l'Intérieur et des Outre-mer                                             | M@GRH                      | avril 21     | 2,8                           | Déploiement / Bilan intermédiaire | 14          |
| Ministère de la Justice                                                               | ASTREA                     | janvier 12   | 14                            | Conception / Réalisation          | 77,3        |
| Ministère de la Justice                                                               | PORTALIS                   | mars 14      | 12,8                          | Conception / Réalisation          | 98,9        |
| Ministère de la Justice                                                               | PROJAE – AXONE             | juin 17      | 7,1                           | Conception / Réalisation          | 14,5        |
| Ministère de la Justice                                                               | NED                        | janvier 18   | 4,8                           | Terminé                           | 8,7         |
| Ministère de la Justice                                                               | PPN                        | janvier 20   | 6                             | Conception / Réalisation          | 145,1       |
| Ministère de la Justice                                                               | ATIGIP                     | janvier 20   | 4,4                           | Déploiement / Bilan intermédiaire | 43,2        |
| Ministère du Travail, du Plein emploi et de l'Insertion                               | SI EMPLOI                  | novembre 21  | 4                             | Conception / Réalisation          | 33          |
| Ministère du Travail, du Plein emploi et de l'Insertion                               | SI FSE                     | janvier 20   | 6                             | Déploiement / Bilan intermédiaire | 30,1        |

:::
:::

::: {.column .text-micro width="45%"}

<b>Conduite des grands projets numériques de l'Etat</b>
<br>2020, source [Cour des Comptes](https://www.ccomptes.fr/fr/publications/la-conduite-des-grands-projets-numeriques-de-letat)
<br>Soit une moyenne de **343,631  millions d'euros / an** entre 2016 et 2018

![&nbsp;](images/cdc/cdc-07.png)

:::
::::::

## Prendre conscience <br>des enjeux de souveraineté

- Les outils libres situés dans les couches basses et peu visibles des différents services numériques mis en place par les pouvoirs publics posent des **questions de criticité et de risques** (voir [la note du Quai d'Orsay](https://www.diplomatie.gouv.fr/IMG/pdf/20200731-note-complete-communs_cle021839.pdf)) : sécurité, complexité technique, mises à jour, maintenabilité...

- Le bon fonctionnement des outils "_low level_" peut être une dimension sous-évaluée lors des **calculs de risques** des chantiers numériques, chantiers qui ont parfois une **dimension infrastructurelle** (_cloud_ souverain, données de santé, logiciel de paie des armées...).

- Des outils "_low level_" parfois critiques peuvent être **maintenus par des individus** hors des frontières hexagonales, **hors des compétences sectorielles** des administrations commanditaires, et pourtant se révéler essentielles au bon fonctionnement des services mis en oeuvre ([exemples de `cURL` ou `openSSL`](https://github.com/sponsors), ou celui de la librairie [`core.js`](https://github.com/zloirock/core-js/blob/master/docs/2023-02-14-so-whats-next.md)).

## La place de l'Etat

### L'Etat **facilitateur des communs**

:::{.text-micro}
Avant de jouer un rôle d'acteur ou producteur de communs, l'Etat peut endosser en priorité le rôle de <b>garant du cadre</b> pour la production de communs d'intérêt général par la société civile. Ce cadre est avant tout juridique et législatif (RGPD, RGAA, DMA...) mais le rôle de garant de ce cadre se joue également par des actions d'accompagnement et d'animation, comme le proposent déjà des acteurs publics tels que l'ANCT, l'IGN ou la DINUM. 
:::

### L'Etat porteur d'une **politique industrielle**

:::{.text-micro}
L'Etat, mais également toutes les administrations annexes (agences) ou décentralisées (collectivités), peuvent <b>soutenir l'économie industrielle et les emplois que représentent les acteurs privés produisant les communs</b>, en pérennisant des modes de financements directs divers dont le "2% communs", en complément des autres modes de soutien historiques.
:::

## Un mécanisme de financement pérenne

- Une **alternative aux financements ponctuels** (subventions, appels à projets, fonds _ad hoc_...)

- Une prise en compte des **ordres de grandeur** nécessaires au soutien à une réelle économie des communs

- Une capacité à "**passer à l'échelle**" : soit en jouant sur le pourcentage, soit en élargissant à d'autres secteurs que le numérique

## Une mise en oeuvre à préciser

- Le **cadre réglementaire existant est-il suffisant** ? Y a-t-il un risque juridique d'être accusé de distorsion de concurrence ?

- Quels chantiers, dans quelles administrations partenaires, avec quels alliés, pour **expérimenter dès maintenant** ?

- Quelle stratégie pour **aller porter ce principe devant les décisionnaires** dans l'administration ?

## Quelques idées complémentaires

- Commencer par le public, élargir au privé (ou partage de l'effort) : serait-il intéressant (pour des raisons politiques ou économiques) de décomposer le "2%" en "**2% = 1% public + 1% privé**" ? C'est-à-dire 1% de la commande publique en amont, et 1% du CA ou des factures des entreprises privées en maîtrise d'oeuvre ?

- Mettre **des chiffres en face des idées** : évaluer quels sont les montants d'argent public actuellement dépensés en chantiers numériques pour établir une sorte de prévisionnel
