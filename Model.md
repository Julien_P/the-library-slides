# The Librery (projet)

## Schéma de la gouvernance générale et des missions d'un OGC des communs ("The Librery")

<iframe style="border:none" width="800" height="450" src="https://whimsical.com/embed/RimouvNB2jVq7pcu59zP4H"></iframe>

source schéma : https://whimsical.com/libry-RimouvNB2jVq7pcu59zP4H

## Modélisation de la base de données de l'OGC des communs

### A/ Process côté diffuseurs de communs numériques

- Est considéré comme diffuseur une entité morale ou physique prête à verser volontairement une somme afin de rémunerer les auteurs et co-auteurs d'un ou plusieurs communs
- Un diffuseur signe un contrat de rétribution aux communs avec l'OGC, l'OGC s'engage à redistribuer les montants volontairement versés par le diffuseur.
- Le contrat porte sur un montant de contribution volontaire sur un ou plusieurs communs, voire sans commun particulier, le tout sur une période donnée
- Ce contrat peut porter des clauses et règles de redistribution
- Le contrat est alors ventilé par montants alloués par année et par commun
- Tous les contrats et leurs montants associés sont agregés par commun, cela constitue un fonds par commun (et par année)

```mermaid
graph TD

    diffusorA[Diffuseur A]
    diffusorA --> contractA[contrat avec OGC]
    contractA --> amountA1[montant x]
    contractA --> amountA2[montant y]
    
    diffusorB[Diffuseur B]
    diffusorB --> contractB[contrat avec OGC]
    contractB --> amountB1[montant z]
    
    subgraph "commun 1"
    fund1[fonds mutualisé n.1] -- "fléché pour" --> commons1[commun n.1]
    end 
    
    subgraph "commun 2"
    fund2[fonds mutualisé n.2] -- "fléché pour" --> commons2[commun n.2]
    end 
    
    amountA1 --> fund1
    amountA2 --> fund2
    amountB1 --> fund2
    
```



### B/ Process côté auteurs de communs

- Un auteur s'enregistre à l'OGC en précisant à quelles oeuvres/communs il contribue
- Un contrat de cession/gestion de ses droits d'auteur est etabli entre l'OGC et l'auteur
- Ses contributions à des communs sont calculées et enregistrées au cours de l'année, commun par commun
- En fin d'année ses contributions permettent de calculer le montant de la rétribution qui lui est allouée, commun par commun, en fonction du fonds associé et des règles de redistribution
- L'OGC établit sa déclaration de droits d'auteurs auprès de l'urssaf et transmet à l'auteur la preuve de déclaration

```mermaid
graph 

    subgraph "Contribution d'un auteur  à un commun"
    author[Auteur / commoner]
    author --> contrib1[contributions à commun n.1]
    %% author -.- contract[contrat de gestion des droits]
    end
    
    subgraph "commun 1"
        fund1[fonds mutualisé n.1]
        fund1 -- "fléché pour" --> commons1[commun n.1]
    end 
    
    retrib1[part de rétribution]
    contrib1 -- "indicateurs de contribution à" --> commons1
    commons1 -. "règles de redistribution" .- retrib1
    contrib1 -. "donne droit à" .-> retrib1
    
    fund1 -- "calcul selon règles et indicateurs" --> retrib1
    
    retrib1 -- "déclaration et versement par OGC à" --> author
```

### C/ Cas particulier des éditeurs de communs numériques

- Un éditeur de communs emploie un ou des salariés, considérés comme les auteurs initiaux des communs édités
- L'éditeur est considéré comme titulaires des droits d'auteurs de ses employés 
- L'éditeur permet la gestion des droits en son nom par l'OGC auprès de l'urssaf
- L'éditeur enregistre ses employés comme auteurs sur l'OGC
- L'OGC calcule et déclare les rétributions aux auteurs comme en B/, avec la particularité d'enregistrer ces sommes à l'urssaf au nom de l'éditeur aussi mais envoie les documents à l'éditeur

### Modèle de base de données pour ces process

```mermaid
erDiagram
    
    %% PHYSICAL OR MORAL ENTITIES
    DIFFUSOR {
        string diffusor_id PK
        string siren_number PK
        string denomination
        string description
        array structure_type
        string country
        string url
        file logo
        boolean is_editor
        boolean is_pure_donator 
        string bank_iban
        string contact_email
        date date_adhesion
        boolean yearly_adhesion_payed
    }
    AUTHOR {
        string author_id PK
        string siren_number PK
        string urssaf_number PK
        string entreprise_type
        string name
        string surname
        string country
        string contact_email
        file cession_contract_to_ogc
        booelan is_verified
        boolean is_public
        string bank_iban
        date date_adhesion
        number parts_in_organisation
        string author_gitlab_id
        string author_github_id
        array common_ids
    }
    EDITOR {
        string editor_id PK
        string siren_number PK
        string structure_type
        string denomination
        string description
        string url
        file logo
        file contract
        array comon_ids FK
        array author_ids FK
        booelan is_verified
        string bank_iban
        string contact_email
        date date_adhesion
        boolean yearly_adhesion_payed
    }
    
    %% CATALOG OF DIGITAL COMMONS
    COMMON {
        string common_id PK
        string repo_url
        string name
        string description
        string documentation_url
        array links
        array diffusor_ids FK
        date added
        file logo
        string licence_type
        array categories
        array environments
        array tech_languages
        string version
        array dependencies
        array linked_common_ids FK
    }

    %% RELATIONAL OBJECTS
    DIFFUSOR_CONTRACT {
        string contract_id PK
        string diffusor_id FK
        file document_contract
        string affair_state
        string validated
        date signature_date
        number amount_in_euros
        array retribution_rules
    }
    AMOUNT_BY_CONTRACT {
        string amount_id PK
        string contract_id FK
        string diffusor_id FK
        string common_id FK
        number amount_in_euros
        array retribution_rules
        boolean is_payed
        date year
    }
    COMMON_FUND {
        string fund_id PK
        string common_id FK
        array amount_ids FK
        number amount_in_euros_year_1
        number amount_in_euros_year_2
        number amount_in_euros_year_n
        array retribution_rules
    }
    
    %% TRANSACTIONAL OBJECTS
    AUTHOR_CONTRIBUTION {
        string contribution_id PK
        string author_id FK
        string common_id FK
        date year
        number amount_of_contributions
        boolean validated
    }
    AUTHOR_RETRIBUTION {
        string retribution_id PK
        string author_id FK
        string common_id FK
        string fund_id FK
        string contribution_id FK
        string editor_id FK
        file document_urssaf
        boolean process_by_editor
        date year
        number amount_in_euros
        boolean is_due
        boolean is_declared
        string editor_id FK
    }
    
    %% RELATIONSHIPS
    COMMON ||..|{ DIFFUSOR : "common has one or many diffusors" 
    COMMON ||--|{ AUTHOR : "common has one or many authors" 
    COMMON ||..o{ EDITOR : "common can have zero or many editors" 
    COMMON ||..|{ COMMON : "a common can be linked to zero or many other commons"
    AUTHOR_CONTRIBUTION ||--|| COMMON  : "contribution is linked to only one common" 
    EDITOR ||--|{ AUTHOR : "editor has one or many authors (employees)"
    
    AUTHOR ||--|{ AUTHOR_CONTRIBUTION : "author makes one or many contributions upon one common each year"
    
    DIFFUSOR ||--|{ DIFFUSOR_CONTRACT : "diffusor has one or many contracts"
    DIFFUSOR_CONTRACT ||--|{ AMOUNT_BY_CONTRACT : "contracts are ventilated into one or many amounts by year and common"
    AMOUNT_BY_CONTRACT }|--|| COMMON_FUND : "amounts are linked to only one fund per common"
    AMOUNT_BY_CONTRACT ||--|| COMMON : "amounts per year are linked to only one common"
    COMMON_FUND ||--|{ COMMON : "fund is linked to only one or many commons"
    COMMON_FUND ||--|{ AUTHOR_RETRIBUTION : "fund leads to one or many retributions by year and by author"
    
    AUTHOR_RETRIBUTION ||--|| AUTHOR_CONTRIBUTION : "retribution is linked to only one author's contribution"
    AUTHOR_RETRIBUTION ||--|| COMMON : "retribution is linked to only one common"
    AUTHOR_RETRIBUTION ||--|| AUTHOR : "retribution is linked to only one author"
    AUTHOR_RETRIBUTION ||..o| EDITOR : "retribution can be linked to zero or only one editor"

```

## Modèle simplifié

```mermaid
erDiagram
    
    %% PHYSICAL OR MORAL ENTITIES
    DIFFUSOR {
    }
    AUTHOR {
    }
    EDITOR {
    }
    
    %% CATALOG OF DIGITAL COMMONS
    COMMON {
    }

    %% RELATIONAL OBJECTS
    DIFFUSOR_CONTRACT {
    }
    AMOUNT_BY_CONTRACT {
    }
    COMMON_FUND {
    }
    
    %% TRANSACTIONAL OBJECTS
    AUTHOR_CONTRIBUTION {
    }
    AUTHOR_RETRIBUTION {
    }
    
    %% RELATIONSHIPS
    COMMON ||..|{ DIFFUSOR : "common has one or many diffusors" 
    COMMON ||--|{ AUTHOR : "common has one or many authors" 
    COMMON ||..o{ EDITOR : "common can have zero or many editors" 
    COMMON ||..|{ COMMON : "a common can be linked to zero or many other commons"
    AUTHOR_CONTRIBUTION ||--|| COMMON  : "contribution is linked to only one common" 
    EDITOR ||--|{ AUTHOR : "editor has one or many authors (employees)"
    
    AUTHOR ||--|{ AUTHOR_CONTRIBUTION : "author makes one or many contributions upon one common each year"
    
    DIFFUSOR ||--|{ DIFFUSOR_CONTRACT : "diffusor has one or many contracts"
    DIFFUSOR_CONTRACT ||--|{ AMOUNT_BY_CONTRACT : "contracts are ventilated into one or many amounts by year and common"
    AMOUNT_BY_CONTRACT }|--|| COMMON_FUND : "amounts are linked to only one fund per common"
    AMOUNT_BY_CONTRACT ||--|| COMMON : "amounts per year are linked to only one common"
    COMMON_FUND ||--|{ COMMON : "fund is linked to only one or many commons"
    COMMON_FUND ||--|{ AUTHOR_RETRIBUTION : "fund leads to one or many retributions by year and by author"
    
    AUTHOR_RETRIBUTION ||--|| AUTHOR_CONTRIBUTION : "retribution is linked to only one author's contribution"
    AUTHOR_RETRIBUTION ||--|| COMMON : "retribution is linked to only one common"
    AUTHOR_RETRIBUTION ||--|| AUTHOR : "retribution is linked to only one author"
    AUTHOR_RETRIBUTION ||..o| EDITOR : "retribution can be linked to zero or only one editor"

```
